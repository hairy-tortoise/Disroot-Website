#!/usr/bin/env bash

# Disroot Website Development Environment using Vagrant on DEBIAN

# !! Make sure your user is in sudoers !!

sudo apt update ; sudo apt -y dist-upgrade ; sudo apt -y install wget

## Requirements
### Vagrant 2.2.17

dpkg -s vagrant | grep "install ok installed" &> /dev/null  ## INSTALL AGAIN even when already installed!!

if [ $? -eq 0 ]; then
    echo "====> Vagrant is installed!"
else
    wget https://releases.hashicorp.com/vagrant/2.2.17/vagrant_2.2.17_x86_64.deb
    sudo dpkg -i vagrant_2.2.17_x86_64.deb
    sudo apt install -f -y
fi


### Virtuabox 

dpkg -s virtualbox-6.1 | grep "install ok installed" &> /dev/null

if [ $? -eq 0 ]; then
    echo "====> Virtualbox is installed!"
else
    echo 'deb http://download.virtualbox.org/virtualbox/debian buster contrib' | sudo tee /etc/apt/sources.list.d/virtualbox.list
    wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
    wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
    sudo apt-get update ; sudo apt-get install -y virtualbox-6.1
fi


### Vagrant plugins vagrant-vbguest:
vagrant plugin list | grep vbguest &> /dev/null

if [ $? -eq 0 ]; then
    echo "====> vagrant-vbguest is installed!"
else
    vagrant plugin install vagrant-vbguest
fi



### Git
sudo apt install -y git


## Installation
echo "====> Downloading all needed Disroot repos..."

if [ ! -d ~/Disroot ]; then
  mkdir ~/Disroot;
fi

if [ ! -d ~/Disroot/Website ]; then
    git clone https://git.disroot.org/Disroot/Website.git ~/Disroot/Website
fi

if [ ! -d ~/Disroot/grav-theme-disroot ]; then
    git clone https://git.disroot.org/Disroot/grav-theme-disroot.git ~/Disroot/grav-theme-disroot
fi

if [ ! -d ~/Disroot/grav-theme-disrootblog ]; then
    git clone https://git.disroot.org/Disroot/grav-theme-disrootblog.git ~/Disroot/grav-theme-disrootblog
fi

if [ ! -d ~/Disroot/grav-plugin-language-selector ]; then
    git clone https://git.disroot.org/Disroot/grav-plugin-language-selector.git ~/Disroot/grav-plugin-language-selector
fi

if [ ! -d ~/Disroot/Disroot-Privacy-Policy ]; then
    git clone https://git.disroot.org/Disroot/Disroot-Privacy-Policy.git ~/Disroot/Disroot-Privacy-Policy
fi

if [ ! -d ~/Disroot/Disroot-Changelog ]; then
    git clone https://git.disroot.org/Disroot/CHANGELOG.git ~/Disroot/Disroot-Changelog
fi


cat /etc/hosts | grep '192.168.33.12'

if [ $? -eq 0 ]; then
    echo "====> Disroot Website Host is already set!"
else
    echo '192.168.33.12 disroot.lan' | sudo tee -a /etc/hosts
fi

echo "====> Booting Disroot Website Virtual Environment..."
cd ~/Disroot/Website/vagrant
vagrant up disroot_lan
vagrant provision


echo "====> You can now access Disroot Website VE with your browser at http://disroot.lan"
