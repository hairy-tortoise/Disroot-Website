---
title: Privacy Policy
bgcolor: '#FFF'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _title
            - _pp
body_classes: modular
header_image: pp.jpg
---
