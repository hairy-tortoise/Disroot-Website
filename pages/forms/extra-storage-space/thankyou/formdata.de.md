---
title: 'Extra storage requested'
cache_enable: false
process:
    twig: true
---

<<br><br> **Wir haben Deine Anfrage erhalten. <br><br>Du wirst eine Bestätigung per Email mit den Zahlunganweisungen erhalten. <br>Wenn wir Deine Zahlung erhalten haben, werden wir Dir den zusätzlichen Speicherplatz zuweisen. <br><br> Vielen Dank für Deine Unterstützung von Disroot!**
<br>
<hr>
<br>
**Hier nochmal eine Zusammenfassung Deiner Anfrage:**
