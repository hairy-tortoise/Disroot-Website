---
title: 'Merci pour votre demander'
process:
    markdown: true
    twig: true
cache_enable: false
---

<br><br> **En fonction de notre charge de travail, l'examen de votre demande peut prendre jusqu'à deux semaines, mais nous faisons de notre mieux pour examiner les demandes chaque semaine..**
<br>
<hr>
<br>
**Voici un résumé de la demande reçue:**
