---
title: 'Solicitud de Alias'
cache_enable: false
process:
    twig: true
---

<br><br> **Dependiendo de la carga de trabajo que tengamos, podría llevarnos hasta dos semanas revisar tu solicitud. Estamos trabajando para mejorar eso en el futuro.**
<br>
<hr>
<br>
**Aquí está un resumen de la solicitud recibida:**
