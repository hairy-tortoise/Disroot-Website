---
title: Services
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: Email
        icon: email.png
        link: 'https://disroot.org/services/email'
        text: "Comptes de messagerie gratuits et sécurisés pour votre client IMAP de bureau ou via une interface Web."
        button: 'https://webmail.disroot.org'
        buttontext: "Se connecter"
    -
        title: Cloud
        icon: cloud.png
        link: 'https://disroot.org/services/nextcloud/'
        text: "Vos données sous votre contrôle ! Collaborez, synchronisez et partagez des fichiers, des calendriers, etc."
        button: 'https://cloud.disroot.org'
        buttontext: "Se connecter"

    -
        title: Chat XMPP
        icon: xmpp.png
        link: 'https://disroot.org/services/xmpp'
        text: "Messagerie instantanée décentralisée."
        button: 'https://webchat.disroot.org'
        buttontext: "Se connecter"
    -
        title: Pads
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Créez et modifiez des documents en collaboration et en temps réel."
        button: 'https://pad.disroot.org'
        buttontext: "Commencer"

    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Paste-bin en ligne chiffré / forum de discussion."
        button: 'https://bin.disroot.org'
        buttontext: "Partager"
    -
        title: Upload
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Logiciel d'hébergement et de partage de fichiers temporaires et chiffrés."
        button: 'https://upload.disroot.org'
        buttontext: "Partager"
    -
        title: Search
        icon: search.png
        link: 'https://disroot.org/services/search'
        text: "Une plate-forme anonyme multi moteurs de recherche."
        button: 'https://search.disroot.org'
        buttontext: "Rechercher"

    -
        title: 'Calls'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "Un outil de vidéo conférence."
        button: 'https://calls.disroot.org'
        buttontext: "Appeler"
    -
        title: 'Git'
        icon: git.png
        link: 'https://disroot.org/services/git'
        text: "Un hébergement de code et une collaboration de projet."
        button: 'https://git.disroot.org'
        buttontext: "Se connecter"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "Une application de chat vocal à faible latence et de haute qualité."
        button: 'https://mumble.disroot.org'
        buttontext: "Créer une chaîne"
        #badge: EXPERIMENTAL
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "Une alternative privée aux outils de bureautique populaires."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Accès"
        #badge:
    -
        title: 'Akkoma'
        icon: akkoma.png
        link: 'https://disroot.org/services/akkoma'
        text: "A microblogging tool that can federate with other servers that support ActivityPub."
        button: 'https://fe.disroot.org'
        buttontext: "Log in"
---
