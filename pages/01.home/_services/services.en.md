---
title: Services
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: Email
        icon: email.png
        link: 'https://disroot.org/services/email'
        text: "Free and secure email accounts for your desktop IMAP client or via a Web-interface."
        button: 'https://webmail.disroot.org'
        buttontext: "Log in"
    -
        title: Cloud
        icon: cloud.png
        link: 'https://disroot.org/services/nextcloud/'
        text: "Your data under your control! Collaborate, sync & share files, calendars, contacts and more."
        button: 'https://cloud.disroot.org'
        buttontext: "Log in"
    -
        title: XMPP Chat
        icon: xmpp.png
        link: 'https://disroot.org/services/xmpp'
        text: "Decentralized instant messaging."
        button: 'https://webchat.disroot.org'
        buttontext: "Log in"
    -
        title: Pad
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Create and edit documents collaboratively in real-time directly in the web browser."
        button: 'https://pad.disroot.org'
        buttontext: "Start a pad"

    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Encrypted online paste-bin/discussion board."
        button: 'https://bin.disroot.org'
        buttontext: "Share a pastebin"
    -
        title: Upload
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Encrypted, temporary file hosting and sharing software."
        button: 'https://upload.disroot.org'
        buttontext: "Share a file"
    -
        title: Search
        icon: search.png
        link: 'https://disroot.org/services/search'
        text: "An anonymous multi search engine platform."
        button: 'https://search.disroot.org'
        buttontext: "Search"

    -
        title: 'Calls'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "A videoconferencing tool."
        button: 'https://calls.disroot.org'
        buttontext: "Start a call"
    -
        title: 'Git'
        icon: git.png
        link: 'https://disroot.org/services/git'
        text: "A code hosting and project collaboration."
        button: 'https://git.disroot.org'
        buttontext: "Log in"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "A low latency, high quality voice chat application."
        button: 'https://mumble.disroot.org'
        buttontext: "Log in"
        #badge: EXPERIMENTAL
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "A private-by-design alternative to popular office tools."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Access"
        #badge: EXPERIMENTAL

    -
        title: 'Akkoma'
        icon: akkoma.png
        link: 'https://disroot.org/services/akkoma'
        text: "A microblogging tool that can federate with other servers that support ActivityPub."
        button: 'https://fe.disroot.org'
        buttontext: "Log in"
---
