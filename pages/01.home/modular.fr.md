---
title: Disroot
bgcolor: '#1F5C60'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _about
            - _donatebutton
            - _donation
            - _signup
            - _services
            - _disapp
menu: home
big_header: true
onpage_menu: true

translation_banner:
    set: true
    last_modified: Mars 2022
    language: French
---

<link rel="alternate" type="application/atom+xml" title="Disrot Blog" href="fr/blog.atom" />
