---
title: 'Disroot App'
bgcolor: '#1F5C60'
fontcolor: '#fff'
text_align: left
wider_column: left
---

<br>

## Récupérez la sur  [![F-droid](F-Droid.svg.png?resize=80,80&class=imgcenter)](https://f-droid.org/en/packages/org.disroot.disrootapp/) [F-droid](https://f-droid.org/en/packages/org.disroot.disrootapp/?class=lighter)

---

## Application Disroot
Cette application est comme votre couteau suisse de la plateforme **Disroot**, faite par la communauté pour la communauté. Si vous n'avez pas de compte **Disroot**, vous pouvez quand même utiliser cette application pour accéder à tous les services **Disroot** qui ne nécessitent pas de compte, comme etherpad, upload, etc.
