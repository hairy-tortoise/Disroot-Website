---
title: 'Disroot App'
bgcolor: '#1F5C60'
fontcolor: '#fff'
text_align: left
wider_column: right
---

<br>

## Get it on  [![F-droid](F-Droid.svg.png?resize=80,80&class=imgcenter)](https://f-droid.org/en/packages/org.disroot.disrootapp/) [F-droid](https://f-droid.org/en/packages/org.disroot.disrootapp/?class=lighter)

---

## Disroot app
This app is like your Swiss Army knife to the **Disroot** platform, made by the community for the community. If you don’t have a **Disroot** account you can still use this app to access all the **Disroot** services that do not require an account, like etherpad, upload, etc.
