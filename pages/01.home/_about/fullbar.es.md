---
title: Disroot es
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
text_align: center
body_classes: modular
---

<br>
## Disroot es una plataforma que provee servicios en línea, basada en los principios de libertad, privacidad, federación y descentralización.
**¡Sin rastreadores, sin publicidad, sin análisis de perfil, sin extracción de datos!**
