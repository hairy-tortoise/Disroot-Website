---
title: privacy
fontcolor: '#555'
wider_column: left
bgcolor: '#fff'

---

# Privacidad

El software se puede crear y darle cualquier forma. Cada botón, cada color y cada enlace que vemos y utilizamos en la web fue puesto ahí por alguien. Cuando hacemos uso de las aplicaciones que nos ofrecen, generalmente no vemos (y muchas veces, no nos interesa) lo que ocurre detrás de la interfaz que utilizamos. Nos conectamos con otra gente, almacenamos nuestros archivos, organizamos reuniones y festivales, enviamos correos o chateamos durante horas y todo sucede "mágicamente".
En las últimas décadas la información se ha vuelto muy valiosa y cada vez más fácil de recopilar y procesar. Estamos acostumbradxs a ser analizadxs, aceptando ciegamente términos y condiciones por "nuestro propio bien", confiando en autoridades gubernamentales y compañías multimillonarias para que protejan nuestros intereses, mientras que en realidad todo el tiempo somos productos en sus 'granjas de personas'.

**Se dueñx de tu información:**
Muchas redes usan tus datos para hacer dinero analizando tus interacciones y utilizando esta información para publicitarte cosas. **Disroot** no utiliza tus datos con ningún otro propósito que no sea permitir conectarte y usar el servicio.
Tus archivos en la nube están cifrados con tu contraseña de usuarix, cada [pastebin](https://es.wikipedia.org/wiki/Pastebin) y archivo subido al servicio de **Lufi** está cifrado del lado del cliente también, lo que significa que ni siquiera los administradores del sistema tienen acceso a tus datos. Siempre que exista una posibilidad para cifrar, la habilitamos y si no es posible, aconsejamos utilizar un software de cifrado externo. Cuanto menos sepamos, como administradores, sobre tu información, mejor :D. (Consejo del día: *¡Nunca pierdas tu contraseña!)* [Ver tutorial](https://howto.disroot.org/es/tutorials/user/account/administration/ussc)

---

<br>
![](priv1.jpg?lightbox=1024)
![](priv2.jpg?lightbox=1024)
