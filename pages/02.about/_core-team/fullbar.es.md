---
title: 'Green bar'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: center
---


## Equipo de Disroot

**Antilopa**, **Muppeth**, **Fede**, **Meaz**, **Avg_Joe**.

![antilopa](antilopa.png "Antilopa") ![muppeth](muppeth.png "Muppeth") ![fede](fede.png "Fede") ![meaz](meaz.png "Meaz") ![avg_joe](avg_joe.png "Avg_Joe")
