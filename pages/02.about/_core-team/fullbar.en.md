---
title: 'Green bar'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: center
---


## Disroot Team

**Antilopa**, **Muppeth**, **Fede**, **Meaz**, **Avg_Joe**.

![antilopa](antilopa.png "Antilopa") ![muppeth](muppeth.png "Muppeth") ![fede](fede.png "Fede") ![meaz](meaz.png "Meaz") ![avg_joe](avg_joe.png "Avg_Joe")


## Translations team
The Italian translation is the work of **@l3o**, French, Spanish and German are taken care by the **Core team**.
*If you're interested in helping us with this work, contact us [here](https://git.disroot.org/Disroot/Disroot-Project/issues/148)*
