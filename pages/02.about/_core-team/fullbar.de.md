---
title: 'Green bar'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: center
---


## Disroot Team

Disroot wurde von **Antilopa** und **Muppeth**  2015 gegründet.
Im Juli 2019 hat sich das Team um **Fede** und **Meaz** erweitert.

![antilopa](antilopa.png "Antilopa") ![muppeth](muppeth.png "Muppeth") ![fede](fede.png "Fede") ![meaz](meaz.png "Meaz")
