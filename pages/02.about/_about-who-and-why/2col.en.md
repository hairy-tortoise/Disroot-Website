---
title: Email
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
body_classes: modular
wider_column: right
---

<a class="button button1" href="/mission-statement">Read our mission statement</a>

---

**Disroot** is a project that was born from the personal need to communicate, share and organize ourselves within our circles through tools that met a series of fundamental criteria for us: they had to be **open**, **decentralized**, **federated** and above all **respectful** towards **freedom** and **privacy**. None of the most popular solutions complied with them, but in our search we found some very interesting projects that did. And since we thought that they should be available to all those with similar ethical principles, we decided not only to group these applications and share them with others, but also to manage them according to those values. That's how **Disroot** started.


The project is based in Amsterdam, was founded by **Muppeth** and **Antilopa** in 2015, it is maintained by volunteers and relies on the support of its community. Between 2017 and 2018, a group of members started to get very actively involved and to collaborate very closely. In 2019, two of them, **Fede** and **Meaz**, officially became part of the Core Team that manages the platform. Two years later, in 2021, **Avg_Joe** joined the tasks and responsibilities.


**Disroot** aims to change the way people are used to interact on the web. We want to encourage and show them not only that there are open and ethical alternatives but also that it is the only possible way to break free from the *walled gardens* proposed and promoted by proprietary software and corporations, either through our platform, others that have similar values and goals or even their own projects.


We are convinced that we can build together a truly independent network, focused on the benefit of the people and not on their exploitation.
