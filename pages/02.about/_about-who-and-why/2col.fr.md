---
title: Email
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
body_classes: modular
wider_column: right
---

<a class="button button1" href="/mission-statement">Lire notre Declaration de Mission</a>

---

**Disroot** est un projet basé à Amsterdam, il est géré par des bénévoles et dépend du soutien de sa communauté.


A l'origine, nous avons créé **Disroot** par besoin personnel, nous recherchions des logiciels que nous pouvions utiliser pour communiquer, partager et organiser au sein de nos cercles. La plupart des solutions disponibles manquaient les éléments clés que nous trouvons importants. Nos outils devraient être **ouverts**, **décentralisés**, **fédérés** et **respectueux** de la **liberté** et de la **vie privée**.

En cherchant de tels outils, nous avons trouvé des projets vraiment intéressants - des projets qui, à notre avis, devraient être accessibles à tous ceux qui valorisent des principes similaires. Nous avons donc décidé de regrouper certaines applications et de les partager avec d'autres. C'est ainsi que **Disroot** a commencé.

En lançant **Disroot**, nous espérons changer la façon dont les gens interagissent généralement sur le web. Nous voulons encourager les gens à **se libérer** des jardins murés des logiciels populaires et à se tourner vers des alternatives ouvertes et éthiques, que ce soit sur notre plateforme ou sur une autre (ou vous pourriez même héberger la vôtre).

Ensemble, nous pouvons former un réseau véritablement indépendant, centré sur le bénéfice des gens plutôt que sur leur exploitation.
