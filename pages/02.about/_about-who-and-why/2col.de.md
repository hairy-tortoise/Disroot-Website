---
title: Email
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
body_classes: modular
wider_column: right
---

<a class="button button1" href="/mission-statement">Unser Leitbild</a>

---

**Disroot** ist ein in Amsterdam ansässiges Projekt, welches durch Freiwillige unterhalten wird und auf die Unterstützung seiner Gemeinschaft angewiesen ist.


Ursprünglich haben wir **Disroot** aufgrund unseres persönlichen Bedarfs erschaffen. Wir waren auf der Suche nach einer Software, mit der wir innerhalb unseres Umfelds kommunizieren, teilen und organisieren konnten. Den meisten verfügbaren Lösungen fehlten die grundlegenden Prinzipien, die uns wichtig sind; unsere Programme sollten **offen**, **dezentralisiert** und **föderalistisch** sein und **Freiheit** und **Datenschutz** absolut **respektieren**.

Während wir nach solchen Programmen gesucht haben, fanden wir einige wirklich interessante Projekte. Projekte von denen wir denken, dass sie jedem zur Verfügung stehen sollten, der die gleichen Prinzipien wertschätzt wie wir. Deswegen haben wir beschlossen, einige Programme zu bündeln und sie mit Anderen zu teilen. **Disroot** war geboren.

Durch **Disroot** hoffen wir, die Art, wie Menschen sich alltäglich im Internet begegnen, grundlegend zu ändern. Wir wollen die Menschen ermutigen, aus den "Walled Gardens", den geschlossenen Systemen der etablierten Software **auszubrechen** und sich offenen und ethischen Alternativen zuzuwenden. Egal, ob auf unserer Plattform oder einer anderen (oder Du bietest vielleicht sogar irgendwann selbst eine an).

Zusammen können wir ein Netzwerk erschaffen, dass wirklich unabhängig ist, ausgerichtet auf den Vorteil der Menschen anstatt auf ihre Ausbeutung.
