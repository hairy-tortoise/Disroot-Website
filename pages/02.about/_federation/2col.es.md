---
title: Federation and Decentralization
fontcolor: '#555'
wider_column: right
bgcolor: '#fff'

---

<br>
<br>
![](about-organize_es.jpg)

---

<br>
# Federación y Descentralización

La mayoría de los servicios de internet funcionan desde un punto centralizado, controlado y operado por corporaciones. Estas almacenan información privada de sus "usuarios" y "usuarias" y la analizan utilizando algoritmos avanzados con el fin de crear perfiles precisos de ellxs. Esta información es usada habitualmente para aprovecharse de la gente en beneficio de los anunciantes. Son datos que también pueden obtener instituciones gubernamentales o hackers malintencionados. La información incluso puede ser eliminada sin aviso, por razones dudosas o por políticas regionales de censura.

Un servicio descentralizado puede residir en varias máquinas, propiedad de diferentes individuos, compañías u organizaciones. Con protocolos de federación, esas instancias pueden interactuar y formar una red de muchos nodos (servidores que hospedan servicios similares). Se podría dar de baja un nodo pero nunca la red entera. Con este tipo de organización, la censura es prácticamente imposible.

Piensa en cómo utilizamos el correo electrónico: puedes elegir cualquier proveedor de servicio o armar el tuyo propio, y aún así intercambiar correos con personas que usen otro proveedor de correo. El correo electrónico está construido sobre un protocolo descentralizado y federado.

Estos dos principios juntos, constituyen la base de una enorme red que depende de una estructura bastante simple y con costos relativamente bajos. Cualquier máquina puede convertirse en un servidor y ser un participante semejante en la red. Desde una pequeña computadora de escritorio en tu casa hasta un servidor dedicado o racks con múltiples servidores. Este abordaje proporciona la posibilidad de crear la más grande red global perteneciente a lxs usuarixs. Tal como se concibió que fuera la Internet.

No deseamos que **Disroot** se convierta en una entidad centralizada sino una parte de una comunidad más grande, un nodo entre muchos. Esperamos que otras personas se sientan inspiradas y estimuladas a crear más proyectos con similares intenciones.
