---
title: 'Schritt 2'
bgcolor: '#1F5C60'
fontcolor: '#fff'
wider_column: right
---

# Schritt 2:
## Richte Dein DNS ein

Wenn Du Deine eigene Domain hast, musst Du zwei Dinge erledigen, um sie zu disroot.org zu verlinken.
*(Die folgenden Schritte werden in dem von Deinem Domainanbieter bereitgestellten Interface erledigt -DNS-Record hinzufügen-)*:


---      

## 1: Dein Eigentum der Domain nachweisen
Erstelle einfach einen **TXT** Eintrag mit: **disroot.org-deinedomain.tld** (selbstverständlich ersetzt Du `deinedomain.tld` durch **Dein Domain Name**). Das wird uns beweisen, dass Du die Domain in der Tat besitzt.

## 2: Adressiere Deine Domain auf disroot.org
**1.** Setze den MX Eintrag auf **disroot.org.** **`(Vergiss nicht den abschließenden Punkt!)`**
**2.** Setze **"v=spf1 mx a ptr include:disroot.org -all"** Das ermöglicht es Dir, Emails sowohl von Deinen eigenen Servern als auch von **Disroot** zu senden. Das ist lediglich eine Standardeinstellung, die für die meisten Szenarios anwendbar sein sollte. Wenn Du mehr über SPF-Einträge erfahren und Deinen eigenen optimieren möchtest, schau im Web nach mehr Infos (z.B. [diese Seite](https://www.dmarcanalyzer.com/spf/how-to-create-an-spf-txt-record/)).
