---
title: 'Dominio personalizado'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Dominio personalizado

---      

<br>
**Disroot** ofrece una opción de dominio personalizado para el correo y el chat XMPP. Esto significa que puedes utilizar los servicios de **Disroot** con tu propio dominio personal privado para enviar y recibir correos o chatear usando un dominio como `nombre@tudominio.net`, por ejemplo.

Esta funcionalidad está disponible para cualquiera que decida donar el equivalente a, por lo menos, 12 tazas de café (u otra bebida de tu elección) por única vez o anualmente (tú decides).

**✨ ¡La vinculación de dominio personalizado es de por vida! ✨**
Siempre y cuando **Disroot** esté vivo...
<p>
