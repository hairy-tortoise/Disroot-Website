---
title: 'Dominio personalizzato'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Dominio personalizzato

---      

<br>
**Disroot** offre la possibilità di associare un dominio personalizzato per e-mail e chat XMPP/Jabber. Ciò significa che puoi utilizzare i servizi **Disroot** con il tuo nome di dominio privato per inviare e ricevere e-mail o chattare con un dominio come ad esempio: "nome@tuodominio.net". 

Questa funzione è disponibile per chiunque decida di donare l'equivalente di almeno 12 caffè (o altra bevanda a scelta) almeno una volta. 

**✨ Il collegamento al dominio personalizzato è per sempre! ✨**
Fin quando **Disroot** esisterà...
<p>
