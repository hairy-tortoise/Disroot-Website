---
title: 'Extra storage'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# More storage for mail and/or cloud

![email](logo_email.png?resize=150) ![cloud](logo_cloud.png?resize=150)

---      

<br>
With your **Disroot** account, you get FREE storage: 1GB for your emails, 2GB for the cloud. However, we offer the possibility to extend this.

Here are the prices **per year, payment fees included**:

||||
|---:|---|---:|
| 5GB |......| 11€ |
| 10GB |......| 20€ |
| 15GB |......| 29€ |
| 30GB |......| 56€ |
| 45GB |......| 83€ |
| 60GB |......| 110€ |


<br>
Transactions within the EU are subjected to extra VAT (Value Added Tax) of 21%.

You can decide to allocate this extra storage as you wish between mail and cloud. For example, if you get the 10GB storage, you could decide to have 8GB for cloud, and 2GB for mail.

<a class="button button1" href="/forms/extra-storage-space">Request Extra Storage</a>
