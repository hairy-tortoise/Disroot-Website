---
title: 'Passo 1'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Passo 1:
## Ottieni il tuo dominio



---      

<br>
Se non hai ancora un tuo dominio, devi prima procurartelo. Al momento **Disroot** non fornisce la funzione di acquisto di domini. Ci sono comunque molti provider dai quali puoi acqustarne uno. Ti suggeriamo di provare a scegliere qualche provider di domini locale, etico e di piccole dimensioni invece di andare a fare shopping da grandi aziende.
Sostieni la tua economia locale. 