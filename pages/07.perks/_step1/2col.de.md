---
title: 'Schritt 1'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Schritt 1:
## Hole Dir eine Domain



---      

<br>
Wenn Du bis jetzt noch keine hast, musst Du Dir zunächst eine besorgen. Derzeit bietet **Disroot** nicht die Möglichkeit, eine Domain zu kaufen. Allerdings gibt es da draußen Unmengen an Domainanbietern, so dass Du einfach ein bisschen suchen und shoppen gehen kannst. Wir empfehlen einen eher regionalen, ethischen, kleinen Domainanbieter zu wählen anstatt zu einem Big Business Anbieter zu gehen.
Unterstütze Deine lokale Wirtschaft!
