---
title: Tor
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://www.torproject.org/download/" target="_blank">Ottieni il Tor Browser</a>

---

![](Tor_Logo.png)


## Accedere al sito web di Disroot attraverso Tor

È possibile accedere al sito web di **Disroot** attraverso Tor utilizzando Tor Browser. Tor impedisce a chi osserva la vostra connessione di sapere che state visitando il sito web di Disroot.

È sufficiente copiare e incollare questo indirizzo .onion nel browser Tor:  
**j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion** o se si utilizza già il browser Tor, fare clic [qui](http://4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion)

Pagina iniziale del progetto Tor: [https://www.torproject.org/](https://www.torproject.org/)
