---
title: Tor
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://www.torproject.org/download/" target="_blank">Get Tor Browser</a>

---

![](Tor_Logo.png)


## Access Disroot's website through Tor

You can access **Disroot's website** through Tor using Tor Browser. Tor prevents someone watching your connection from knowing that you're visiting Disroot's website. 

Simply copy and paste this .onion address into Tor Browser: **j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion** or if you're already using Tor browser, click [here](http://4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion)


Project Homepage: [https://www.torproject.org/](https://www.torproject.org/)