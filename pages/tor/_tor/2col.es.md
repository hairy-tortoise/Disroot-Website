---
title: Tor
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://www.torproject.org/download/" target="_blank">Descarga el navegador Tor</a>

---

![](Tor_Logo.png)


## Accede al sitio de Disroot a través de Tor

Puedes acceder al **sitio de Disroot** a través de Tor utilizando su navegador. Tor previene que alguien monitoreando tu conexión sepa que estás visitando el sitio de Disroot. 

Simplemente copia y pega esta dirección .onion en el navegador Tor: **j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion** o si ya utiliza el navegador Tor, haga clic [aquí](http://4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion)

Sitio del proyecto: [https://www.torproject.org/](https://www.torproject.org/)