---
title: 'Objectif'
bgcolor: '#09846aff'
fontcolor: '#FFFFFF'
text_align: left
---

<br>


# Guide rapide :
1. Pour commencer, installez Teeworlds soit en utilisant votre gestionnaire de paquets si vous utilisez un système d'exploitation correct comme linux/bsd, soit en allant sur teeworlds [site web] (https://teeworlds.com/?page=downloads) et en le téléchargeant pour votre plate-forme.
2. Une fois que vous avez terminé, démarrez le jeu.
3. Sélectionnez d'abord "Setup" pour ajouter le nom de votre joueur, puis choisissez et personnalisez votre Tee selon vos goûts.
4. Cliquez sur "Jouer" et trouvez **"Quarantine with Disroot "** et rejoignez-nous.

![](splashtee3.png)
