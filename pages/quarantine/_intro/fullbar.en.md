---
title: 'Are you bored yet'
bgcolor: '#7b384fff'
fontcolor: '#FFF'
text_align: left
---

<br>

<span style="padding-right:10%;">
## Are you bored yet?

Since lots of you (us included) are locked in their houses trying to avoid contact with other human beings, we thought it would be nice to spend this time socializing in front of computer with bunch of strangers you know so well from chats, social federated networks and whom you most probably never seen in real life.

Let's spend an evening drinking booze, eating junk, and shooting one another on Teeworlds. The first edition of Quarantine with Disroot starts on Saturday 21.03.2020 20:00 CET and will go on until last one standing. Server will probably be on for the whole week or two if there are people willing to play.

</span>

<span style="padding-right:30%;">
</span>
