---
title: Quarantaine
bgcolor: '#50162D'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _title
            - _intro
            - _goal
            - _rewardstitle
body_classes: modular
header_image: 'header.jpg'
---
