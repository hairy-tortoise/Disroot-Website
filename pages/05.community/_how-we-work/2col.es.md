---
title: 'Cómo puedo ver'
published: true
bgcolor: '#FFF'
fontcolor: '#555'
---

![](puzzle-piece.png?resize=45,45)
# ¿Cómo trabajamos?

Usamos un tablero de proyecto donde ponemos todas las cosas que necesitan hacerse. Hemos elegido una "metodología ágil" como punto de partida y trabajamos en "sprints" (carreras cortas), que son períodos cortos de tiempo. En nuestro caso, cada "sprint" dura cuatro semanas. Al principio del "sprint", nos sentamos juntxs y decidimos cuáles tareas tienen prioridad y deberían ser agregadas a la pizarra de tareas en curso. Luego, dentro del período de cuatro semanas, hacemos nuestro mejor esfuerzo para lograr todo lo que podamos. Una vez cumplido el plazo, cerramos el "sprint", movemos las tareas sin terminar de vuelta al montón de pendientes, y publicamos un mensaje a través de todos los canales (Mastodon, publicación en el blog) acerca de los cambios recientes y nuevos progresos en la plataforma **Disroot**.

Adicionalmente, una vez al año enviamos nuestro **Reporte Anual**, que es un resumen de lo que se ha hecho en el año que pasó, cuáles fueron los costos e ingresos de todo el año, y nuestro presupuesto estimado y planes para el próximo año.

---

![](eye.png?resize=45,45)
# ¿Cómo puedo ver lo que está sucediendo?

La mejor manera de ver con qué estamos ocupados actualmente, qué hemos hecho hasta el momento y qué estamos planificando para el futuro, es seguir nuestro tablero de proyecto. 

[Tablero de proyecto de Disroot](https://board.disroot.org/project/disroot-disroot) | [Progreso actual](https://board.disroot.org/project/disroot-disroot/kanban) |
[Lista de incidentes](https://board.disroot.org/project/disroot-disroot/issues)
