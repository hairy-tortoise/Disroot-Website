---
title: Contribuir
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
published: true
---

![](bug.png?resize=45,45&classes=pull-left)
# ¿Cómo puedo ayudar?

Hay muchas maneras de involucrarse y contribuir al proyecto **Disroot**.
