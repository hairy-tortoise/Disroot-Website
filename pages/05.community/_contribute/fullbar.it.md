---
title: Contribute
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
published: true
---

![](bug.png?resize=45,45&classes=pull-left)
# Come posso aiutare?

Ci sono molti modi per diventare attivi e contribuire al progetto Disroot.
