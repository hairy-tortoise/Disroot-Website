---
title: 'How can I help'
bgcolor: '#FFF'
fontcolor: '#555'
---

### Submit and track bugs
It doesn't matter if you've been using **Disroot** for some time or if you've just joined, reporting bugs is something you can always do. So if you discover something that doesn't look quite right or doesn't function as it should, let us know by making an issue on the [**Disroot-Project repository**](https://git.disroot.org/Disroot/Disroot-Project/issues).

### Submit features
You can also use the [**Disroot-Project repository**](https://git.disroot.org/Disroot/Disroot-Project/issues) to let us know which new features you would like to see in the future.

![](theme://images/logo_git.png?resize=35,35) [Disroot-Project](https://git.disroot.org/Disroot/Disroot-Project/?classes=button5)

---

### Help solve other peoples issues
If you're using **Disroot** for some time now, you're familiar with the platform or had to solve some issues before, then you can be of huge help to others who are struggling to find their way around all the features. It is also a great help for us if more Disrooters help answering questions and solving minor problems for others, meaning that the workload is better spread and replies can appear faster. Start watching our **XMPP room** to see how you can help if anyone need it.

![](theme://images/logo_chat.png?resize=35,35) [disroot@chat.disroot.org](xmpp:disroot@chat.disroot.org?classes=button5)

---

### Help us with howto's
We have already written and translated quite some [guides and tutorials](https://howto.disroot.org) to help users learn how to use the services we provide, but there's still a long way to go to cover all of its aspects, for all possible devices and operating systems. We need more people to get involved in writing and translating howtos if we want to create a universal documentation that can be used by our users and those of other platforms that provide similar services and that are also, as much as possible, in their own languages.

![](theme://images/logo_h2.png?resize=35,35) [Disroot Howto Project](https://howto.disroot.org?classes=button5)
![](theme://images/logo_chat.png?resize=35,35) [howto@chat.disroot.org](xmpp:howto@chat.disroot.org?classes=button5)

### Donate money
And last but not least, financial support is always needed, so don't be afraid throwing a briefcase full with dollars our way. And if that's too much, then remember that every penny counts and if you decide to "buy us coffee" on regular basis, it would be awesome. For more details, check [here](https://disroot.org/donate)

---
