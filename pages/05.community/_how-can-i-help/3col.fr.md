---
title: 'Comment puis-je aider'
bgcolor: '#FFF'
fontcolor: '#555'
---

### Soumettre et suivre les bogues
Peu importe si vous utilisez **Disroot** depuis longtemps ou si vous venez juste de le rejoindre, rapporter des bogues est quelque chose que vous pouvez faire. Donc, si vous découvrez quelque chose qui n'a pas l'air normal ou qui ne fonctionne pas comme il se doit - faites-le nous savoir en signalant le problème sur le [dépôt Disroot](https://git.disroot.org/Disroot/Disroot-Project/issues).

### Soumettre des fonctionnalités
Vous pouvez également utiliser le [dépôt Disroot](https://git.disroot.org/Disroot/Disroot-Project/issues) pour nous faire part de nouveautés que vous aimeriez voir à l'avenir.

![](theme://images/logo_git.png?resize=35,35) [dépôt Disroot](https://git.disroot.org/Disroot/Disroot-Project/?classes=button5)


---

### Aider à résoudre les problèmes des autres gens
Si vous utilisez **Disroot** depuis un certain temps maintenant, que vous êtes familier avec la plate-forme ou que vous avez dû déjà résoudre certains problèmes, alors vous pouvez être une aide énorme pour d'autres qui ont des difficultés à comprendre toutes les fonctionnalités. C'est aussi une aide énorme pour nous si plus de Disrooters prennent le temps de répondre aux questions et aident les autres à résoudre de petits problèmes. Cela signifie que la charge de travail sera mieux répartie et les réponses pourront apparaître plus rapidement. Commencez à regarder notre **Salle XMPP** pour voir si quelqu'un a besoin d'aide.

![](theme://images/logo_chat.png?resize=35,35) [disroot@chat.disroot.org](xmpp:disroot@chat.disroot.org?classes=button5)

---

### Aidez-nous avec les tutoriels
Nous avons déjà écrit de nombreux [guides et tutoriels](https://howto.disroot.org) pour aider les gens à apprendre à utiliser les services que nous proposons, mais il reste encore un long chemin à parcourir avant que nous parvenions à tout couvrir, pour tous les périphériques et systèmes d'exploitation possibles. Nous avons besoin de plus de personnes impliquées à écrire et à traduire les tutoriels si nous voulons créer une documentation universelle qui pourrait être utilisée par nos utilisateurs et ceux des autres plateformes qui fournissent des services similaires, et qui soit, autant que possible, dans leur langue.

![](theme://images/logo_h2.png?resize=35,35) [Projet Disroot Howto](https://howto.disroot.org?classes=button5)
![](theme://images/logo_chat.png?resize=35,35) [howto@chat.disroot.org](xmpp:howto@chat.disroot.org?classes=button5)

### Donner de l'argent
Enfin et surtout, un soutien financier est toujours nécessaire, alors n'ayez pas peur de jeter une mallette pleine d'argent sur notre chemin. Et si c'est trop, n'oubliez pas que chaque centime compte et si vous décidez de nous "acheter du café"" régulièrement, ce serait génial. Pour plus de détails, consultez [ici](https://disroot.org/donate)

---
