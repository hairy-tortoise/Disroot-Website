---
title: 'Wie kann ich helfen'
bgcolor: '#FFF'
fontcolor: '#555'
---

### Bugs aufspüren und melden
Es ist egal, ob Du **Disroot** nun seit einer Weile benutzt oder neu dazugestoßen bist. Bugs mitzuteilen ist etwas, das Du immer tun kannst. Wenn Du also etwas entdeckst, das irgendwie nicht richtig zu sein scheint oder nicht so funktioniert, wie es sollte, lass es uns wissen, indem Du eine Fehlermeldung im [**Disroot-Projekt repository**](https://git.disroot.org/Disroot/Disroot-Project/issues) erstellst.

### Funktionen vorschlagen
Du kannst das [**Projekt-Projekt repository**](https://git.disroot.org/Disroot/Disroot-Project/issues) nutzen um uns mitzuteilen, welche Funktionen Du zukünftig gerne bei **Disroot** sehen möchtest.

![](theme://images/logo_git.png?resize=35,35) [Disroot-Projekt](https://git.disroot.org/Disroot/Disroot-Project/?classes=button5)

---

### Hilf anderen bei der Problemlösung
Wenn Du **Disroot** nun schon für einige Zeit nutzt, bist Du mit der Plattform vertraut oder hattest auch schon einige Probleme zu lösen. Dadurch kannst Du anderen Nutzern eine große Hilfe sein, die damit kämpfen, sich in all den Funktionen zurechtzufinden. Für uns ist es zudem eine große Hilfe, wenn viele Disrooter helfen, Fragen zu beantworten und kleinere Probleme für andere zu lösen. Denn dadurch wird die Arbeitsbelastung besser verteilt und Anfragen werden schneller beantwortet. Fang an [**XMPP-Raum**](https://webchat.disroot.org/#converse/room?jid=disroot@chat.disroot.org) zu beobachten um zu sehen, ob Du helfen kannst, wenn jemand Hilfe braucht.

![](theme://images/logo_chat.png?resize=35,35) [disroot@chat.disroot.org](xmpp:disroot@chat.disroot.org?classes=button5)

---

### Hilf uns mit How-To's
Wir haben schon einige [Anleitungen und Tutorials](https://howto.disroot.org) geschrieben und übersetzt, um unseren Nutzern zu helfen, die Nutzung der von uns angebotenen Dienste zu erlernen, aber es ist noch ein langer Weg, alle Aspekte abzudecken, für alle möglichen Geräte und Betriebssysteme. Wir brauchen noch mehr Leute, die sich am Schreiben und Übersetzen der How-To's beteiligen, wenn wir eine universelle Dokumentation erschaffen wollen, die durch unsere Nutzer genutzt werden kann, aber auch durch Nutzer anderer Plattformen, welche ähnliche Dienste wie wir anbieten.

![](theme://images/logo_h2.png?resize=35,35) [Disroot Howto Projekt](https://howto.disroot.org?classes=button5)
![](theme://images/logo_chat.png?resize=35,35) [howto@chat.disroot.org](xmpp:howto@chat.disroot.org?classes=button5)

### Spende Geld
Nicht zuletzt wird natürlich immer finanzielle Unterstützung benötigt, also scheue Dich nicht, einen Sack voll Geld in unsere Richtung zu werfen. Und wenn Dir das zu viel ist, denk daran, dass jeder Cent zählt. Und wenn Du entscheidest, uns regelmäßig "einen Kaffee zu kaufen", wäre das phantastisch. Weitere Informationen findest Du [hier](https://disroot.org/donate)

---
