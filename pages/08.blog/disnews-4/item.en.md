---
title: 'DisNews #4 - Nextcloud performance; Framadate and Ethercalc; Custom domains and XMPP; Lacre.io; fe.disroot.org'
date: '29-12-2021'
media_order: 2022.jpg
taxonomy:
  category: news
  tag: [disroot, news, nextcloud, etherpad, framadate, cryptpad, lacre,]
body_classes: 'single single-post'

---

Hi there Disrooters!


We would like to wish you all a happy new year! Let's not mention how crazy the world has become and how gloomy the future is shaping up to be. Let's focus on the good things in life instead. Imagine yourself sitting in a hot-tub outside (or a cold pool if you happen to be in the Southern Hemisphere now). You're enjoying your favorite drink while a fresh breeze touches your face, you open your eyes and you see the people that are the closest to you, celebrating the last minutes of the year with you. The time seems to stop in that moment and none of your worries, your bad thoughts, seem to be real. You take your last sip of 2021 and before you open your eyes you are in 2022. A year you look forward to, the year you will finally kick out that bad habit of yours you always wanted to get rid of, the year you will finally realize that cool project you always imagined, the year government restrictions become a thing of the past and everyone is smiling to you on the streets and greeting each other "good morning"...

**That is what we wish to you all! Let's make 2022 awesome again!**

Alright. Although, our last newsletter was not so long ago, there are some little things we would like to highlight.


# Nextcloud performance hit

For few weeks Nextcloud has been slower than normal slow. We have since detected the issue and fixed it so that now things should be back to normal slow. At the same time we are working on improving the performance of the cloud service so that it will be again more enjoyable to work on. We are getting ready to purchase new servers to power our infrastructure which will definitely be a big boost, but until then we want to make sure we squeeze as much out of current hardware as possible to ensure that the performance of the cloud is as optimized as it can be.


# Future of the Framadate/Ethercalc

In the last post we have informed you about our concerns with **Framadate** and **EtherCalc** not being actively developed for a while and the idea of removing those services in favor of **CryptPad** which does provide similar solution with benefit of them being end-to-end encrypted and very actively developed. The crushing majority of you supports that idea and so, by end of March 2022, we will shut down both **[Ethercalc](https://calc.disroot.org)** and **[Framadate](https://poll.disroot.org)** and recommend CryptPad as a solution to collaborative spreadsheets and polls with extra security sauce on top. And since we are talking about it, we would like to invite you to check all the services bundled into CryptPad more closely. CryptPad is, in our opinion, a great way to collaborate with others, whether it's a text document, spreadsheet, project board or presentation. Additionally, it's an end-to-end encrypted software that keeps the content of all your files private and not even us, the admins, can read them.


# Custom domains and XMPP

We are very surprised and grateful for the responses from all of you. The requests for custom domain linking for email keep pouring in, and we are happy you guys are going for it. After all, we think it's the best option to keep your email address/internet presence without need to worry about the provider you use, as you can move it anywhere you want without worrying about informing all your contacts about the change.

Although we also provide XMPP chat domain linking, we haven't processed any of those yet (priorities), but we will be contacting all of you who have requested those soon, so stay tuned!


# Glimps of the future

## Lacre.io

Although we haven't been able to work on the mail encryption project as much as we would like and only started recently, we want to finalize it in the coming year. @pfm has been doing magic and working on getting the software up to date and usable so we are almost ready to put it to some closed alpha tests very, very soon. We are really looking forward to it.


## Fe.disroot.org - A new seed in the fediverse.

For those who do not know yet, the Fediverse is a network of federated services, ranging from social networks, file sharing, image boards and so on. It's great network where no single entity owns all resources and user's data. It's a decentralized network that promotes freedom and cooperation between different entities to create truly independent self-managed network of ethical service providers.

One of Disroot's main goals is providing federated services and so entering ActivityPub protocol (main driving force behind the Fediverse) is something we wanted to do for a while. We have been testing out many possible software solutions for a while now. Although we think **Hubzilla** is the most advanced and privacy focused one out there, it is also very demanding for us to maintain it, and to make it user-friendly. We have decided to take a different approach. Instead of using one piece of software to do all things, we decided to look into different federated services and use number of those that do one thing. And so we have started with microblogging platform (like Mastodon or Twitter) and went for **Pleroma**. It is still very much in test phase, but it is usable enough to jump on and test it out. If you are already familiar with the Fediverse we could really use your feedback to catch all the small issues and bugs before we promote it for regular use. In the meantime we are preparing all the needed tutorials and website information to make the onboarding of Disrooters that are new to the concept of Fediverse as easy as possible. If you are interested in testing things out you can log in to our Pleroma instance with your Disroot's credentials at: **https://fe.disroot.org**. If you want to participate in discussions about future fediverse apps we want to provide you can join our federated chat at xmpp:fedisroot@chat.disroot.org .


**Once again: have a great new year celebration and we wish you all a very awesome 2022!**

Special thanks to all the Disrooters that have been helping us by reporting issues, donating, hanging out on our chat (disroot@chat.disroot.org), those who were helping others with Disroot related problems, all those who submitted patches, translations and tutorials! You are all awesome!

Disroot would not exist without the tireless work of all FLOSS developers either. We would like to thank you all for your hard work! Everyone involved in creating Linux kernel, GNU utilities, Debian and all the other distributions (also Arch :P), all those behind projects we use on daily basis at Disroot: **Nginx, Debian, CryptPad, Framasoft, Mumble, Pleroma, Soapbox, Hubzilla, Etherpad, Ethercalc, Taiga, Nextcloud, Lufi, ConverseJS, Privatebin, Searx, Jitsi, Gitea, Prosody, Mattermost, Postfix, Dovecot, Amavis, Rainloop and many many more...** You are the heroes! Happy new year!
