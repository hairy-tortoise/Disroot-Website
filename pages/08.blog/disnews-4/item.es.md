---
title: 'DisNews #4 - Performance de Nextcloud; Framadate y Ethercalc; Dominios personalizados y XMPP; Lacre.io; fe.disroot.org'
date: '29-12-2021'
media_order: 2022.jpg
taxonomy:
  category: news
  tag: [disroot, news, nextcloud, etherpad, framadate, cryptpad, lacre,]
body_classes: 'single single-post'

---

¡Hola, Disrooters!


¡Nos gustaría desearles a todxs un feliz año nuevo! No mencionemos lo loco que se ha vuelto el mundo y lo sombrío que se perfila. En vez de eso, enfoquémonos en las cosas buenas de la vida. Imagínense en un jacuzzi afuera (o en una piscina fresca si están ahora en el hemisferio sur). Están disfrutando de su bebida favorita mientras una brisa fresca les toca la cara, abren los ojos y ven a las personas más cercanas, celebrando con ustedes los últimos minutos del año. El tiempo parece detenerse en ese momento y ninguna de sus preocupaciones, sus malos pensamientos, parecen ser reales. Beben el último sorbo de 2021 y antes que abran los ojos están en 2022. Un año que esperan con ansias, el año en el que por fin se sacarán ese mal hábito del que siempre quisieron deshacerse, el año en el que finalmente realizarán ese proyecto buenísimo que siempre imaginaron, el año en el que las restricciones del gobierno se convertirán en cosa del pasado y todo el mundo les sonreirá por la calle y se saludarán con un "buenos días"...

**¡Eso es lo que les deseamos a todxs! ¡Hagamos que 2022 sea genial de nuevo!**

Muy bien. Aunque nuestro último boletín no fue hace mucho, hay algunas cositas que nos gustaría resaltar.


# La crítica performance de Nextcloud

Durante algunas semanas Nextcloud ha estado más lento de lo normal. Desde entonces hemos detectado el problema y lo hemos solucionado, por lo que ahora las cosas deberían volver a la lentitud normal. Al mismo tiempo, estamos trabajando en mejorar el rendimiento del servicio en la nube para que vuelva a ser más agradable trabajar con él. Nos estamos preparando para comprar nuevos servidores para potenciar nuestra infraestructura, lo que sin duda será un gran impulso, pero hasta entonces queremos asegurarnos de exprimir al máximo el hardware actual para garantizar que el rendimiento de la nube esté lo más optimizado posible.


# El futuro de Framadate/Ethercalc

En el último post les informamos de nuestra preocupación por el hecho de que Framadate y EtherCalc no se hayan estado desarrollando activamente desde hace un tiempo y de la idea de eliminar esos servicios en favor de CryptPad, que proporciona una solución similar con el beneficio de que están cifrados de extremo-a-extremo y desarrollados muy activamente. La inmensa mayoría de ustedes apoya esta idea así que, a finales de marzo de 2022, cerraremos tanto [Ethercalc](https://calc.disroot.org) como [Framadate](https://poll.disroot.org) y recomendaremos CryptPad como solución para las hojas de cálculo colaborativas y las encuestas con una salsa de seguridad extra. Y ya que estamos hablando de ello, nos gustaría invitarlxs a comprobar más de cerca todos los servicios que incluye. CryptPad es, en nuestra opinión, una excelente manera de colaborar con otras personas, ya sea en un documento de texto, una hoja de cálculo, un tablero de proyectos o una presentación. Adicionalmente, es un software cifrado de extremo-a-extremo que mantiene privado el contenido de todos sus archivos y ni siquiera nosotros, los admins, podemos leerlos.


# Dominios personalizados y XMPP

Estamos muy sorprendidos y agradecidos por las respuestas de todxs ustedes. Las solicitudes de enlaces de dominio personalizados para el correo electrónico siguen llegando, y estamos felices de que ustedes lo sigan pidiendo. Después de todo, pensamos que es la mejor opción para mantener su dirección de correo electrónico/presencia en internet sin necesidad de preocuparse por el proveedor que utilizan, ya que pueden llevarla a donde quieran sin molestarse en informar a todos sus contactos sobre el cambio.

Aunque también proporcionamos la vinculación de dominios de chat XMPP, aún no hemos procesado ninguna (prioridades), pero pronto nos estaremos contactando con todxs lxs que lo hayan solicitado, ¡así que sigan atentxs!


# Destellos del futuro

## Lacre.io

Aunque no hemos podido trabajar en el proyecto de cifrado del correo tanto como hubiéramos querido y solo empezamos recientemente, queremos terminarlo el año que viene. @pfm ha estado haciendo magia y trabajando para que el software esté actualizado y sea utilizable así que estamos casi listos para hacer unas pruebas cerradas de fase alfa muy, muy pronto. Estamos verdaderamente ansiosos de eso.


## Fe.disroot.org - Una nueva semilla en el Fediverso.

Para quienes aún no lo saben, el Fediverso es una red de servicios federados, que van desde redes sociales, intercambio de archivos, tableros de imágenes, etc. Es una gran red en la que no hay una única entidad que sea dueña de todos los recursos y datos de las personas usuarias. Por su naturaleza descentralizada promueve la libertad y la cooperación entre diferentes entidades para crear una red verdaderamente independiente y autogestionada de proveedores de servicios éticos.

Uno de los principales objetivos de Disroot es proveer servicios federados, así que meternos en el protocolo ActivityPub (la principal fuerza motora del Fediverso) es algo que queríamos hacer desde hace rato. Hemos estado probando muchas soluciones de software posibles desde hace ya tiempo. Aunque pensamos que Hubzilla es la más avanzada y centrada en la privacidad que existe, es también muy demandante para nosotros mantenerla y hacerla amigable. Hemos decidido adoptar un enfoque diferente. En lugar de utilizar un solo software para hacer todo, decidimos buscar diferentes servicios federados y utilizar varios de ellos que hagan una sola cosa. Así que comenzamos con una plataforma de microblogging (como Mastodon o Twitter) y optamos por Pleroma. Todavía está muy en fase de pruebas, pero es lo suficientemente utilizable como para lanzarse a probarlo. Si ya están familiarizadxs con el Fediverso, nos vendrían muy bien sus comentarios para detectar todos los problemas y errores pequeños antes de que la promocionemos para su uso regular. Mientras tanto, estamos preparando todos los tutoriales e información del sitio necesarios para que la incorporación de lxs Disrooters para quienes el concepto del Fediverso es nuevo todavía sea lo más fácil posible. Si están interesadxs en probar las cosas, pueden iniciar sesión en nuestra instancia de Pleroma con sus credenciales de Disroot en: https://fe.disroot.org. Si quieren participar en las discusiones sobre las futuras aplicaciones del Fediverso que queremos proveer pueden unirse a nuestro chat federado en xmpp:fedisroot@chat.disroot.org.

**Una vez más: ¡que tengan una gran celebración de año nuevo y les deseamos a todxs un muy buen 2022!**

¡Gracias especiales a todxs lxs Disrooters que han estado ayudándonos reportando problemas, donando, pasando tiempo por nuestro chat (disroot@chat.disroot.org), a aquellas personas que estuvieron ayudando también a otras con problemas relacionados con Disroot, a todxs lxs que han enviado parches, traducciones y tutoriales! ¡Ustedes son geniales!

Disroot tampoco existiría sin el incansable trabajo de todxs lxs desarrolladorxs de software Libre y de Código Abierto. ¡Nos gustaría agraderles a todxs por enorme trabajo! A todas las personas involucradas en la creación del kernel Linux, las utilidades GNU, a Debian y todas las otras distribuciones (también a Arch :P), a todxs aquellxs detrás de los proyectos que utilizamos a diario en Disroot: Nginx, Debian, CryptPad, Framasoft, Mumble, Pleroma, Soapbox, Hubzilla, Etherpad, Ethercalc, Taiga, Nextcloud, Lufi, ConverseJS, Privatebin, Searx, Jitsi, Gitea, Prosody, Mattermost, Postfix, Dovecot, Amavis, Rainloop y muchos, muchos más... ¡Ustedes son los héroes y heroínas! ¡Feliz año nuevo!
