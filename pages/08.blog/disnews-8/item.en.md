---
title: 'DisNews #8 - First new server and Happy new year!'
date: '25-12-2022'
media_order: roos.jpg
taxonomy:
  category: news
  tag: [disroot, news, server, lacre, encryption]
body_classes: 'single single-post'

---
Hi there,
Almost the end of yet another year. We would like to wish you all Happy Solstice, awesome new year celebrations and great, amazing and incredible 2023!
Here are the highlights of the past month and a half on Disroot.

## New server Roos

Previously we have announced the purchase of new servers for Disroot. These new hardware will be replacing our current servers which have already reached the end of their life span long time ago so it's about time for upgrade. Since such operation takes quite some work, especially to minimize downtime, we have decided to replace one server at the time. On 13th November we have replaced the first of them. Roos has replaced the server (Nelly) responsible for handling all databases. It's a real boost in hardware performance and although we still have some optimization work to do, you can already feel the difference. The new server will allow us to further improve performance as well as add more services.


## Lacre

We are very pleased to announce that our work on Lacre (our end-to-end mailbox encryption project) has reached version 0.1 and is almost ready for proper battle test on Disroot. As you know we have announced open beta tests few weeks earlier. However, due to a bug we have noticed during closed alpha tests on Disroot server (yes we did already implemented it on Disroot but it had been disabled since), we decided to postpone it until issue is solved. @pfm as well as some of the community contributors are working hard to reproduce the issue and find a solution (special thanks to *@darhma, @Onnayaku,* and *@l3o*). The bug is a very nasty one to solve because it's hard to reproduce, and @pfm has been loosing his mind over it. He's been working tirelessly and he's closer to finding the solution. This means soon we will attempt to test end-to-end mailbox encryption on Disroot, and perhaps set it permanently.  If you are on Fediverse, give [him](https://edolas.world/users/pfm) some love!

## Akkoma/Pleroma, are we on Mastodon? Who is John Mastodon and what's this fediverse

One of Disroot's pillars is to support federated services. Federation creates decentralized network of service providers allowing users to interact with one another even if hosted on different servers. One of the prime examples of federated service is email. Users on Disroot can send emails to any other user hosted elsewhere (eg. tutanota, posteo, riseup or even corporate ones like gmail or yahoo). There is no central owner of email service, and thus all providers have to cooperate with each other and allow intercommunication. No one can imagine having to create separate account on Disroot, gmail, and hundreds of thousands of other services to communicate.

Since the dawn of Disroot, we were providing a federated social network. Since 2020, we have searched for new social network platform based on new interoperable protocol called ActivityPub. We have tested number of services like Hubzilla, Mastodon, Pleroma and at the end settled for Akkoma. Akkoma is a fork of Pleroma software but what's most important it is compatible with the entire ActivityPub protocol family. ActivityPub is a network protocol allowing multitude of services such as microblogging, video hosting, file sharing, etc., to communicate with each other. This means that not only users can communicate with other Akkoma users, but can communicate with the entire Fediverse. Fediverse is a network utilizing ActivityPub protocol and allows intercommunication between different platforms like for example Mastodon/Pleroma/Akkoma (microblogging like twitter), PeerTube (video publishing like youtube), PixelFed (photo publishing like instagram), and many others. Imagine having instagram account, interacting with facebook post or commenting on reddit without the need to leave instagram nor having account on reddit. ActivityPub is very rapidly growing protocol with all sort of new services emerging.


We would like to invite you all to our microblogging fediverse instance called [FEDIsroot](https://fe.disroot.org) This is our first embark onto the network and we will be providing more fediverse services in the future. Our Akkoma instance is working fine, though still has few issues here and there we need to solve in the coming weeks (for example usernames with dots are not accepted due to an issue we are working on solving). If you experience any issues with fe.disroot.org, have feedback to provide, etc., please use our communication channels (listed below) to let us know. Your feedback, especially for young service such as this one is very important.

**We would like to wish you all amazing 2023 \o/**
