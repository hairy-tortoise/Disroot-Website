---
title: '[Sprint 5-6] – New website, Disroot for groups'
date: 01/27/2016
taxonomy:
  category: news
  tag: [disroot, news]
body_classes: 'single single-post'

---


**Dear Disrooters,**

First, we would still like to wish you all a happy new year. The last 6 months have been a blast for us. A lot of hard work went into the Disroot project but it sure was seasoned with many fun challenges too. We would like to thank you all for using Disroot, testing it and sending us feedback. We really appreciate that. We are looking forward to upcoming months and hope you do to. We wish you all the best, success and lots a fun with your projects, actions and personal life.

Hokay,
**So what have we been busy with the last weeks?**

## New website

We got a bit bored and felt constrained with the old CMS we’ve been using, so we decided to move our website to good old WordPress. Antilopa did some amazing work tweaking the theme and making everything look pretty once again. We re-wrote some of the texts on the website and are now focusing on more useful functionalities on the website as well as adding more howtos and tutorials (contribution is more then welcome).


## Disroot for groups

This new feature has been brewing in our heads from the moment we started working on Disroot. We wanted it to be possible for groups to use all the features we offer as easy and independently as possible. We did a lot of work under the hood preparing for this service. A lot of configuration has been re-done to facilitate groups. The feature is still rough on the edges and needs some work but it’s usable enough to give it a go and let groups make use of it. It’s also the best way to test and fix things up.

**So what would this new feature offer?**

    - **Autonomous user management;** admins of groups can add new users, modify existing ones and request deletion or removal from the group.
    - **Disroot services can be linked to your domain** (cloud.yourgroup.org, forum.yourgroup.org)
    - **Disroot can handle your E-mail using your domain** (member@yourgoup.org) that includes your administrative E-mail accounts (info@yourgroup.org)
    - **You can request email aliases** to be assigned to any of your E-mail accounts (still in planning: providing interface for group admin to manage email aliases)
    - **Every new user that is created by group admin is added to the group** (meaning it has access to shared folders of the group, gets added to contact lists, gets added to chat roster, gets added to the forum group)
    - **Every group gets its own forum space which can be public or private**


## What’s coming up?

First thing – Holidays \o/ – Upcoming three weeks we aren’t planning a sprint. We want to use this time to focus on other projects we have been neglecting. We are of course going to monitor/use and fix any Disroot related problem.

Secondly we are planning to transform Disroot to an official organization. This is necessary to be able to ask / receive donations, and to share better the responsibility towards the platform (currently there is one person responsible for all the infrastructure / finances). Getting financial stability is very important for the service to grow and sustain it’s user-base.

And of course, more integration and polishing of user experience. We are planning to introduce SSO (Single Sign On) service. That means you will only need to login once to get access to all the Disroot goodies. We also want to have all Disroot services accessible via TOR. And of course add more interesting services, write more tutorials etc..

As always you can follow what’s up and what are we up to [here](https://board.disroot.org/project/disroot-disroot/backlog)



cheers!

– Disroot team
