---
title: 'Competencia de Fin de Año de diseño de stickers'
media_order: stickers.jpg
published: true
date: '07-12-2020 21:00'
taxonomy:
    category:
        - news
    tag:
        - novedades
        - concurso
body_classes: 'single single-post'
---

El año pasado, nuestra recaudación de fondos de Fin de Año fue un éxito, y algo realmente divertido de hacer. Pensamos que deberíamos terminar también este loco año con algo igual de divertido y creativo. Desde hace rato estamos queriendo ofrecer más regalos a todxs lxs Disrooters que donan dinero para mantener la plataforma a flote. Mientras que hacer stickers tradicionales con el logo está bueno, pensamos que sería grandioso si lxs Disrooters mismxs diseñaran algunos más geniales. Así que pensamos en organizar un pequeño concurso.

El objetivo es simple. **Crea un diseño para stickers que te guste y súbelo a [esta dirección](https://cloud.disroot.org/s/FP9HPEXbfKb6ZQQ) antes del 25 de Diciembre**. Los stickers ganadores serán impresos y enviados en un paquete a los donantes de **Disroot** durante 2021.

Organizaremos una encuesta para que cada unx pueda votar por los stickers que más les guste. Los ganadores recibirán un paquete de ellos primeros como regalo de Año Nuevo ;)

Aquí van los requisitos:

- Se creativo
- El diseño de tu sticker no tiene que ser necesariamente de Disroot, puedes hacer sobre Software Libre y de Código Abierto (FLOSS), Federación, Fediverso, hardware abierto (somos todxs una gran familia).
- Enloquece
- Envía archivos svg (preferentemente, aunque puedes hacerlo en formato png)
- Fuck 2020 (nos encantaría ver ese sticker también 😂)

Tu diseño debe tener una licencia Atribución-CompartirIgual 4.0 Internacional (CC BY-SA 4.0)

¡Así que toma tu tablet gráfica, mouse, un lápiz o lo que necesites y dibuja el sticker de Disroot más lindo! No podemos esperar por sus diseños y también participar nosotros :).

¡No dudes en compartirlo con el hashtag **#Distickeroot** en tus redes sociales favoritas!

**El Equipo de Disroot**
