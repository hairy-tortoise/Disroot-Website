---
title: 'DisNews #9 - Spring cleaning; TOR; Forgejo'
date: '20-03-2023'
media_order: Protests_against_war_in_Ukraine_in_Moscow.jpg
taxonomy:
  category: news
  tag: [disroot, news, tor, forgejo, discourse, snappymail, taiga]
body_classes: 'single single-post'

---

Hi there,

This paragraph was originally about the fact we wanted to stay more consistent this year and send out DisNews on regular monthly basis. There would be nothing wrong except for the fact that, for some unexplained reason, this blog post draft was waiting to be published for over two months. So much for improvement...

On positive note, we still have couple of months left in 2023 to improve that. Here's what we've been up to.

## Ending services (early spring cleaning)

New year brings new energy and new ideas. We decided to start the year by doing some early spring cleaning. One of the things we would like to focus on this year is making Disroot a more unified platform. Very often, services double-up some features, leading to either under-developed products or splitting community into smaller niches. This in turn requires extra maintenance labour and often results in unpolished and abandon-like state.

Taiga is one of such services. For years, Taiga served as a good alternative to project management board. It is a great tool for software related groups offering all the best SCRUM and Kanban can offer. However since few years there are good alternatives that better suit our userbase (and ourselves). Taiga is great but it's meant for professional development teams. For simple Kanban boards or todo lists, Nextcloud's Deck and Nextcloud's Tasks are better suited and for all dev related boards, Forgejo's issue boards are sufficient enough. On top of that, considering that our Taiga instance has been used by just a few people in the last years and recently that number has further declined, we see not much point in keeping it alive. The resources can be better spent elsewhere. And so, we have decided to kill Taiga on the 1st of July 2023. 

Forum has always been a miss project. The main idea behind it was to provide users with a platform to create their own small public and private discussion groups. However due to software limitations this appeared to be very hard to implement and required hacky feature that never properly took off. Apart from a few private groups, the forum was mainly used as a support place for Disroot. This was not the intended purpose of the software. However, even for this use case, it has not worked good enough. It did not get much community involvement to help others with Disroot related issues (most of the active disrooters hangout on chatroom and Fediverse), it created yet another place for us, the admins, to keep an eye on and reply. To end this, we have decided to provide at this moment two contact points for support/feedback:

- By email - more private way to reach us with your issues

- Git Issue Board - where you can follow up on current developments, leave feedback, share issues, suggestions, etc. This board is public for anyone to see and follow.

So as of now, you can no longer view subjects on Discourse nor create new accounts. We're thinking about a replacement service to suit the idea of self ruled community discussion board service, but haven't decided yet which way to go. You'll most likely hear from us about this soon.

Last service we are planning to shutdown is Snappymail. For several months now, we've been offering two web based email clients. One is Snappymail, which some of you are more familiar with if you are an old school disrooter (reachable on https://mail.disroot.org/ ) and one called Roundcube, available on https://webmail.disroot.org being our default webmail for newcomers for some time now. Providing two web mail solutions allow all users to easily and smoothly switch from one to the other, and also helped us polish the replacement. So far, we're very happy with Roundcube: it integrates fine with Disroot's Nextcloud instance, so all your contacts set in Nextcloud are available in your webmail; has a good filters feature (which is important for many users) and for those that want to encrypt their emails, it also works fine with the Mailvelope webbrowser app. You can check the howto [here](https://howto.disroot.org/en/tutorials/email/webmail/roundcube/encryption). So the time has come to settle on one webmail solution. On the 1st of June Snappymail will go offline for good and Roundcube will be the only solution for everyone. For those on Snappymail that haven't made the transition yet, please have a look at tutorials on [migration](https://howto.disroot.org/en/tutorials/email/webmail/migration).


## TOR

If we say we will provide onion services you don't have to remind us every five years :). Yes. Finally here. It's one of the first requests that for some reason was pushed down the todo list over and over. Time has come to get to it and we have landed first portion of onion addresses. Currently most of web services have their onion address (autodetected on your Tor browser also). Next up are things like XMPP and email. Let's hope it won't take another few years.

## Switching Gitea to Forgejo

Time and again profit gain greed destroys everything. Gitea, the version control software we provided at https://git.disroot.org some months ago became a commercial for profit entity. The idea behind this decision was to provide proper stable financial base for the core development team. And nothing would be bad about it and we would support this move 100% as we think people putting their time, effort and love to open source projects should be financially compensated for their work for the benefit of us all (this is why we try to share the surplus of our donation to those projects). However, very often the decision to pivot in direction of monetizing the project are just done bad, and story of Gitea is unfortunatelly one of the examples. In short, Gitea core team announced the creation of new for profit company and seized all assets (including the name, logo, etc.) without prior community consultation. Since Gitea as a project was not only developed by the core team but larger community of contributors, people felt betrayed. This was the reason some of the involved members decided to fork the project and create truly community driven derivative of Gitea called Forgejo. And, so we have decided to follow that notion and support them by switching our instance to Forgejo. You can read more about the whole situation [here](https://forgejo.org/2022-12-15-hello-forgejo/). Power to the People!


## Friendly reminder about password care

We wanted to also do a quick reminder about Disroot's account password. Lately, we've been receiving a lot of questions and requests from users who lost their password. It is necessary to remember that **passwords and accounts are users' responsibility**. Since we don't ask for more information than is strictly necessary for the operation of the service, we have no way to determine whether you are the legit owner of the account and therefore **we never reset password for users**. This means account can be lost. So here are some suggestions.

First, the best way to keep your password safely is to use tools like password managers (KeePassX, KeeWeb, QTPass, Pass or Bitwarden). They are life savers.

Second, if you still lose it for some reason, you could reset it by going to https://user.disroot.org. BUT you can do this only if you have already set:
* either setup security questions
* or notification email to where password reset email will be sent

So, if you want to be safe, don't hesitate to go to https://user.disroot.org to set those. Here you can learn how to do it (https://howto.disroot.org/en/tutorials/user/account/administration/ussc)
