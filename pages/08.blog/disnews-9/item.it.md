---
title: 'DisNews #9 - Pulizie di primavera; TOR; Forgejo'
date: '20-03-2023'
media_order: Protests_against_war_in_Ukraine_in_Moscow.jpg
taxonomy:
  category: news
  tag: [disroot, news, tor, forgejo, discourse, snappymail, taiga]
body_classes: 'single single-post'

---

Ciao a tutti,

avevamo detto che quest'anno volevamo essere più coerenti e inviare DisNews con regolarità mensile. Gli intenti c'erano ma per qualche inspiegabile motivo, la bozza di questo post aspettava di essere pubblicata da oltre due mesi. Alla faccia dei miglioramenti...

## Pulizie di primavera anticipate

L'anno nuovo porta con sé nuove energie e nuove idee. Abbiamo deciso di iniziare l'anno facendo le prime pulizie di primavera. Una delle cose su cui vorremmo concentrarci quest'anno è rendere Disroot una piattaforma più unitaria. Molto spesso i servizi raddoppiano alcune funzionalità, il che porta a prodotti poco sviluppati o a dividere la comunità in nicchie più piccole. Questo a sua volta richiede un lavoro di manutenzione aggiuntivo e spesso si traduce in uno stato di abbandono e di scarsa qualità.

Taiga è uno di questi servizi. Per anni, Taiga ha rappresentato un valido strumento per la gestione di progetti. È un ottimo strumento per i gruppi che si occupano di software e offre tutto il meglio che SCRUM e Kanban possono offrire. Tuttavia, da qualche anno esistono valide alternative che si adattano meglio alla nostra base di utenti (e a noi stessi). Taiga è ottimo, ma è pensato per team di sviluppo professionali. Per semplici schede Kanban o elenchi di cose da fare, Deck e Tasks di Nextcloud sono più adatti e per tutte le schede relative allo sviluppo, le schede di issue di Forgejo sono più che sufficienti. Inoltre, considerando che la nostra istanza di Taiga è stata utilizzata da poche persone negli ultimi anni e che di recente il numero è ulteriormente diminuito, non vediamo il motivo di tenerla in vita. Le risorse possono essere spese meglio altrove. Abbiamo quindi deciso di chiudere Taiga il 1° luglio 2023. 

Anche il Forum è sempre stato un progetto mancato. L'idea principale era quella di fornire agli utenti una piattaforma per creare i propri piccoli gruppi di discussione pubblici e privati. Tuttavia, a causa delle limitazioni del software, questa funzione è risultata molto difficile da implementare e ha richiesto un'operazione macchinosa che non è mai decollata. A parte alcuni gruppi privati, il forum è stato utilizzato principalmente come luogo di supporto per Disroot. Non era questo lo scopo previsto del software. Tuttavia, anche per questo caso d'uso, non ha funzionato abbastanza bene. Non ha coinvolto molto la comunità per aiutare gli altri a risolvere i problemi relativi a Disroot (la maggior parte dei disrooters attivi frequenta la chatroom e il Fediverso), ma ha creato un altro luogo per noi amministratori da tenere d'occhio e a cui rispondere. Per porre fine a questa situazione, abbiamo deciso di fornire in questo momento due punti di contatto per il supporto/feedback:

- Per e-mail: un modo più riservato per comunicare gli eventuali problemi.

- Git Issue Board - dove è possibile seguire gli sviluppi attuali, lasciare feedback, condividere problemi, suggerimenti, ecc. Questa bacheca è pubblica e chiunque può vederla e seguirla.

Stiamo quindi pensando a un servizio sostitutivo che si adatti meglio all'idea di un forum di discussione comunitario autonomo, ma non abbiamo ancora deciso quale strada intraprendere. Molto probabilmente avrete presto nostre notizie in merito.

L'ultimo servizio che intendiamo chiudere è Snappymail. Da diversi mesi offriamo due client di posta elettronica basati sul Web. Uno è Snappymail, che alcuni di voi conoscono meglio se siete disrooter di vecchia data (raggiungibile all'indirizzo https://mail.disroot.org/ ) e uno chiamato Roundcube, disponibile all'indirizzo https://webmail.disroot.org, che da qualche tempo è la nostra webmail predefinita per i nuovi arrivati. La presenza di due soluzioni di web mail consente a tutti gli utenti di passare facilmente e senza problemi da una all'altra, e ci ha anche aiutato a perfezionare la sostituzione. Finora siamo molto soddisfatti di Roundcube: si integra bene con l'istanza Nextcloud di Disroot, in modo che tutti i contatti impostati in Nextcloud siano disponibili nella webmail; ha una buona funzione di filtri (che è importante per molti utenti) e per coloro che vogliono criptare le proprie e-mail, funziona bene anche con l'applicazione webbrowser Mailvelope. Potete consultare la guida [qui](https://howto.disroot.org/en/tutorials/email/webmail/roundcube/encryption). È quindi arrivato il momento di scegliere una soluzione di webmail unica. Il 1° giugno Snappymail andrà definitivamente offline e Roundcube sarà l'unica soluzione per tutti. Per coloro che non hanno ancora effettuato il passaggio a Snappymail, si consiglia di dare un'occhiata ai tutorial su [migrazione](https://howto.disroot.org/en/tutorials/email/webmail/migration).

## TOR

Sì, finalmente ci siamo! È una delle richieste che, per qualche motivo, è stata spinta in fondo alla lista delle cose da fare più e più volte. È arrivato il momento di farlo e abbiamo ottenuto la prima porzione di indirizzi onion. Attualmente la maggior parte dei servizi web ha il proprio indirizzo onion (rilevato automaticamente anche dal browser Tor). I prossimi servizi saranno XMPP e la posta elettronica. Speriamo di darvi buone notizie a breve.

## Passaggio da Gitea a Forgejo

Spesso l'avidità del profitto distrugge tutto. Gitea, il software di gestione delle versioni che abbiamo fornito su https://git.disroot.org, qualche mese fa è diventato un'entità commerciale a scopo di lucro. L'idea alla base di questa decisione era quella di fornire una base finanziaria stabile al team di sviluppo principale. Non c'è nulla di male in tutto ciò e noi appoggiamo questa mossa al 100%, poiché pensiamo che le persone che dedicano il loro tempo, il loro impegno e il loro amore ai progetti open source debbano essere ricompensate finanziariamente per il loro lavoro a beneficio di tutti noi (ecco perché cerchiamo di condividere il surplus delle nostre donazioni a questi progetti). Tuttavia, molto spesso la decisione di orientarsi verso la monetizzazione del progetto viene presa male, e la storia di Gitea è purtroppo uno di questi esempi. In breve, il team di Gitea ha annunciato la creazione di una nuova società a scopo di lucro senza consultare preventivamente la comunità. Poiché Gitea come progetto non è stato sviluppato solo dal core team, ma anche da una più ampia comunità di collaboratori, le persone si sono sentite tradite. Questo è il motivo per cui alcuni dei membri coinvolti hanno deciso di fare un fork del progetto e di creare un vero e proprio derivato di Gitea, chiamato Forgejo. Abbiamo quindi deciso di seguire questa idea e di sostenerli migrando la nostra istanza a Forgejo. Potete leggere di più su tutta questa situazione [qui](https://forgejo.org/2022-12-15-hello-forgejo/). Potere al popolo!


## Promemoria sulla cura delle password

Vogliamo fare un rapido promemoria sulla password dell'account di Disroot. Ultimamente abbiamo ricevuto molte domande e richieste da parte di utenti che hanno perso la loro password. È necessario ricordare che **le password e gli account sono responsabilità degli utenti**. Poiché non chiediamo più informazioni di quelle strettamente necessarie per il funzionamento del servizio, non abbiamo modo di determinare se l'utente è il legittimo proprietario dell'account e quindi **non resettiamo mai la password degli utenti**. Questo significa che l'account può essere perso. Ecco quindi alcuni suggerimenti.

Innanzitutto, il modo migliore per conservare le password in modo sicuro è utilizzare strumenti come i gestori di password (KeePassX, KeeWeb, QTPass, Pass o Bitwarden). Sono dei veri e propri salvavita.

In secondo luogo, se per qualche motivo doveste comunque aver perso le vostre credenziali, potete sempre reimpostare la password andando su https://user.disroot.org. Attenzione! È possibile farlo solo se si è già impostato:
* l'impostazione delle domande di sicurezza
* o l'e-mail di notifica a cui verrà inviata l'e-mail di reimpostazione della password

Quindi, se volete essere sicuri di non perdere per sempre i vostri accessi, non esitate ad andare su https://user.disroot.org e impostate quanto suggerito. Qui potete imparare come farlo (https://howto.disroot.org/en/tutorials/user/account/administration/ussc)
