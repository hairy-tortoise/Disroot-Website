---
title: 'Nuevos Servidores - Almacenamiento Extra - Nextcloud 13'
media_order: twoServers.jpg
published: true
date: '14-03-2018 22:38'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
body_classes: 'single single-post'
---

### ¡Importante!
**Cambios en las Condiciones de Uso:**

Debido al creciente abuso de nuestros servicios (del correo principalmente) por empresas, que encuentran conveniente utilizar un proveedor voluntario de servicio gratuito con la intención de generar beneficios económicos, hemos agregado la siguiente sección a nuestras [Condiciones de Uso](https://disroot.org/es/tos):


> **Uso de los servicios de Disroot para actividades comerciales**<br>
Disroot es una organización sin fines de lucro que provee servicios para individuos con el criterio "paga lo que desees". En virtud de esta estructura, entendemos la utilización de los servicios de Disroot con propósitos comerciales como un abuso del servicio y será tratado como tal.
- Comerciar cualquier servicio de Disroot con terceras partes está prohibido.
- Utilizar el correo con fines tales como cuentas del tipo "no responder" para negocios implicará el bloqueo son aviso si dicha actividad es detectada.
- Enviar correos a granel, incluyendo mas no limitado a, marketing y publicidad, con fines comerciales, será tratado como spam e implicará el bloqueo sin aviso previo en caso de ser descubierto.
- Utilizar los servicios de Disroot con afán de lucro, incluyendo, mas no limitado a, comerciar o conseguir ventas, no es tolerado. Las cuentas creadas con el propósito de generar beneficios económicos estarán sujetas a rescisión en respuesta.
- Utilizar los servicios de Disroot para cualquier otra actividad comercial será revisada en cada caso y la decisión de rescindir tales cuentas se basará en la comunicación con el titular de la cuenta y el tipo de actividad en cuestión.


La nueva sección tendrá efecto a partir del 1ro de Abril. Si tienes preguntas, o no estás seguro si caes dentro de esta nueva definición, por favor, contáctanos.

<Anuncio\> Cualquier negocio legítimo que quisiera seguir utilizando el estupendo servicio y soporte, por favor, contáctenos para escuchar nuestras ofertas comerciales. </Anuncio\>

### Nuevos servidores
Como algunos de ustedes ya saben, y han experimentado, en los últimos meses estuvimos ocupados implementando nuevos servidores. Esto mejora enormemente el rendimiento general de la plataforma Disroot a la vez que genera un recurso [búfer](https://es.wikipedia.org/wiki/Buffer_de_datos) muy necesario para los pŕoximos meses. Así que, sin más preámbulos, los presentamos:

Knopi - nuestro servidor principal - Dell R410:
  - 32GB RAM
  - 2x L5630
  - 4x 8TB en HWRAID10 (16TB en total)

Nelly - nuestro servidor de base de datos - Dell R610:
  - 88GB RAM
  - 2xE5520
  - 6x512GB SSD en HWRAID10 (1.5TB Total)


La operación llevó más tiempo del esperado y con algunos errores, provocando caídas del servicio que se podrían haber evitado. Las noches sin dormir que pasamos en el [data center](https://es.wikipedia.org/wiki/Centro_de_procesamiento_de_datos) nos aportó un montón de experiencia que seguramente nos vendrá bien la próxima vez que necesitemos mudar o actualizar la infraestructura :). Queremos decir que lamentamos mucho cualquier inconveniente ocasionado, y agradecer a la comunidad de Disroot por apoyarnos en esos momentos difíciles.

### Nextcloud 13
La largamente esperada actualización a Nextcloud 13 ha llegado a Disroot. ¿Qué hay de nuevo? Además del habitual incremento en el rendimiento, hay dos grandes funciones que muchos de ustedes esperaban:
  - Nextcloud Talk - Actualización de la aplicación antes conocida como Llamadas Spreed (conferencia en audio/video). No sólo tiene ahora un chat incorporado, sino también una aplicación para móviles para gestionar tus llamadas. Esto es todavía muy reciente y esperamos la futura integración con otras aplicaciones de Nextcloud. Los rumores dicen que Nextcloud Talk se integrará con Matrix y XMPP. Mantenemos los dedos cruzados.

  - Cifrado de extremo a extremo - Está funcionando ahora con los archivos de Nextcloud. Aunque todavía está en fase alfa y sólo disponible para el cliente de Android, es posible cifrar de extremo a extremo tus archivos (mediante carpeta). Es prudente estar consciente de su principal inconveniente: no es posible utilizar la interfaz web para acceder a la carpeta cifrada (está bien cifrada). **Por favor, lee la documentación y recuerda que esta es una función en etapa temprana de desarrollo, así que conserva un buen respaldo de los archivos que pienses cifrar.**

  https://nextcloud.com/endtoend/

### Espacio Extra de Almacenamiento
¿Te estás quedando sin espacio en tu nube o correo? Finalmente te tenemos cubierto. Ya que ahora puedes solicitar espacio adicional para la nube o la casilla de correo. Como ya comunicamos en nuestro anuncio previo, el precio por GB es 0.15euro mensual y en principio estaremos ofreciendo ampliaciones a 14GB, 29GB y 54GB (de espacio total). Puedes enviar tu solicitud a través de [este](https://disroot.org/es/forms/extra-storage-space) formulario.

### Finanzas
Hemos actualizado nuestra gráfica de finanzas. ¡Se ve muy bien! ¡Muchas gracias a todos los que han contribuido hasta ahora! Estamos muy emocionados de ver que, por primera vez, podemos pagar las cuentas mensuales de Disroot íntegramente con las donaciones recibidas. Esperamos seguir siendo sorprendidos también en los meses que vienen. Así que, no dejen de ["comprarnos un café"](https://disroot.org/es/donate) todavía. Además, si queremos ampliar, actualizar el hardware, conseguir más servidores (para poder hospedar más usuarios y servicios) necesitaremos más fondos. Recuerda que no hay tal cosa como servicio 'gratuito' y que "nube" es sólo un eufemismo de "la computadora de otro".

### ¿Nuevos servicios?
Estamos recibiendo muchos pedidos de nuevos servicios. Nosotros mismos estamos pensando en hacer algunos agregados. Naturalmente, no podemos facilitarlos todos inmediatamente debido a los recursos de hardware que se necesitan y las horas de trabajo que requiere mantenerlos. Dado los limitados recursos, necesitamos decidir cuál es el servicio más necesario/esperado que te gustaría ver en Disroot. Hemos hecho una corta lista de servicios que estamos considerando, puedes mirar [esta](https://poll.disroot.org/ff3Qbrs1Kh74pHtN) encuesta y votar por tu favorita (aunque sin garantías). La misma se cerrará el 15 de Abril.

<br><br>

**En los próximos meses nos vamos a tomar las cosas con un poco más de calma y enfocarnos en el mantenimiento y la limpieza general de primavera.**

**Queremos desearles a todos grandes celebraciones de primavera, y a aquellos en el hemisferio sur, acogedoras noches de otoño.**
