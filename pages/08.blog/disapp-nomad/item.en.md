---
title: 'DisrootApp & Nomad on Fdroid'
date: 09/29/2018
media_order: nomad_disapp.png
taxonomy:
  category: news
  tag: [disroot, news, adndroid, hubzilla, apps]
body_classes: 'single single-post'

---

Disroot.org is a platform providing multiple services for many different use cases. Most of the services offered on the platform already provide their own mobile app but that is ungraspable to the many people who were constantly inquiring about a "Disroot app". Time and time again we've been explaining people that they can use any email app supporting IMAP/POP3 or that they can just grab nextcloud app to sync files or use any Jabber client for their chat.  But now, thanks to a very clever Disrooter, we actually have a Disroot app, something we, the admins, couldn't even imagine! the Disapp works as a swiss army knife for Disroot. It helps users install appropriate apps for each service, as well as open Disroot related URL's within the app window for those services that don't have a dedicated application. Its a very neat approach that solves a lot of problems for new comers as well as veteran users. We are very excited and very thankful for **massimiliano**  as well as all the people in the community that have helped making it happen.

**Nomad.**

As some of you know for last months we are experimenting with an amazing social network called Hubzilla. Together with other disrooters we are working on the documentation, theme, plugins and few other things that will make our instance a complete package easy to onboard and use. Yet again, with pure caffeine running through his veins, the sleepless Disrooter decided to take on the task of creating dedicated app for hubzilla called Nomad. Nomad, created by **massimiliano** , is a fork of #dandelion (mobile app for diaspora) which serves a webview version of hubzilla with extra features such as native menus, share with, and other neat things you expect from the mobile app. It's still in early stages of development and needs a lot fo work but its very much awesome already and makes hubzilla usage on mobile a breeze.

We are very excited about those two new apps and would like to thank all the people that were involved in the process of creating them, testing and translating.

you can find both apps on [fdroid]( https://f-droid.org/) if you don't use that app store yet, its about time to start:
[disrootapp](https://f-droid.org/en/packages/org.disroot.disrootapp/)
[nomad](https://f-droid.org/packages/com.dfa.hubzilla_android/)
