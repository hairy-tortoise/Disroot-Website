---
title: 'Registro de usuarios de Disroot temporalmente cerrado'
media_order: dont_like_spam.jpg
published: true
date: '07-09-2018 15:10'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - registration
body_classes: 'single single-post'
---

Cuando comenzamos Disroot, queríamos tener un montón de herramientas que respetaran la privacidad y compartirlas con otras personas, con una comunidad de aquellos que quizá compartieran nuestras elecciones éticas o técnicas. Pusimos mucho de nuestro tiempo y esfuerzo para construir esta plataforma y brindar las mejores y útiles soluciones libres y de código abierto que pudiéramos encontrar, compartiendo nuestra experiencia y conocimiento en el proceso. Esa es la esencia de Disroot.

Desde hace un año, estamos experimentando un problema cada vez más grande con nuestro servicio de correo electrónico. Ahora tenemos una enorme cantidad de cuentas abusivas robando nuestro tiempo y creando un montón de problemas cada día. Esto significa que dedicamos un pedazo significativo de nuestras vidas a resolver esta cuestión en lugar de invertirlo en mejorar nuestros servicios. Estamos seguros que actualmente alrededor del 80% de nuestra base de usuarios son estafadores, spammers y abusadores de todo tipo. La mayor parte de nuestra energía se va en combatir a esta gente y estamos continuamente manteniendo una plataforma que está siendo mal utilizada por personas con intenciones dañinas. Sabemos que como resultado de nuestra reciente popularidad debíamos esperar un cierto porcentaje de cuentas turbias, pero en la situación presente este porcentaje se ha vuelto abrumador, con un número que excede por lejos a los Disrooters fiables que queremos mantener. Esto no solo es irritante para nosotros sino también una amenaza para la credibilidad de la plataforma y por lo tanto para todos aquellos que están usándola con confianza.

Esto significa que necesitamos encontrar una solución a este problema. Por el momento, hemos decidido cerrar los registros como una medida de protección para nuestros usuarios y para Disroot mismo mientras intentamos pensar en maneras más adecuadas de resolverlo.

En los próximos días, estaremos ocupados limpiando nuestra base de usuarios de toda la "plaga" de cuentas abusivas y planearemos cómo será el registro en el futuro. Queremos mantener el modelo de subirse de la manera más sencilla que se pueda y que cualquiera pueda unirse a la comunidad de Disroot y se sienta como en casa. Al mismo tiempo queremos impedir que conductas malintencionadas lo arruinen todo para todos nosotros. Es un hueso duro de roer, pero esperamos encontrar una solución que satisfaga a la mayoría.
