---
title: 'Nextcloud - Problemas de performance'
date: '20-11-2019 15:00'
media_order: this-is-fine.0.jpg
taxonomy:
  category: news
  tag: [disroot, noticias, nextcloud]
body_classes: 'single single-post'

---

Como habrán notado nuestra nube está lenta e inestable en este momento. Estamos trabajando junto a desarrolladores de Nextcloud además de otras instancias que están afectadas por lo que parece un "bug", tratando de encontrar la causa principal.

Con suerte, pronto tendremos una solución funcional y todo volverá a la normalidad. Sin embargo, hasta ahora, las noches de insomnio de los últimos días no han producido ningún resultado.
