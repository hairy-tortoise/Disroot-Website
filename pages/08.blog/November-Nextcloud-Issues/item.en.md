---
title: 'Nextcloud - performance issues'
date: '20-11-2019 15:00'
media_order: this-is-fine.0.jpg
taxonomy:
  category: news
  tag: [disroot, news, nextcloud]
body_classes: 'single single-post'

---

As you might have noticed our cloud instance is slow and unstable at the moment.We are working together with Nextcloud developers as well as few other instances that are affected by what seems to be a bug, trying to find a root cause.

Hopefully soon we'll have a working fix and everything will go back to normal. However up until now, sleepless nights of last days did not produce any result yet.
