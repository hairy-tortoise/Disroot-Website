---
title: '[Sprint 11 – 12] – Server migration; Operation ‘Nextcloud’'
date: 11/04/2016
taxonomy:
  category: news
  tag: [disroot, news, nextcloud]
body_classes: 'single single-post'

---



**Hi there.**
It’s been a while since you heard form us about what’s new in disroot. Don’t think we were lazy doing nothing, in fact we were extremely busy the last months finishing the most important and time consuming tasks of this year.

## Disroot is 1 year old!

We would like to celebrate with you the fact that Disroot has been running for a whole full year \o/ – We are happy that we were able to develop the project and make all these wonderful tools available to anyone. We hope we can keep improving and growing next year and that you will all stick with us for many year to come. For an overview of what has been done so far go to [https://forum.disroot.org/c/disroot/disroot-changelog](https://forum.disroot.org/c/disroot/disroot-changelog).

## Server migration.

On September 19th Disroot became a truly independent project running on self owned hardware. We have successfully migrated all the services to new servers in a new datacenter, to which we have 24/7 access and full control over our machines. The most obvious reason to own our own hardware is the independence and flexibility this give us to run this project. Until the move we have been renting a server at one of the hosting providers in the Netherlands. Thanks to a server donation from a good friend, we decided it’s high time to put those old servers back in use. We scraped some money together and bought brand new harddrives, RAM and we were ready for deployment. Well, at least with hardware. Moving all the services was way more complex. We used the opportunity to introduce some automation to the way we deploy and manage all the disroot services. We decided to work with Ansible and automate most configurations. This took great amount of time, but in long term gave as flexibility and clear overview on how everything is set up. Once our deployment scripts and ansible roles mature enough we intend to release self-deploy images so that anyone could also host his/her very own disroot instance.

## Terms of Service

You might remember our callout to comment and review a draft of Disroot’s Term of Services. Despite low external input, we have decided to declare the TOS ready and to implement it as is. Soon you will be prompted to accept the new Terms of Use before proceeding to use disroot services. Obviously, we are still open for suggestions and comments and out always ready to improve the terms. You can find the agreement on [https://disroot.org/tos](https://disroot.org/tos) .
We are still working on the Privacy Policy – so keep your eyes open for that one too.

## Operation Nextcloud

As mentioned before (see [https://disroot.org/disroot-goes-nextcloud/](https://disroot.org/disroot-goes-nextcloud)), recent changes in ownCloud project and the development of the new fork Nextcloud, led us to follow the majority of the community and core developers and move to the promising project of Nextcloud. Once we were done with server migration, we immediately started with the preparations to change clouds. First of we had to get rid of all the pending bugs that were already reported. Once this was off the table, we proceeded to test all existing apps plus work on implementing new features. On 16th October we have finally migrated to Nextcloud.

**What does it mean for you?**
First of, you probably noticed refreshed interface. There are also new mobile/desktop apps available (check fdroid for free versions). Though your current owncloud mobile/desktop apps are still compatible, we recommend to switch to nextcloud apps as those will provide extended functionalities in the future.

Apart of all that, we’ve also launched new service accessible within the cloud — Spreed.me — which let’s you host audio/video conferences with your friends, co-workers etc. We would like to hear your feedback on this new feature, so test it out and tell us what you think.
For more details on current and brand new cloud features check our page here: https://disroot.org/cloud/

## Updates and Improvements

**Diaspora**
Diaspora has been updated to the long awaited version 0.6.x This release brings a lot of changes including a beautiful new interface and a better synchronization with other pods.This means that public posts are published and synced among big and small pods alike and tags are better searched now over the many pods out there.

**Searx (search.disroot.org)**
Searx got a new UI and an extra set of search engine plugins that can be switched on. One of the top new design improvements is the infinite scroll. Yep, no more clicking “Next page” button. Just toggle the option on in Preferences >> Plugins >> Infinite scroll and enjoy.

**Taiga (board.disroot.org)**
Apart of a new improved UI, public projects are now viewable and browsable for everyone – directly on the main page. Just switch your project to “public” if you want to show your stuff to the world.

**Lufi (upload.disroot.org)**
Now you can enjoy improved upload and download speeds.

## Upcoming:

Next sprint theme is “Make XMPP great again”. We want to focus on bringing as many improvements and features as possible to our chat instance and we hope to create user experience up to date and to current standards (say bye bye to skype and whatsapp).
In upcoming weeks/months we’re also planning to focus on preparing more tutorials, videos and howtos to help you get as much as possible from our services.
