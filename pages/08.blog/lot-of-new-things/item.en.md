---
title: 'A lot of new things'
date: '12-09-2017 00:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - TOS
        - PrivacyPolicy
        - Nextcloud
        - foundation
body_classes: 'single single-post'
---

It has been a very busy month. The summer here is ending and Disroot is entering a new chapter.

## New Website!
First of all we would like to point out that we have re-built and updated the website. It is now Javascript-free friendly so anyone should be able to view it without a problem. 
We decided to dump the cumbersome Wordpress CMS for a new no-database flat-file CMS called Grav. We really recommend it for anyone who is building simple websites, because it is fast, easy to deploy and to use.

## [https://howto.disroot.org](https://howto.disroot.org)
We also launched [https://howto.disroot.org](https:/howto.disroot.org) - a better organized overview of all the tutorials. Up until now all tutorials were published on the forum which led to a big mess where information was hard to find. We think the new site solves that problem and will encourage Disrooters to submit more tutorials. The new website uses Git to submit and correct content which is super convenient. If you are not yet familiar with how Git works, we will soon provide a tutorial to explain the basics and how you can use it in the most simple way. We are also planning to eventually create possibilities to submit translations, so keep your eyes open if you want to help adding tutorials in your native language.

## Annual Report
With two years behind us, we made a resolution to be more organized and send a general report every year around the end of the summer. This is only partly connected to the fact that we will soon become an official foundation because actually it is something we've been wanting to sort out for a long time - we want to share as much information as we can with the people using this platform. You can read more what we've been doing so far, what we plan to do and what we want to do in the [Yearly Rapport](https://disroot.org/annual_reports/2017.pdf). This includes a short update on where we stand in the process of becoming a foundation.

## Becoming a foundation
Yup. It's on. We finally managed to gather enough funds, and found a missing person for the board. We have submitted the initial form and we have a meeting on 9th October to (hopefully) finalize our non-profit organization. It's a huge step for us and the project and we are very excited about it.

## Aliases, extra disk space, domain linking
One of the important aspect to becoming a not-for-profit foundation, is the ability to easily provide paid features to obtain financial stability. Many people have been asking us already if they can purchase extra disc space or use multiple aliases, so we set down and made some decisions about possibilities and costs. The following will be implemented in the month of October. We will eventually re-evaluate this model to see if it is sustainable enough (but don't worry - any changes to these extra perks we might decide to make will only affect future transactions). 
   
  - Extra space - 0.15euro/GB/month - At this point only available up to 50GB per account
  - Aliases - 5 aliases for 24 euro/year
  - Linking own domain to Email - 36 euro/year - including 10 aliases with own domain

## TOS and PrivacyPolicy
We finally published our [Terms Of Service](https://disroot.org/tos) and [Privacy Policy](https://disroot.org/privacy_policy). We tried to keep it simple and easy to read through. Let us know what you think so we'll try to improve upon your remarks.


## New apps on Nextcloud:
1. Nextcloud has now the Bookmark manager available. You can save you favorite web-pages and sync them across devices. 
Available at the moment are clients for:
**Android:**
  - Nextcloud Bookmarks - Android app with full add/edit/delete and view functionality
  - NCBookmarks - App to view/edit/open bookmarks for Android
**Webbrowsers:**
  - Floccus - Bookmark sync for Firefox/Chromium-based browsers
  - A version for iOS is in the making, you can help testing it:  https://github.com/lenchan139/NCBookmark_iOS

2. Checksum app allows users to create a hash checksum of a file, which allows you to verify if the file content has changed.

## Disroot Community
Another thing we are very excited about is the Disroot community. Yes , it seems like such a thing has formed itself ( wow! ), and now it is time to get this ring of Disrooters active. Many people are asking us how they can help and contribute to the Disroot project. To make it easy we made a list of possible ways to [contribute](https://disroot.org/community#_contribute) so you can choose what is most suitable for you. For developers, we will soon release a list of wish-listed features we want to improve.



Our next sprint will start in October. Tomorrow we are leaving for a two week vacation, meaning we will be spending more time swimming and hiking then reading and typing. Obviously we will not loose connection and will check regularly for any issues that need immediate attention, but apart of that we are going to try to take it easy and enjoy ourselves.
