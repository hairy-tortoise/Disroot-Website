---
title: 'Feuer, Wasser und Eis'
date: '09-12-2019 15:00'
media_order: 49104435191_596d016b7b_k.jpg
taxonomy:
  category: news
  tag: [disroot, nextcloud, issues, roadmap, sprint, burnout]
body_classes: 'single single-post'

---
*[Sprint 49 - Changement de saison]*

Was für eine Achterbahnfahrt die letzten Wochen waren!


Wir beginnen mit einem gigantischen **Feuer**, das sich in den letzten zweieinhalb Wochen durch unseren Server gefressen hat. Das alles wurde verursacht durch das Upgrade von PHP (Das Backend, das unseren Nextcloud-Service antreibt). Wir wussten, dass dies ein problematisches Upgrade war, weil wir die Anomalie bereits in der Vergangenheit beobachtet hatten und daraufhin entschieden hatten, auf die vorhergehende Version zurückzukehren. Obwohl wir bereits Schritte unternommen haben, um unser Setup zu schützen, indem wir unsere Webserver-Konfiguration aktualisiert haben, wurden wir durch die kürzlich entdeckte Verwundbarkeit und die Gerüchte über eine im Umlauf befindliche Ransomware, die komplette Instanzen verschlüsselt, dazu getrieben, das zu Grunde liegende PHP zu aktualisieren - nur, um auf der sicheren Seite zu sein. Das hat, wie zu erwarten, dazu geführt, dass unser Server mit maximaler Leistung lief und die gesamte Cloud so bis auf Schneckentempo verlangsamt wurde. Nach vielen Tests, Optimierungen, schlaflosen Tagen und Wochen im Versuch, die grundlegende Ursache zu finden, haben wir sie identifiziert und eine Übergangslösung implementiert. Gleichzeitig haben wir dies an das Entwickler-Team von Nextcloud kommuniziert und warten nun auf eine dauerhafte Lösung. Das dringend benötigte **Wasser** löschte das Feuer und gab der Plattform ihre Stabilität zurück.


All das und die Tatsache, dass unsere Verpflichtungen außerhalb von Disroot wie z.B. Alltagsjobs, Familien und andere Projekte mehr Zeit einforderten, ließen uns erkennen, dass eine Aufrechterhaltung unserer derzeitigen Belastung mit permanenter Arbeit - die wiederholte Abfolge von zweiwöchentlichen Sprints, fortgesetzter Verbesserung der Plattform, Beantwortung aller Support-Tickets, Anfragen, das Einbringen in Unterhaltungen via Chat, Soziale Netzwerke und was nicht noch alles - definitiv nicht unter den derzeitigen Voraussetzungen fortgeführt werden kann, da dies ansonsten in naher Zukunft zu einem totalen Burnout führen würde. Solange Disroot nicht zu unserer Hauptbeschäftigung wird, was es tatsächlich eigentlich schon ist, da einige von uns mehr Zeit in die Arbeit daran investieren als in einen Alltagsjob, brauchen wir einen Eimer voller **Eis** und eine kleine Entschleunigung.


Also haben wir entschieden, die Sprint-Perioden von 2 auf 4 Wochen zu setzen. Selbstverständlich heißt das, dass wir weiterhin konstant an Disroot arbeiten werden, aber mit den zusätzlichen zwei Wochen fühlt es sich, so glauben wir, weniger der Murmeltier-Tag an - mit wöchentlichen Meetings, Sprint-Starts/-Stopps, Verbesserungen werden zu einem langen, permanenten Kreislauf, der Dich wöchentlich in den Hintern tritt (jetzt nur noch zweiwöchentlich :P). Wir wissen, dass wir uns in naher Zukunft mehr auf finanzielle Nachhaltigkeit über das bloße Bezahlen von Rechnungen und das Sammeln von Geld für Hardware-Upgrades hinaus konzentrieren müssen. Wir müssen ein Ökosystem erschaffen, in dem wir uns Disroot, FLOSS und dem dezentralisierten, föderierten Internet als unsere Hauptbeschäftigung widmen können, wenn wir das Ganze fortführen wollen. Bei uns brodeln schon einige Ideen und wir werden im kommenden Jahr häufiger darüber berichten.

## Was kommt...

Der derzeitige, erste 4-Wochen-Sprint heißt *"Den Roadmap-Zug erwischen"* und wir wollen uns dabei auf den Abschluss der wichtigsten Punkte der aktuellen Roadmap (Q4 2019) konzentrieren, um das neue Jahr mit einem besseren Setup, einer schöneren Einstiegs- und Nutzer-Erfahrung, klaren Köpfen und mit Ideen für die nächsten Schritte zu beginnen.
