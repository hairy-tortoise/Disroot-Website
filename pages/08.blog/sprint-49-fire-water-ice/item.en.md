---
title: 'Fire, water and ice'
date: '09-12-2019 15:00'
media_order: 49104435191_596d016b7b_k.jpg
taxonomy:
  category: news
  tag: [disroot, nextcloud, issues, roadmap, sprint, burnout]
body_classes: 'single single-post'

---
*[Sprint 49 - Changement de saison]*

What a roller-coaster the last weeks were!


We start from a gigantic **fire** that has been eating through our server the past two and half weeks. It was all caused by the upgrade of PHP (the backend that powers our Nextcloud service). We knew it was a problematic one because we already have observed the anomaly in the past and decided to roll back to previous version of it. Even though we already took steps to protect our setup by updating our webserver configuration, the recently discovered vulnerability and the rumors of ransomware in the wild that encrypts entire instances, convinced us to go on an update the underlying PHP - just to be on the safe side . This, as we expected, resulted in our server working at maximum power, causing the entire cloud to slow down to a crawl. After much tests, tweaks, sleepless days and nights trying to find the root cause we have identified it and put a temporary fix in place. At the same time we communicated this to the development team at Nextcloud and now we are waiting for a permanent fix. The much needed **water** extinguished the fire and brought stability back to the platform.


All this, and the fact that our outside Disroot's responsibilities such as dayjobs, families and other projects became more demanding, made us realize that keeping our current pace of constant work - the continuous cycle of two week sprints, continuously improving the platform, responding to all the support tickets, requests, engaging in conversations via chats, social networks and what have you - definitely cannot continue in the current setting or it will lead us to total burnout in the near future. Unless Disroot becomes our main occupation, which in fact it already is as some of us spend more time working on it than on a dayjob, we need a bucket of  **ice** and slow down a little.


So we decided to extend the sprint period from 2 weeks to 4. Of course, this means we will be still constantly working on Disroot, but with the additional two week stretch we think it will feel less like groundhog day - with weekly meetings, sprint starts/stops, refinements becoming one long constant cycle kicking your butt on weekly basis (now its just bi-weekly :P). We know that in near future we need to focus more on financial sustainability beyond just paying hosting costs and accumulating money for hardware upgrades. We need to create an ecosystem where we can commit to Disroot, FLOSS and decentralized federated internet as our main occupation if we are going to continue. We have some ideas brewing and we will be more vocal about those more often in the coming year.

## Up next...

Current first 4 week sprint is called *"Catching that roadmap train"* and we will be focusing on finalizing the most important points of the current roadmap (Q4 2019) to start the new year with better setup, nicer onboarding- and user experience, clear minds and ideas on what to do next.
