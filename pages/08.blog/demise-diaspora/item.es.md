---
title: 'Cierre de Diaspora*'
media_order: tunnel.jpg
published: true
date: '19-01-2020 18:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - noticias
        - hojaderuta
        - diaspora
body_classes: 'Cierre de Diaspora*'
---

Durante el pasado año nuestro interés por **Diaspora\*** como solución de red social ha ido decayendo. Sabíamos que en algún punto llegaría la hora de tomar una decisión sobre el futuro de nuestro pod en el mapa de la federación. Ha llegado el momento de enfrentar lo inevitable y anunciar que estaremos eliminando gradualmente **Diaspora\*** a favor de algo que sentimos que tiene más potencial, se ajusta mejor a nuestra plataforma y en lo que nos hemos venido comprometiendo durante el último año.

El desempeño de nuestro pod de **Diaspora\*** en los últimos meses ha sido terrible. La gente se quejaba constantemente sobre los lentos tiempos de carga del stream y nosotros particularmente no estábamos satisfechos con la performance, aún cuando hicimos todo lo que pudimos para mejorarla. También hemos sido atacados por la creación de cuentas de SPAM, lo que nos llevó a decidir el cierre temporario de los registros mientras las combatíamos. Sin embargo, nunca los hemos vuelto a reabrir lo que refleja nuestra falta de motivación para seguir manteniendo el pod funcionando.

¿Tenemos algo contra **Diaspora\***? Para nada.

Por supuesto, el desempeño podría ser mejorado. **Diaspora\*** ha estado con nosotros desde el principio mismo. Fue en **Diaspora\*** donde publicamos nuestro primer "Hola, Mundo" como proyecto **Disroot**, y de hecho, fue el primer y único anuncio de **Disroot** hecho por nosotros. Fue una gran experiencia y una gran comunidad. Sin embargo, nada permanece en un solo lugar. Los tiempos están cambiando ("*el ecosistema está en movimiento*" :wink:  :wink: ) y sentimos que necesitamos avanzar. No queremos llegar a la situación de proveer múltiples redes sociales y por consiguiente necesitamos elegir una sola solución que podamos sostener, enfocarnos y mantener tanto como podamos. Durante los últimos cuatro años fue **Diaspora\***, pero mirando alrededor, hay proyectos asombrosos que capturan nuestro interés y con los que nos hemos involucrado más. La abrumadora colección de funcionalidades de **Hubzilla** en términos no solo de interoperabilidad con otros protocolos sino también características como grupos, perfiles, chats, foros, artículos y, claro, la identidad nómada y el protocolo Zot en general, nos hizo mirar en su dirección más y más. Por otro lado, las redes impulsadas por ActivityPub muestran el potencial de nuevas, frescas y masivamente ocupadas redes federadas. Sentimos la necesidad de cambiar y queremos poner nuestros esfuerzos en algo con lo que nos podamos identificar. Obviamente, en este momento estamos comprometidos con **Hubzilla** porque pensamos que es una red social brillante adelantada a su tiempo, que proporciona funcionalidades no vistas en ningún otro lado, y aún así muy subestimada. Dicho eso, no queremos tomar una decisión "impulsiva". Es por ello que en esta etapa no podemos decir exactamente cuál red social eventualmente elegiremos. Es muy probable que vaya a ser **Hubzilla** pero podría ser también **Friendica**, **Pleroma**, **Pixelfed** o incluso **Mastodon**. En este momento es demasiado pronto para decirlo. Sabemos que **Hubzilla** tiene sus defectos y por eso, antes de que logremos resolverlos, esto queda sin respuesta.

Lo que podemos decir es que oficialmente iremos apagando gradualmente nuestro pod de **Diaspora\***. Así que, ¿qué significa esto para los usuarixs actuales? No mucho, en realidad. **Diaspora\*** estará operativo hasta que él o la últimx usuarix apague la luz. No somos **Google** para echar a patadas a la gente como hizo con **G+**. Sin embargo, vamos a:

-   Quitar la página de **Diaspora\*** de nuestro website
-   Cerrar los registros
-   Comenzar a purgar las cuentas inactivas sin posibilidad de reactivarlas. Purgaremos automáticamente lxs usuarixs inactivxs por más de 4 meses sin previo aviso.
-   Mientras tanto, estaremos trabajando en el reemplazo de **Diaspora\***.

Nos gustaría agradecer a lxs desarrolladorxs de **Diaspora\*** por proporcionarnos la primera red social federada de todas, por todos los años que han estado con nosotros durante y antes del nacimiento de **Disroot**. Estamos tremendamente agradecidos por sus esfuerzos y trabajo. Haremos lo mejor para no dejar el protocolo de "La Federación" y seguir en contacto con la red **Diaspora\***.

Por ahora, sin embargo, "gracias por todo el pescado" y nos vemos del otro lado.
