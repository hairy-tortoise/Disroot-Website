---
title: 'FLISoL 2020'
media_order: flisol_2020.jpg
published: true
date: '14-04-2020 00:30'
taxonomy:
    category:
        - news
    tag:
        - flisol
        - news
body_classes: 'single single-post'

---
The **Latin American Communities of Free/Libre Technologies** are pleased to invite you to an intense day of talks, workshops and live installations.

On Saturday, April 25, 2020, starting at 09:00 (UTC-5) a new edition of **FLISoL** will take place. The **Festival Latinoamericano de Instalación de Software Libre** (Latin American Festival of Free Software Installation), is the biggest event of free/libre software diffusion and promotion that is being held since 2005 in different countries and simultaneously.

It is focused on all the people interested in knowing more about free/libre software and culture. During the meeting, the different local free software communities organize events in which free software is installed, legally and for free, on the computers of the attendees. At the same time, there are talks, lectures and workshops on local, national and Latin American issues related to Free/Libre Software, in all its range of expressions: artistic, academic, business and social.

This year, as a result of the Coronovirus outbreak, the Festival will be entirely online.

For more information, and to register for free, go to:
[**flisolonline.softlibre.com.ar**](https://flisolonline.softlibre.com.ar)
