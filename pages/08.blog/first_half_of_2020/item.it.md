---
title: 'Prima metà del 2020'
media_order: sunflower.jpg
published: true
date: '07-08-2020 13:10'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
body_classes: 'single single-post'
---

# Prima metà del 2020

Ciao,
la prima parte del 2020 è ormai alle spalle. È stato un periodo molto intenso, tanto che per i prossimi mesi non possiamo che aspettarci un'invasione aliena, l'esplosione del sole e la scoperta del teletrasporto e della macchina del tempo. Se guardiamo i recenti sviluppi mondiali, nessuno sarà particolarmente sorpreso se uno dei sopracitati eventi dovesse avverarsi, poiché la vita di tutti noi è stata stravolta in tutte le sue sfaccettature.
Visto che il progetto Disroot durante questo mese ha spento le sue prime cinque candeline, troviamo opportuno fare un piccolo resoconto dell'ultimo periodo e condividere alcune idee per i prossimi mesi.

## Caselle di posta crittografate

Come hai potuto leggere nei post precedenti, quest'anno ci siamo prefissati l'obiettivo di portare la crittografia nelle caselle di posta di Disroot. Abbiamo deciso di esplorare due alternative: 
1. Crittografia lato server
2. Crittografia end-to-end

**Crittografia lato server**: è una forma di crittografia in cui le chiavi pubbliche e private sono conservate sul server. È un sistema molto simile al modo in cui i file vengono crittografati sul cloud di Disroot (Nextcloud).
Per gli utenti non è necessario disporre o utilizzare alcun software aggiuntivo e mantenendo la compatibilità con i protocolli imap/pop3, gli utenti possono utilizzare i client di posta (app). Questa opzione purtroppo comporta alcuni (pochi) lati negativi. Il primo è ovviamente che le chiavi di crittografia sono memorizzate sul server, il che significa che conoscendo la password dell'utente (ottenuta ad esempio attraverso un attacco Man in the Middle o qualche forma di Brute Force) è possibile accedere al contenuto dei messaggi.
Inoltre la modifica/reimpostazione delle password presenta dei problemi: proprio come con il cloud, in caso di cambio password, gli utenti devono richiedere la rimozione delle chiavi di crittografia (non riuscendo però più a leggere i messaggi precedenti), oppure devono aggiornare le proprie chiavi con una nuova password.
Questa opzione comporta quindi alcuni benefici ma pure diversi grattacapi. Proprio per questo abbiamo deciso di esplorare pure altre possibilità.

**Crittografia End-to-end** è un sistema dove la chiave privata utilizzata per decifrare i messaggi è archiviata sul dispositivo dell'utente. In questo modo anche con l'accesso al server è impossibile decriptare i messaggi archiviati sul server senza prima entrare in possesso della chiave privata (che è stoccata solo sul dispositivo dell'utente). Alcuni providere che optano per questa soluzione, solitamente creano un ecosistema in cui è necessario utilizzare un unico client di posta fornito dalla piattaforma (non è quindi possibile utilizzare un client di propria scelta), sono incompatibili con altri provider (crittografia sviluppata internamente), non consentono di crontrollare le chiavi e alcuni di loro mantengono pure le chiavi private sui server (riproponendo le stesse problematiche della crittografia lato server).

Per noi è importante che la soluzione end-to-end dia ai Disrooter la libertà di scelta sul client, che fornisca il pieno controllo sulle chiavi di crittografia, che sia un'opzione a codice aperto utilizzabile anche da altri attori del Librehost.
 
Dopo esserci guardati attorno e aver provato diverse soluzioni, ci siamo imbattuti in un software chiamato *"GPG Mail Gate"*. Anche se sembra essere un progetto abbandonato, ha tutte le caratteristiche per risolvere le problematiche sopracitate.
-  Utilizza lo standard GnuPG per la crittografia delle e-mail. Ciò significa che funzionerà non solo all'interno di Disroot.

- Il caricamento della chiave pubblica permette di crittografare automaticamente tutte le email in arrivo, mentre la chiave privata rimane sul dispositivo dell'utente.

- Crittografia delle e-mail che invii: anche se invii come testo normale, tutte le e-mail inviate da te sarebbero archiviate in modo crittografato end-to-end sul server.

- Configurazione semplice: fa quello che deve fare senza richiedere competenze elevate.

### *Quindi,* *quando verrà implementato?*

Poiché il software è abbastanza obsoleto, vorremmo prima trovare un maintainer, riscriverne alcune parti e migliorarne l'implementazione nella webmail. Vorremmo inoltre renderlo compatibile con Autocrypt per renderlo ancora più robusto.

#### *Voglia di aiutare?*

Stiamo cercando degli sviluppatori che siano disposti ad assumersi la manutenzione del progetto, riscriverlo con python3 (o un altro linguaggio) e rifare il servizio web di caricamento delle chiavi. Per questo progetto stiamo cercando di ottenere dei finanziamenti, quindi potremmo pagare un po' più di una birra (forse una cassa di birra) per aiutare a finanziare il lavoro che ha il potenziale per avvantaggiare una rete più ampia di provider di posta elettronica. Se sei interessato a collaborare, mettiti in contatto con noi.
Puoi trovare il codice sorgente sul nostro [git](https://git.disroot.org/Disroot/gpg-mailgate)

## Temi

Per fornire un'esperienza più unificata, Meaz ha iniziato a lavorare sui temi di alcuni servizi. In questo momento abbiamo personalizzato Hubzilla, Lufi, Nextcloud, Roundcube *(in arrivo ...)*. Abbiamo quasi pronti pure Searx, Privatebin e ConverseJS. Questo lavoro ci permetterà pure di iniziare a lavorare sul design generale delle app che migliorerà l'esperienza dell'utente. Finora ci stiamo divertendo a farlo e siamo entusiasti di vedere cosa ne uscirà.


## Compenso per volontari

Disroot ha fatto molta strada per la ricerca dell'indipendenza e sostenibilità finanziaria.
Siamo partiti pagando di tasca nostra tutti i costi, poi grazie alle donazioni siamo riusciti a coprire le spese e pure fare delle donazioni a comunità di sviluppo di software FLOSS.

Abbiamo deciso che è tempo di fare un ulteriore passo avanti. Disroot è la nostra gemma preziosa. Ad essa dedichiamo più tempo che al nostro lavoro retribuito (non ditelo ai nostri capi). Mentre la piattaforma cresce, cresce anche il tempo necessario per la sua manutenzione e per il suo sviluppo. Il nostro obiettivo finale è raggiungere l'indipendenza finanziaria e concentrarci su Disroot come occupazione principale pure mantenendo saldi i principi sui quali questo progetto si fonda e si è sviluppato. La situazione attuale non è infatt sostenibile e la nostra vita privata/familiare ne soffre..

Siamo consapevoli del fatto che il percorso scelto per raggiungere un obiettivo così ambizioso non è facile. Crediamo che un approccio sociale all'economia, che consenta alle persone di decidere quanto possono e sono disposti a contribuire finanziariamente al progetto, sia possibile e possa generare un equilibrio sostenibile tra coloro che possono permettersi di pagare, coloro che non sono in grado di pagare e coloro che mantengono e sviluppano la piattaforma Disroot. Crediamo che questa sia la strada giusta per andare oltre, ma la strada che conduce a quell'obiettivo finale è lunga e richiederà molto tempo. Quindi, abbiamo deciso di fare un passo alla volta.

La legislazione olandese consente alle fondazioni come la nostra (nel nostro caso una no profit) di pagare una piccola quota per il lavoro di volontariato. Non può essere più di 170 euro al mese o più di 1700 euro all'anno in totale (= circa 140 euro al mese). Partendo da questo contesto, abbiamo deciso di avviare un programma di quote per i volontari del Disroot Core Team. Il Team è attualmente composto da 4 persone.

Oltre ai costi e alle donazioni FLOSS, quest'anno abbiamo anche deciso di accantonare fino a 400 euro al mese per spese impreviste oltre che per futuri investimenti in hardware. Una volta coperti i costi e le spese, se restano almeno 140 euro, pagheremo una quota di volontariato a uno dei membri del Team. Se ci saranno più di 280 euro potremmo pagare due volontari e così via. Come rete di sicurezza, se non riusciamo a raccogliere fondi sufficienti per tre mesi consecutivi, smetteremo di pagare i volontari.

Siamo lieti di annunciare che siamo già in grado di pagare la quota a due dei nostri volontari.
Contiamo sul tuo aiuto! È importante rendersi conto che qualsiasi importo è importante. Se tutti gli utenti di Disroot pagassero 1 euro al mese non solo saremmo in grado di pagare tutti i costi, investire in hardware migliore, sviluppare software a codice aperto senza sponsor, ma pure a dare un compenso agli amministratori di Disroot. Se puoi fai quindi una piccola donazione e cresciamo insieme.
Quando acquiti una bevanda al bar, pensa pure a Disroot: [pagina delle donazioni](https://disroot.org/donate).

## Report annuale 2019
Il report del 2019 è andato un po' per le lunghe. Potremmo dare la colpa al coronavirus, ma il reale motivo è che la grande mole di lavoro accumulata e la costante ricerca di priorità hanno spinto il rapporto annuale in basso nella lista dei todo. Finalmente però ci siamo riusciti. Eccolo [qui](https://disroot.org/en/annual_reports/AnnualReport2019.pdf).

Ci auguriamo che sia di vostro gradimento e dato che è uscito "leggermente" in ritardo è un buon momento per controllare i momenti salienti dell'anno passato.

## Cosa abbiamo pianificato per i prossimi mesi?
Vogliamo focalizzare il nostro lavoro su una migliore esperienza dell'utente. Fede sta lavorando duramente per aggiornare tutti i tutorial e gli howto e ne sta producendo di nuovi. Continueremo inoltre a lavorare sui temi. 
L'aspetto principale sul quale focalizzeremo il nostro impegno sarà però per migliorare il servizio di posta elettronica.
 Vogliamo lanciare una migliore protezione antispam, nuova webmail, crittografia delle caselle di posta e una migliore gestione delle richieste di domini e alias personalizzati.

Quindi, a meno che gli alieni non invadano il nostro pianeta, la Terra venga inghiottita da un buco nero o qualsiasi altro evento inaspettato si presenti quest'anno, noi guardiamo ai prossimi mesi, che saranno molti impegnativi, con ottimismo ed entusiasmo.
