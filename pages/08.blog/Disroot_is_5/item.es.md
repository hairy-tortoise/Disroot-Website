---
title: '¿5 años de Disroot?'
media_order: 2020-reset.png
published: true
date: '07-08-2020 13:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
body_classes: 'single single-post'
---

Debido a un error en el calendario, donde Marzo no ha terminado todavía, Disroot no cumplirá 5 este año. Estamos esperando con ansia al 2021 y la celebración por los 6 años.
