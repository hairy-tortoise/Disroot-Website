---
title: 'Feliz 2020'
media_order: aliasPinguine.jpg
published: true
date: '01-01-2020 19:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - fundraiser
        - alias
body_classes: 'single single-post'
---

Saludos del Equipo Disroot a todxs lxs Disrooters por ahí. Esperamos que hayan tenido toneladas de diversión anoche y estén ansiosos por el 2020 tanto como nosotrxs. El último año ha sido una locura de trabajo y parece que el entrante 2020 no será diferente. No podemos aguardar y esperamos que ustedes tengan también planes geniales para el año que comienza.

#¡El objetivo del DisrootAliasChallenge fue alcanzado!

Estamos comenzando el año con un anuncio asombroso. El objetivo para nuestra campaña de recaudación de fondos de la última semana ¡fue alcanzado! Recaudamos 1500€ un día antes de la fecha límite. ¡Queremos agradecer a todxs lxs que decidieron donar a Disroot no solo en esta última semana sino a lo largo de todo el año! Estamos muy agradecidos. Ustedes nos mantienen con vida ♥

Esto significa que @getgoogleoff.me será agregado como alias de correo para todxs lxs Disrooters muy pronto.

Si donaron más de 30€ a través de transferencia o criptomonedas, por favor, contáctenos con los detalles así podemos enviarles sus recompensas.

Y en caso que se hayan perdido el [video...](https://peertube.co.uk/videos/watch/5a3cbc03-1831-4dee-b2cd-92d80dd51f2b)

## ¿Qué sigue?

Actualmente, estamos cerrando el año, terminando las últimas cosas de nuestra hoja de ruta. También estamos trabajando para cristalizar los planes para el nuevo año al tiempo que escribimos el reporte 2019 que será publicado más adelante este mes.

### ¡Saludos, y gracias por quedarse con nosotrxs. Que el año nuevo sea increíble para todxs ustedes!
