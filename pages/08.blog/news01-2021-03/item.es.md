---
title: 'DisNews #1: Nuevo miembro en el equipo; Financiamiento para el cifrado del buzón; Período de Mantenimiento'
media_order: Protest_in_Myanmar_against_Military_Coup_14-Feb-2021_05.jpg
published: true
date: '27-03-2021 14:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - novedades
        - mailgate
body_classes: 'single single-post'
---
Los primeros tres meses han quedado atrás. El año 2021 no ha vuelto a la normalidad, a pesar de nuestras esperanzas globales compartidas. Parece que las cosas solo empeoran y no da la sensación que vayan a cambiar en ese sentido. Por lo tanto, nos gustaría enviar nuestros cálidos abrazos a todxs lxs que han estado luchando con la realidad durante los últimos meses. Estamos todxs juntxs en esto, mantengamos la concentración y la solidaridad entre nosotrxs porque parece que el 2020 fue solo un tutorial. Permanezcamos juntxs (cuidando la distancia, por supuesto), apoyémonos mutuamente e intentemos encontrar lo positivo en esta nueva realidad.

Este año hemos decidido cambiar la forma de comunicar las novedades. En lugar de limitarnos a publicar en las redes sociales y en el blog, hemos decidido también escribir un boletín informativo, ya que la mayoría de lxs Disrooters están aquí principalmente por el servicio de correo electrónico. El año pasado nuestras publicaciones fueron bastante escasas, pero este año esperamos hacer un mejor trabajo y publicar más regularmente. Así que, aquí estamos, escribiendo nuestro primer post de este año.

## avg_joe es un nuevo miembro del equipo central de apoyo
**Avg_joe**, al que algunxs de ustedes conocen principalmente por nuestra [Sala de chat de la Comunidad](https://webchat.disroot.org/#converse/room?jid=disroot@chat.disroot.org), se ha involucrado cada vez más en el proyecto. Le hemos preguntado si le gustaría ayudarnos más regularmente y amablemente ha aceptado. Así que durante las últimas semanas ha estado participando en nuestras reuniones quincenales y desde hace unos días también ha empezado a responder a los tickets entrantes de soporte. Su visión fresca de las cosas nos ha dado un nuevo punto de vista y ya estamos trabajando en algunas de las mejoras sugeridas por él. Estamos muy contentos de que **Avg_joe** haya decidido adoptar un enfoque más implicado y esperamos que disfrute trabajando para la comunidad tanto como nosotros. Denle una cálida bienvenida, especialmente cuando vean sus respuestas a sus tickets.

## Plan de Cifrado del Buzón para 2021 y financiación de la NLNET
El año pasado comenzamos a trabajar en la implementación del cifrado del buzón. Nos hemos decidido por el cifrado de extremo a extremo basado en **GnuPG**. Aunque originalmente habíamos planeado realizar esto en 2020, la realidad tuvo otros planes. En octubre, [NLNET](https://nlnet.nl/thema/NGIZeroPET.html) lanzó una convocatoria de presentación de proyectos para su financiación. Presentamos una solicitud y fue aprobada, por lo que vamos a obtener financiación para nuestro proyecto en 2021. Esto significa que por primera vez en la historia de **Disroot** hemos conseguido fondos para ayudar a desarrollar una funcionalidad que será beneficiosa para todxs lxs Disrooters y la comunidad en general. Nuestro trabajo será de código abierto y estamos planeando lanzar una campaña para promover nuestra solución y proporcionar suficiente documentación, tanto para lxs administradores de servicios como para lxs usuarixs finales, de manera que otrxs puedan implementarla también. Les iremos informando regularmente de los avances. Esperamos tener una prueba "cerrada" en funcionamiento más adelante este año y poder realizar pruebas beta abiertas en **Disroot** alrededor del invierno europeo. Así que, a menos que experimentemos una invasión extraterrestre, una erupción de volcán, tsunamis u otras catástrofes desconocidas, esperamos que el cifrado del buzón llegue finalmente a **Disroot**.

Una vez más, muchas gracias a **NLNet** por toda su ayuda.

## Período de mantenimiento quincenal
Una de las cosas que hemos decidido introducir recientemente es un período de mantenimiento quincenal. Lo hemos fijado en los martes por la tarde, a partir de las 20:30 (Hora Central de Europa). Durante este período, actualizaremos a nuevas versiones de software de los servicios que alojamos, implementaremos nuevas funciones importantes o realizaremos otros trabajos de mantenimiento que provoquen tiempos de inactividad. Todo ello se anunciará a través de nuestras cuentas en las redes sociales (Mastodon/Hubzilla) y se publicará en https://state.disroot.org (al que puedes suscribirte por correo electrónico o RSS) y en nuestra sala XMPP en xmpp:state@chat.disroot.org?join

Pensamos que este enfoque deja más claro, tanto para ustedes como para nosotros, en qué vamos a trabajar y qué servicio se interrumpirá durante ese tiempo. Además, el hecho de que se trate de un evento rutinario y recurrente evitará que se produzcan interrupciones aleatorias de los servicios y permitirá que los procedimientos sean claros y fáciles de seguir.

Para que el proceso sea más transparente, hemos creado un repositorio git en el que se puede seguir el historial (changelog) de los despliegues iniciados durante los períodos de mantenimiento. El siguiente paso es ofrecer ese registro de cambios en nuestro sitio web para que sea fácil y rápido echar un vistazo a lo que ha cambiado en la plataforma.

Hay un montón de cosas en desarrollo y aún más en preparación. A medida que se acerca la primavera (en Europa), nos ponemos más activos y excitados y, por supuesto, queremos gastar esa energía en nuestro amado proyecto. Sigan sintonizadxs para más noticias.

**¡Gracias por volar con Disroot!**
