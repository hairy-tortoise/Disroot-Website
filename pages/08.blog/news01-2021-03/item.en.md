---
title: 'DisNews #1 - New core member; Mailbox encryption funding; maintenance window'
media_order: Protest_in_Myanmar_against_Military_Coup_14-Feb-2021_05.jpg
published: true
date: '27-03-2021 14:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - mailgate
body_classes: 'single single-post'
---
First three months behind us. 2021 has failed to return to normality, despite our globaly shared flase hopes. Seems like things are getting only worse and does not look like thing will change in that regard. We would like to therefore, send our warm hugs to everyone who's been struggling with reality for the past months. We are all in it together, and let's stay focus and solidarity with each other because seems like 2020 was just a tutorial. Stick together (minding the distance of course), support each other and try to find positives in the new reality.

This year we have decided to change the way we communicate about what's new. Instead of just posting to social media and blog, we have also decided to write newsletter given that the majority of Disrooters are here mainly for email service. Last year our posting was rather sparse so this year we hope to do better job and post more on a regular basis. So here we are writing our first post this year.

## avg_joe is a new core team support member.

Avg_joe which some of you might know mainly from our [Community chat hangout room](https://webchat.disroot.org/#converse/room?jid=disroot@chat.disroot.org), has been more and more involved in the project. We have asked him whether he would like to help us more on regular basis and he kindly agreed. So for the past weeks he's been participating in our bi-weekly meetings and since few days also started answering incoming support tickets. His fresh look on things has already given us a new angle on things and we are working on some if his suggested improvements already. We are very happy that avg_joe decided to take on a more involved approach and we hope he's going to enjoy working for the community as much as we do. Give him a warm welcome, especially when you see his reply to your tickets.

## Mailbox encryption plan for 2021 and NLNET funding

Last year we have started work on implementing mailbox encryption. We have settled for end-to-end encryption based on GnuPG. As much as we have planned to accomplish this in 2020, reality had different plans. In October [NLNET]( https://nlnet.nl/thema/NGIZeroPET.html) has released a call for submission for project funding. We have submitted a request and, our application has been approved so we are getting funding for the project in 2021. This means that for the first time in Disroot's history we have managed to secure funds to help develop a feature that will be benefitial for all Disrooters and the community at large. Our work will be open sourced and we are planning on launching a campaign to promote the solution and provide enough documentation for both service administrators and end users, so that others could implement our solution too. We will be updating you regularly on the progress. We hope to have a working closed-testing later this year and hope for open beta tests on Disroot around the winter. So, unless we experience an alien invasion, a vulcano eruptions, tsunamis or other unknown catastrophes, we hope to have the mailbox encryption finally landing on Disroot.

Once again big thanks to NLNet for all the help.

## Bi-weekly maintenance window

One of the things we decided to introduce recently is a bi-weekly maintenance window. We settled for Tuesday evenings, starting from 20:30 (CET). During that window we will be rolling out new software releases of services we host, implement new big features or do other maintenance work that causes downtimes. All that is going to be announced via our accounts on social networks (mastodon/hubzilla) and posted on https://state.disroot.org (which you can subscribe via email or rss) and our xmpp room at xmpp:state@chat.disroot.org?join

We think this approach makes it clearer, for both you and ourselves, what we will be working on and which service will be interupted during the time. Furthermore, making it a routine, ra e-occuring event, will prevent random disruptions of services and allow for clear, easy to follow procedures.

To make the process more transparent, we have created a git repository where you can follow the history (changelog) of deployments commenced during the maintenance windows. Next step is to provide that changelog on our website for easy, and quick glance on what has changed on the platform.

There are tons of things in development and yet more in the pipeline. As spring approaches, we are getting more active and excited and we surely want to spend that energy on our lovely project. Stay tuned for more news.

Thanks for flying Disroot!
