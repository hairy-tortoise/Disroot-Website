---
title: 'Important! - Due to security vulnerability we have switched to different Webmail software!'
date: '22-04-2022'
media_order: warship.jpg
taxonomy:
  category: news
  tag: [disroot, rainloop, snappymail. email,]
body_classes: 'single single-post'

---
Dear Disrooters,

Due to [published security vulnerability](https://thehackernews.com/2022/04/unpatched-bug-in-rainloop-webmail-could.html?m=1) in the webmail software we use we have made a quick decision to migrate to a new piece of software based on Rainloop. Since we had to act fast, we had no time to properly test new software and prepare of all possible edge cases out there. If you experience any issues, please let us know so we are aware of them and can provide a fix.
<br><br>
Additionally, we have disabled Rainloop nextcloud app (mail app in cloud.disroot.org) since there is no replacement yet, and removed two factor authentication for webmail for now. Why removing 2FA?
<br><br>
Two factor authentication in the mail works only on webmail. This means anyone could just use IMAP or POP3 client which does not support it, and access the emails. 2FA in webmail was giving a false feeling of security and was something we were never fans of, but did enable it due to continous user requests. Instead, we will work on a Disroot-wide two factor authentication solution which will provide a proper security.
<br><br>
We are sorry for any inconvenience but we hope you understand. The issue was potentially very severe (check in the link posted above). We would like to send our kudos to folks behind SnappyMail for providing improved webmail software. 
