---
title: 'Interrupción importante - Correo, Nextcloud, XMPP'
date: '31-01-2022'
media_order: sorry.jpg
taxonomy:
  category: news
  tag: [disroot, novedades, mantenimiento, caida, issues]
body_classes: 'single single-post'

---
Ayer, 30 de enero, realizamos algunos trabajos de mantenimiento en el centro de datos. La tarea principal fue el remplazo de una batería de caché defectuosa del controlador raid que se había roto unos días antes. Aunque el procedimiento transcurrió según lo planeado, durante el proceso de reinicio del servidor (necesario para realizar la sustitución de la pieza), el sistema de archivos se corrompió. Esto quiere decir que para volver a poner el servidor en línea tuvimos que reparar el sistema de archivos. Esta operación demoró 12 horas en total, período durante el cual el correo, el chat XMPP y Nextcloud no estuvieron accesibles. Adicionalmente, hubo alguna pérdida de datos, lo que significa que pueden haber perdido algún archivo o correo que hayan subido justo antes del reinicio del servidor.


Queremos darles nuestras más sinceras disculpas y esperamos no haber complicado demasiado su día con la interrupción. Aunque esto fue (aparentemente) inevitable, vamos a trabajar en una mejor comunicación durante los tiempos de interrupción. Esperamos también minimizar estos largos cortes en el futuro con el nuevo hardware que estamos por comprar, y que nos permitirá reconstruir la infraestructura actual añadiendo algo de redundancia (tanto como podamos permitirnos, por supuesto).

Aunque ahora está operativo, todavía vemos algunos [inodos](https://es.wikipedia.org/wiki/Inodo) corruptos en la partición de datos donde almacenamos los correos electrónicos y los archivos de la nube. Esto significa que tendremos que ejecutar otra comprobación en esa partición del disco, lo que implica a su vez que tendremos que poner el servicio fuera de línea durante unas horas. Es decir que continuaremos el trabajo de mantenimiento el sábado 5 de febrero de 2022 a partir de las 0:00 CET. Pensamos que no llevará mucho tiempo, pero es aconsejable estar preparados para unas horas de inactividad. El sábado por la noche (horario de Europa) parece ser el momento en el que el tráfico en el servidor es más bajo, por lo que esperamos afectar al menor número de personas posible.


**De nuevo, les pedimos disculpas por la caída y les agradecemos por su comprensión y apoyo durante este momento.**
