---
title: 'Großer Ausfall - Mail, Nextcloud, XMPP'
date: '31-01-2022'
media_order: sorry.jpg
taxonomy:
  category: news
  tag: [disroot, news, maintenance, downtime, issues]
body_classes: 'single single-post'

---

Gestern, am 30. Januar, haben wir einige Wartungsarbeiten im Datencenter durchgeführt. Hauptaufgabe war das Ersetzen einer fehlerhaften Cache-Batterie eines RAID-Controllers, welche ein paar Tage zuvor kaputtging. Obwohl die Arbeiten wie geplant liefen, wurde während des Rebootvorgangs des Servers, welcher für den Teiletausch notwendig war, das Dateisystem beschädigt. Das heißt, dass wir, um den Server wieder online zu kriegen, das Dateisystem reparieren mussten. Diese Aufgabe dauerte insgesamt mehr als 12 Stunden und während dieser Zeit waren E-Mail, XMPP-CHat und Nextcloud nicht erreichbar. Zusätzlich enstand ein Datenverlust, was bedeutet, dass möglicherweise eine Datei oder eine E-Mail, die Ihr vor dem Server-Reboot hochgeladen habt, verlorengegangen ist.

Wir senden Euch unsere tiefsten Entschuldigungen und hoffen, dass wir Euren Tag mit diesem Ausfall nicht gestört haben. Auch, wenn dies (offenbar) unvermeidlich war, werden wir an einer besseren Kommunikation während der Ausfallzeiten arbeiten. Wir hoffen, solche langen Ausfälle in der Zukunft mit neuer Hardware, deren Kauf wir gerade vorbereiten, vermeiden zu können. Diese neue Hardware wird es uns ermöglichen, die aktuelle Infrastruktur mit einiger Redundanz zu erweitern (soweit wir uns das leisten können natürlich).

Obwohl wir derzeit wieder operativ sind, haben wir noch einige fehlerhafte Inoden auf der Datenpartition, auf der wir E-Mails und CLouddaten speichern. Das bedeutet, dass wir eine weitere Überprüfung dieser Partition durchführen müssen. Daher werden wir den Service erneut für ein paar Stunden abschalten müssen. Das bedeutet außerdem, dass wir die Wartungsarbeiten fortsetzen. Wir werden am Samstag, 05. Februar, um 0:00 Uhr MEZ damit beginnen. Wir denken nicht, dass es allzu lange dauern wird, aber es könnte vorteilhaft sein, sich auf ein paar Stunden Downtime einzustellen. Samstagnacht (europäische Zeitzone) scheint die Zeit zu sein, in der am wenigsten Verkehr auf dem Server ist, daher hoffen wir, dass die Auswirkungen für die Menschen so gering wie möglich sein wird.


**Nocheinmal: Wir bitten um Entschuldigung für den Ausfall. Wir danken Euch für Euer Verständnis und Eure Unterstützung während dieser Zeit.**
