---
title: 'Welcome new Team members'
date: '23-06-2019 0:10'
media_order: penguins.jpg
taxonomy:
  category: news
  tag: [disroot, news, CoreTeam, xmpp, nextcloud]
body_classes: 'single single-post'

---

This time we are bringing you several announcements and some big news we can't wait to share with you.

## Nextcloud performance improvements
The last two weeks we've been focusing on fixing some long standing issues. Our Nextcloud instance seemed to be consuming a lot more resources than it should.  After weeks of battling and searching for the root cause on our Nextcloud setup, we have finally managed to find it and fix it. The issue was related to what seems like a bug in Bookmarks app that caused the creation of millions of cache files, causing enormous strain on our CPU. After simply removing all bookmark cache files, the issue has stopped and Nextcloud did not produce new caches since. In turn, our CPU load has dropped by a factor of 5, which is great news. Nextcloud feels very snappy again and our stress levels are back to where we can actually focus on other things.

## Hubzilla still not happy
Unfortunately we can't say the same about our Hubzilla instance. As some of you know, for the last year or so, we've been experimenting with a new federated services platform called Hubzilla. Our goal is to bring it to a state where we can provide it to a wider Disrooters audience. For the last weeks however, we are struggling with stability of the platform. We think we have identified the root cause but we have not manage to fix it yet. The work on this will continue throughout the upcoming sprints. We want to focus more of our time and energy on this project as we think Hubzilla is the go to social network experience and we would love to promote it.

## XMPP page and botcamp
Antilopa has worked on new looks for the XMPP page which now includes more information about the beloved chat protocol and our particular setup and lists a number of XMPP clients. The changes look great and we will be implementing them throughout all the pages in the coming sprints. [https://disroot.org/en/services/xmpp](https://disroot.org/en/services/xmpp)

<br><br>
We have also started deployment of XMPP bots. Bots appear to look like normal users in the chats but they are in fact,  programs. They can do a lot of things, from remembering and reminding things for you, keeping track of your meetings or telling you the weather, to completely useless tasks such as sending you pictures of kittens, creating memes or even throwing insults at you. All the bots hang out at: **botcamp@chat.disroot.org** where you can test them out. At this moment you can't just simply invite them to your room so you need to request it to the admins (best via disroot@chat.disroot.org room) but soon we hope to find a solution to that also, so you will be able to invite bots to your room without admins interaction. As mentioned in the previous post, if you are looking for home for your bot, we are running a special XMPP instance that is dedicated to bot hosting.

## An improved Disroot Team.
We are very proud and extremely happy to announce our team has grown. Since the inception almost 4 years ago the Disroot team consisted of two people: Muppeth and Antilopa. For the past two years a group of fanatically dedicated Disrooters was helping us in all sort of ways. Starting from translations, writing howtos, creating Hubzilla and Disroot apps, helping test features and debug issues and much much more. It did feel for us they were part of the family and it just felt natural that they officially join the team receiving all the fame and glory but also sharing the workload and responsibility for the operation and well-being of the platform. We are very grateful for all the work they have done so far and we would like to introduce to you the new members of the Disroot Team: Fede, Maryjane, Meaz and Massimiliano. For all of us it's a new experience and this definitely starts a new chapter for the project. In the coming weeks we will focus on finding the best way to coordinate and plan work, share knowledge and tasks, develop optimal decision making process and tons of other processes.

## Mission Statement
Yup, we finally got around to it. Since the moment we officially registered our non-profit foundation, we wanted to write a mission statement defining our ideas. After two years we have finally created such document. Our mission statement defines our ideas for Disroot.org Project, its principals, our view on funding, decision making process and our ambitions for the future. You can read it [here](https://disroot.org/en/mission-statement)
<br><br><br>
As you can see, there are quite some things going on. With new members and extreme high energy we will strive to provide the best experience ever, while having a lot fun doing it.
<br><br>
If you haven't done it yet, please think about contributing. The monthly contribution of "a coffee of your choice" for your beloved web services platform is really necessary because we choose to NOT finance it by doing business with people's data. It is also important to remember that if all users could donate the equivalent of just one coffee a month, that would not only help us keep the platform running and improve services for everyone (for those who can contribute financially and for those who can't), it would also allow us to expand the scope of the project. Please check our [donation page](https://disroot.org/en/donate) for detailed overview.
<br><br><br>

And talking about expanding the scope, if you run a community, an interesting project or something you would like to share with other Disrooters, send us an email to *discover_at_disroot.org* and we will give you a shout-out in our blog posts.
