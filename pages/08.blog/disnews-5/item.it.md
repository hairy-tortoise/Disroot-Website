---
title: 'DisNews #5 - Nuovi server; Spostamento di messaggi di spam; Nuovi prezzi; Lacre; Chat control'
date: '16-06-2022'
media_order: graffitti.jpg
taxonomy:
  category: news
  tag: [disroot, news, servers, spam, prices, lacre, chatcontrol]
body_classes: 'single single-post'

---

Ciao Disrooter!

Come al solito, il tempo vola! Siamo stati parecchio presi negli ultimi mesi, soprattutto nel contesto della nostra vita privata. Siamo comunque riusciti a trovare il tempo per proporre delle novità che vogliamo condividere.

# Nuovi server

Grazie a tutte le donazioni che abbiamo ricevuto negli ultimi anni, siamo lieti di comunicarvi che siamo stati in grado di acquistare tre nuovi server! Questi server sostituiranno quelli attuali. E poiché il processo di implementazione nel rack richiederà del tempo, vogliamo sfruttare questo momento per ricostruire e migliorare la nostra infrastruttura sulla base dell'esperienza dei 7 anni di vita di Disroot. Una volta che i server saranno tutti attivi e funzionanti, godremo tutti di prestazioni maggiori. Il nuovo hardware dovrebbe supportare le esigenze attuali di Disroot e quelle degli anni a venire. Non vediamo l'ora di iniziare a usarli e speriamo che tutti ne possano trarre beneficio.

**Mille, mille grazie quindi a tutti coloro che ci hanno supportato finanziariamente nel corso degli anni. Siete gli eroi che portano avanti questo progetto.**

Ovviamente, la nostra riserva di cassa è notevolmente diminuita. Quindi saremmo davvero felici se potessi aiutare con una donazione. Non dimenticare che il prezzo di una semplice tazza di caffè può fare la differenza.

Visita la nostra pagina [donazione](https://disroot.org/it/donate) per avere una panoramica sulla nostra situazione finanziaria attuale e unisciti al gruppo di Disrooter che sono i nostri donatori regolari di caffeina!
Le tue donazioni consentiranno al progetto Disroot di rimanere sostenibile, indipendente e di essere pronto nel caso di imprevisti.

# Spostamento di messaggi di spam

Come probabilmente saprai, combattere lo SPAM è una battaglia dura e senza fine. Ogni volta che pensi di avere tutto sotto controllo, gli spammer trovano nuovi modi per tenerti occupato. Di recente, abbiamo cambiato il modo in cui funziona il nostro sistema. Una di queste modifiche è stata l'aggiunta di un'etichetta "[Avviso SPAM]" nell'oggetto delle e-mail ritenute sospette dal nostro sistema anti-spam. Finora non avevamo eseguito alcuna azione aggiuntiva su tali e-mail. Tuttavia, poiché abbiamo allentato alcune delle impostazioni, la quantità di spam è aumentata. Molti di voi hanno suggerito di spostare automaticamente le email contrassegnate come spam nella cartella "Posta indesiderata". Per molto tempo ci siamo opposti a questa idea perché pensiamo che gli utenti dovrebbero essere in grado di decidere autonomamente cosa fare con il loro messaggi (anche quelli di spam). Il recente importante aumento dello spam in entrata, ci ha però fatto cambiare approccio. Questo speriamo aiuti soprattutto quegli utenti con meno esperienza o tempo per impostare i filtri e che stanno attualmente "nuotando" nello spam.
Quindi, a partire da oggi, abbiamo deciso che **tutte le email contrassegnate come spam verranno automaticamente spostate nella cartella "Posta indesiderata"**. Quindi, se non riesci a trovare alcune email che stavi aspettando, controlla quella cartella.

# Nuovi prezzi

Abbiamo apportato un grande cambiamento. Abbiamo deciso di aggiornare il nostro sistema di tariffazione e lo abbiamo fatto per due motivi: trasparenza e semplicità.

**I nostri prezzi ora includono le commissioni di transazione** che consentiranno ai Disrooter di sapere subito quanto dovranno donare, invece di scoprire i costi aggiuntivi quando ricevono la fattura. Inoltre non dobbiamo più calcolare quali sono le commissioni per ciascuna fattura e metodo di pagamento (_soprattutto perché le piattaforme di pagamento a volte le cambiano_). Ciò darà anche ai Disrooters una maggiore flessibilità nel modificare il metodo di pagamento, perché non dovremo creare nuove fatture a dipendenza del metodo di pagamento scelto.
D'ora in poi, **il pagamento sarà solo annuale**. Non solo ci consentirà di dedicare meno tempo a ricreare le fatture ogni mese, ma sarà anche meno costoso per quanto riguarda le commissioni di transazione per i Disrooter.

Ecco i nostri **nuovi prezzi** annuali:

|    |     |    |
|----|:---:|----|
|11€ |➡️|5GB |
|20€ |➡️|10GB| 
|29€ |➡️|15GB|
|56€ |➡️|30GB|
|83€ |➡️|45GB|
|110€|➡️|60GB|

**È importante notare che per i residenti dei paesi dell'Unione Europea dobbiamo aggiungere l'IVA** (imposta sul valore aggiunto) **oltre a questi prezzi, che è del 21%**.
Infine, abbiamo anche deciso di consentire agli utenti di **allocare questo spazio di archiviazione aggiuntivo come desiderano** tra cloud e posta. Ad esempio, se un Disrooter dona per 10 GB di spazio di archiviazione, questo utente potrebbe decidere di avere 8 GB per il cloud e 2 GB per la posta. Questi gigabyte extra verranno aggiunti allo spazio di archiviazione predefinito fornito al momento della registrazione dell'account.

# Lacre

È in corso un grande sviluppo su **Lacre, il nostro progetto di caselle postali crittografate end-to-end**. **Pfm** è stato molto impegnato e coinvolto e abbiamo raggiunto alcuni traguardi importanti. Ha finalizzato l'aggiornamento della base di codice di back-end a _python3_ e ha introdotto una serie di _unit test_ che consentono una programmazione più semplice e una migliore qualità del codice. Abbiamo finalizzato la procedura per l'implementazione del server che ci ha permesso di eseguire i primi veri e propri test sul campo. Le cose stanno effettivamente andando avanti e non vediamo l'ora che Lacre raggiunga il server Disroot. Ci vorrà tuttavia ancora del tempo, vogliamo infatti essere sicuri che il sistema sia robusto, ottimizzato e che la maggior parte dei bug e dei problemi siano stati risolti.

I nostri ringraziamenti speciali vanno a **NLNET** per il supporto finanziario a questo progetto.
https://lacre.io/


# Chat control

Infine, ma non di certo meno importante, vogliamo segnalare un possibile preoccupante cambiamento nella legislazione dell'Unione Europea che riguarderà tutti.

La Commissione europea sta infatti lavorando a una legislazione chiamata **Chat control**, che obbligherebbe le piattaforme online a scansionare contenuti come testo, immagini e video creati dagli utenti al fine di rilevare materiale relativo ad abusi sessuali su minori. Ciò rischia di creare pericoloso precedente in cui ogni messaggio, immagine o video condiviso su Internet sarà soggetto a sorveglianza e censura. Aprirebbe le porte ai governi e alle dittature del presente e del futuro di rintracciare, eliminare e mettere a tacere l'opposizione di qualsiasi tipo (l'abuso sui minori in questo caso sarebbe solo il pretesto per la sorveglianza). Offrirebbe a coloro che hanno accesso a questo sistema il controllo sulla popolazione sapendo chi dice cosa a chi e quando. È chiaro che minerebbe la libertà delle persone e il nostro diritto alla privacy. Questa nuova legge, se tradotta nel mondo reale, significherebbe essenzialmente che ogni conversazione privata che hai con chiunque, dovrebbe essere registrata e inviata alle autorità come misura preventiva.
Questo è totalmente inaccettabile e per questo questa proposta, dal nostro punto di vista, dovrebbe essere respinta.
I governi si stanno impegnando molto per porre fine alle comunicazioni private e alla libertà che Internet ci offre. Usano dei pretesti, cercando di venderli al pubblico come misure preventive e protettive, ma la realtà dei fatti è che si tratta di sistemi di sorveglianza di massa. Noi continuiamo a batterci, anche se le probabilità che questo ennesimo strumento di sorveglianza venga accettato sono molto alte.

Se ritieni la privacy un elemento fondamentale, fai tutto il possibile per aiutare a fermare questa ennesima follia. Contatta i tuoi deputati e commissari europei, organizza manifestazioni, fai sentire la tua voce! Se perdiamo il nostro diritto alla comunicazione privata, perderemo un'ulteriore libertà! Agisci ora perché potrebbe essere già troppo tardi!

Se vuoi capire meglio e trovare nuove argomentazioni contro questa proposta, leggi il testo di **Patrick Breyer**, ex giudice e attuale membro del parlamento europeo(https://www.patrick-breyer.de/en/posts/messaging-and-chat-control/).


Il Team Disroot
