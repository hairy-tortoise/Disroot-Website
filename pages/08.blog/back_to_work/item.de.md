---
title: "Zurück an die Arbeit"
date: '04-09-2019 11:30'
media_order: coffee.jpg
taxonomy:
  category: news
  tag: [disroot, news, sprint, translations, conversejs, hubzilla]
body_classes: 'single single-post'

---

*(Sprint 43)*

Hallo da draußen. Ausgeruht? Gut gegessen, ein paar Ausflüge gemacht? Sehr schön. Und jetzt zurück an die Arbeit!

Die letzten Wochen waren, wie wir im letzten Blogbeitrag geschrieben haben, sehr arbeitsreich. Bis hin zu dem Punkt, an dem wir erkennen mussten, dass wir uns zu viel aufbürden, was bedeutet, dass wir dazu neigen, Dinge anzufangen aber nicht zeitgerecht zu beenden. Jetzt kommen wir jedoch langsam dazu, zuvor begonnene Dinge zu beenden, sodass hier nun ein kleiner Haufen von Dingen kommt, mit denen wir kürzlich beschäftigt waren. Ach ja, macht Euch keine Sorgen, wir hatten auch ein bisschen Freizeit.

# Neue Übersetzungen

In den letzten Wochen haben wir verschiedene Übersetzungssoftware getestet, konnten aber keine finden, die unseren Ansprüchen gerecht wird. Websites und Dokumentationen sind, anders als Applikationen, wesentlich schwerer durch spezialisierte Software zu übersetzen und erfordern viel Nacharbeit für geringe Verbesserungen. Also haben wir beschlossen, mit der Arbeit per **Git** weiter zu machen. Das erfordert zwar vielleicht ein etwas höheres Fertigkeitslevel, aber das Erlernen der Grundlagen von **Git** und **Markdown** erbringt auch nützliche Fertigkeiten.

Also haben wir damit begonnen, einen neuen Ablauf zur Erstellung, Änderung und Übersetzung der Howtos und Tutorials zu implementieren. Wir haben außerdem eine sehr schöne Übersicht des aktuellen Übersetzungsstatus erstellt, eine README-Datei für das **Howto**-Repository und ein **Taiga**-Board, auf dem sogar mehrere Übersetzer einer Sprache ihre Arbeit koordinieren können. Bis dahin läuft alles sehr gut. Wir arbeiten immer noch an einigen Einstellungen, an der Verbesserung der Dokumentation und der Prozessoptimierung, aber wir bewegen uns in die richtige Richtung, um alle Hilfen und Tutorials in so vielen Sprachen wie möglich anbieten zu können. Du kannst Dir den **Howto-Übersetzungsstatus** [hier](https://howto.disroot.org/en/contribute/translations_procedure/state/Translations_Howto.pdf) und den **Website-Übersetzungsstatus** [hier](https://howto.disroot.org/en/contribute/translations_procedure/state/Translations_Website.pdf) ansehen (beide benötigen schon wieder ein Update dank der vielen eingereichten Übersetzungen).

In den letzten paar Wochen haben wir mit den Übersetzungen der Website und einiger Howtos ins **Russische** und **Deutsche** begonnen und Revisionen und Updates der **französischen**, **italienischen** und **spanischen** Übersetzungen vorgenommen. Den größten Verdienst an diesen Fortschritten haben die Disrooter **shadowsword**, **wisbit**, **l3o** und **keyfear** durch ihre umfangreiche Mitarbeit und ihre Beiträge nicht nur mit den Übersetzungen sondern auch mit Feedback und ihrer gemeinschaftlichen Einstellung.

**Fede** war ein fleißiges Bienchen und hat all die Arbeit koordiniert und zusätzlich die ganzen Änderungen und Übersetzungen geprüft, genehmigt und zusammengeführt. Er hat an Verbesserungen bei den Abläufen und Übersichten gearbeitet.

Wenn Du ein Teil des Teams werden und mit Übersetzungen oder Howtos helfen möchtest, trete unserem XMPP-Raum bei:[howto@chat.disroot.org](https://webchat.disroot.org/#converse/room?jid=howto@chat.disroot.org)<br>

 - [Übersetzungs-Board](https://board.disroot.org/project/fede-disroot-translations/epics)
 - [Howto-Board](https://board.disroot.org/project/disroot-disroot-h2/epics)

# Neues Howto-Theme

In den vergangenen Wochen war **Antilopa** beschäftigt mit der Erstellung eines neuen Themes für [https://howto.disroot.org](https://howto.disroot.org). Vor einiger Zeit haben wir gemerkt, dass das alte Theme unserem Bedarf nicht gerecht wird. Die Informationen wurden mit wachsender Anzahl an Tutorials immer schwieriger zu finden. Der Effekt ist, wie immer, wirklich riesig. **Antilopa** hat ein wirklich schönes, minimalistisches und sauberes Design entworfen, das Aussehen und Gefühl unserer Website wiedergibt. Wir wurden schon darüber informiert, dass es immer noch ein paar Probleme mit der Darstellung auf ein paar Bildschirmen gibt, und **Antilopa** arbeitet an der Behebung. Wenn Du irgendwas findest, melde das bitte auf unserem [Problem-Board](https://board.disroot.org/project/disroot-disroot/issues). Denk bitte daran, den Prefix *[WEBSITE]* im Titel der Problemmeldung anzugeben, damit der Fehler schneller gefunden und behoben werden kann.

# Verbesserung interner Abläufe

**Meaz** war mit unserer Supportticket-Warteschlange beschäftigt, die so unordentlich geworden war wie eine Studentenbude nach einer einwöchigen Hausparty. Das Ergebnis seiner Bemühungen sind eine verbesserte, klarere Support-Warteschlange und die Einführung von Schichten. Jede Woche ist nun eine andere Person verantwortlich für die Zuordnung der Tickets, die Eingangsbearbeitung und die Arschtritte für diejenigen, die mit zu vielen Antworten und Kommentaren alles aufhalten (Winkewinke, Du weißt wer gemeint ist ...).
<br><br>

**Fede** hat ein schönes Willkommens-Dokument fertiggestellt, das in Kürze irgendwo auf der Website platziert wird. Es soll bei der Eingewöhnung neuer Nutzer helfen. Wir haben auch eine lange Diskussion darüber geführt, wie wir dafür sorgen können, dass neue Nutzer sich hier schneller heimisch fühlen. Wir wissen, dass es hier noch viele raue Kanten gibt. Unser Ziel ist es, an einer einheitlicheren Erfahrung quer durch die Services zu arbeiten. Wir haben einige Zeit mit Brainstorming und der Definition der greifbarsten Ziele verbracht und werden diese in den kommenden Sprints implementieren.

# FAQ

Ja! Endlich! Keine Antworten a la *'nur den Nutzernamen und nicht Nutzername@disroot.org'* mehr zum gefühlt hundertsten Mal an einem Tag :P Dank **meaz** haben wir endlich ein **FAQ** begonnen und auf der Website zusammengestellt **\o/**. Zusätzlich hat er damit begonnen, unseren Bot **milkman** (Der Name soll noch geändert werden, der Witz ist nicht länger lustig) zu trainieren, um die **FAQ** im Chat zu beantworten. **Antilopa** wird weiterhin an optischen und UX-Verbesserungen der Seite arbeiten und natürlich werden wir weitere Fragen und noch mehr Antworten hinzufügen.

# Red Pill
![](redpill_0.png)

**RedPill** ist unser Ansatz für den **XMPP**-Webchat von **Opcode**, genannt **ConverseJS**. In den letzten Wochen hat **Muppeth** sich entschlossen, als Nebenprojekt am Aussehen des Webchats zu arbeiten. Das Resultat davon kannst Du Dir unter [https://webchat.disroot.org](https://webchat.disroot.org) anschauen. Das ist nur der Anfang und eine lange erwartete Änderung. In naher Zukunft werden wir uns damit beschäftigen, verschiedene Features einzubauen wie Audio-/Video-Konferenzen, Sprachnachrichten, Reaktionen und auch optische Verbesserungen. Der Name für unsere Gestaltung des Client war eine Last-Minute-Idee, weshalb die dominierende Hauptfarbe blau statt rot ist, aber wir werden testen, wie sich rot in diesem Setting machen würde. Wer die Arbeit am Webchat verfolgen möchte, kann [https://devchat.disroot.org](https://devchat.disroot.org) benutzen, wo wir mit Ideen spielen und ihre Umsetzung austesten. Den Quellcode unserer Änderungen werden wir veröffentlichen, sobald wir mit dem Umzug unserer derzeitigen Git-Repositories in ihr neues Zuhause fertig sind.

Hier ein paar Screenshots:
![](redpill_1.png)

![](redpill_2.png)

# Diaspora-Addon in Hubzilla

... Zeit für schlechte Neuigkeiten. Obwohl wir unzählige Stunden damit verbracht haben, Probleme in unserem **Hubzilla**-Connector zum **Diaspora**-Netzwerk zu prüfen, haben wir immer noch keinen Fortschritt im Hinblick auf das Finden einer Lösung gemacht. Aktuell (so wie tatsächlich seit Monaten) ist das **Diaspora**-Addon deaktiviert. Wir haben für den Moment entschieden, nicht noch mehr Ressourcen einzusetzen um eine Lösung zu finden. Wir werden dennoch weiterhin Rückmeldungen an die **Hubzilla**-Hauptentwickler geben. Wir hoffen, dass irgendwann, wenn nicht durch uns dann vielleicht durch jemand anderen, der Fehler gefunden und behoben wird. Wir werden uns nun darauf konzentrieren, **Hubzilla** dahin zu bringen, aus dem offenen Betatest herauszukommen und seinen Platz auf der Website als das standardmäßige Soziale Netzwerk zu erhalten.

<br><br>
Wie Du sehen kannst, war das eine Menge Zeug. Keine Sorge, die nächsten Postings werden deutlich kürzer, da wir nun sehr strikt sind bei der Menge der Arbeit, die wir angehen :P
