---
title: 'Sicuro'
wider_column: left
---

# Sicuro e protetto

- Dati compressi e criptati nel browser prima dell'invio al server. Utilizza 256 bit AES.Server non ha conoscenza dei dati memorizzati.
- I tuoi dati sono al sicuro anche in caso di violazione o sequestro del server.
- I motori di ricerca sono ciechi per quanto riguarda il contenuto dell'testo.
- La discussione è naturalmente anche crittografata/decrittografata nel browser.
- Il server non può vedere il contenuto dei commenti o i soprannomi.
- Con la scadenza della testo, è possibile avere una discussione ad hoc di breve durata che scomparirà nel vuoto dopo la scadenza. Questo non lascerà alcuna traccia delle vostre discussioni nelle vostre caselle di posta elettronica.
- Le discussioni non sono indicizzate dai motori di ricerca.

---

![](en/privatebin02.gif)
