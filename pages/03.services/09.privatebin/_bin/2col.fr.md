---
title: Bin
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://bin.disroot.org/">Coller un pastebin</a>
<a class="button button1" href="http://fgb4city54qtch7pdulgryete6hora4dmsrbuygoczrcdtlgpudaftid.onion">Tor</a>

---


![](private-bin.png)
**PrivateBin** est un pastebin et un forum de discussion en ligne minimaliste et open-source. Les données de chaque pastebin sont chiffrées/déchiffrées de bout en bout dans le navigateur de sorte que le serveur n'a aucune connaissance des données hébergées.

Vous n'avez pas besoin d'un compte sur Disroot pour utiliser ce service.

Désactivez Pastebin : [https://bin.disroot.org](https://bin.disroot.org)

Page d'accueil du projet : [https://privatebin.info/](https://privatebin.info/)

Le code source : [https://github.com/PrivateBin/PrivateBin](https://github.com/PrivateBin/PrivateBin)
