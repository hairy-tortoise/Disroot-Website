---
title: Bin
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://bin.disroot.org/">Paste bins</a>
<a class="button button1" href="http://fgb4city54qtch7pdulgryete6hora4dmsrbuygoczrcdtlgpudaftid.onion">Tor</a>

---


![](private-bin.png)
**PrivateBin** es un [pastebin](https://es.wikipedia.org/wiki/Pastebin) y tablero de discusión en línea, minimalista y de código abierto.
La información de cada pastebin es cifrada/descifrada punto a punto en el navegador, de esa manera el servidor tiene cero conocimiento de la información alojada.

No necesitas una cuenta de **Disroot** para utilizar este servicio.

Pastebin de Disroot: [https://bin.disroot.org](https://bin.disroot.org)

Página del proyecto: [https://privatebin.info/](https://privatebin.info/)

Código fuente: [https://github.com/PrivateBin/PrivateBin](https://github.com/PrivateBin/PrivateBin)
