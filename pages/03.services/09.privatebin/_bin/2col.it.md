---
title: Bin
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://bin.disroot.org/">Paste bins</a>
<a class="button button1" href="http://fgb4city54qtch7pdulgryete6hora4dmsrbuygoczrcdtlgpudaftid.onion">Tor</a>

---


![](private-bin.png)
PrivateBin è un pastebin online minimalista, open-source e un pannello di discussione.
I dati di ogni pastebin sono criptati/decodificati nel browser in modo che il server non abbia alcuna conoscenza dei dati ospitati.

[https://bin.disroot.org](https://bin.disroot.org)

Homepage del progetto: [https://privatebin.info/](https://privatebin.info/)
[Codice sorgente](https://github.com/PrivateBin/PrivateBin)
