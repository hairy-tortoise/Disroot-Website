---
title: Bin
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://bin.disroot.org/">Paste bins</a>
<a class="button button1" href="http://fgb4city54qtch7pdulgryete6hora4dmsrbuygoczrcdtlgpudaftid.onion">Tor</a>

---


![](private-bin.png)
**PrivateBin** is a minimalist, open-source online pastebin and discussion board. The data of every pastebin is end to end encrypted/decrypted in the browser so that the server has zero knowledge of hosted data.

You don't need any account on Disroot to use this service.

Disroot Pastebin: [https://bin.disroot.org](https://bin.disroot.org)

Project Homepage: [https://privatebin.info/](https://privatebin.info/)

Source code: [https://github.com/PrivateBin/PrivateBin](https://github.com/PrivateBin/PrivateBin)
