---
title: 'Facile'
wider_column: right
---

![](en/privatebin01.gif)

---

# Davvero facile da usare

 - Incolla il testo, clicca su "Invia", condividi l'URL.
 - Scadenza: 5 minuti, 10 minuti, 10 minuti, 1 ora, 1 giorno, 1 settimana, 1 mese, 1 anno o mai.
 - Opzione "Brucia dopo la lettura": il testo viene distrutto quando viene letto.
 - URL di cancellazione unico generato per ogni testo incollato.
 - Colorazione della sintassi per 54 lingue (usando highlight.js), che supporta il mixaggio (html/css/javascript).
 - Conversione automatica degli URL in link cliccabili (http, https, ftp e magnet).
 - Singolo pulsante per clonare una pastebin esistente.
 - Discussioni: è possibile attivare la discussione su ogni pastebin.
