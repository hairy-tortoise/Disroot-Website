---
title: 'Clientes para Mumble'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
clients:
    -
        title: Mumble
        logo: mumble.png
        link: https://www.mumble.info/
        text: Sitio web oficial
        platforms: [fa-linux, fa-windows, fa-apple]
    -
        title: Mumble-Web
        logo: webbrowser.png
        link: https://mumble.disroot.org/
        text: ¡Nuestro Mumble de Disroot en línea!
        platforms: [fa-linux, fa-windows, fa-apple, fa-android]

    -
        title: Plumble
        logo: plumble.png
        link: https://github.com/acomminos/Plumble
        text: Cliente para Android
        platforms: [fa-android]

    -
        title: Mumla
        logo: mumla.png
        link: https://mumla-app.gitlab.io/
        text: Un fork de Plumble
        platforms: [fa-android]


---

<div class=clients markdown=1>

</div>
