---
title: Audio
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://mumble.disroot.org/">Conéctate</a>
<a class="button button1" href="http://vd2k6x3cvqmm7pt2m76jpcyq7girnd3owykllkpu73rxzqv2cbs5diad.onion">Tor</a>

---

![mumble_logo](mumble.png?resize=100,100)


## Mumble

**Audio de Disroot** está desarrollado por **Mumble**, una aplicación de audio chat de alta calidad de sonido, baja latencia, libre y de código abierto. Fue pensado originalmente para [gamers](https://es.wikipedia.org/wiki/Jugador_de_videojuegos), pero puede ser utilizado para organizar reuniones, audio conferencias, etc.

**¡AVISO!**

No necesitas tener cuenta para utilizar **Mumble**. Pero tienes mayores privilegios si registras tu nombre de usuarix.

Mumble Disroot: [mumble.disroot.org](https://mumble.disroot.org)

Sitio del Proyecto: [https://www.mumble.info](https://www.mumble.info)

Código fuente: [https://github.com/mumble-voip/mumble](https://github.com/mumble-voip/mumble)
