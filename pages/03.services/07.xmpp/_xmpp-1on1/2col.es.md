---
title: 'XMPP Mensajería directa'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](monal.jpg)

---

## Chats 1 a 1

Habla libremente con cualquier Disrooter o con cualquiera que utilice servidores XMPP compatibles en internet. Tú decides si almacenar o no cualquier historial del chat en el servidor o habilitar el cifrado. El nombre de usuarix es parecido a una dirección de correo que se corresponde con el servidor XMPP (p.e: nombre_de_usuarix@servidor.com).   
