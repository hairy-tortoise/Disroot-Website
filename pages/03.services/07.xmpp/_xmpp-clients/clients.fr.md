---
title: 'Clients Xmpp'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
clients:
    -
        title: Dino
        logo: dino.png
        link: https://dino.im
        text:
        platforms: [fa-linux]
    -
        title: Gajim
        logo: gajim.png
        link: https://gajim.org/
        text:
        platforms: [fa-linux, fa-windows]

    -
        title: BeagleIM
        logo: beagle.png
        link: https://beagle.im/
        text:
        platforms: [fa-apple]

    -
        title: Monal IM
        logo: monal.png
        link: https://monal.im/
        text:
        platforms: [fa-apple]

    -
        title: Conversations
        logo: conversations.png
        link: https://conversations.im/
        text:
        platforms: [fa-android]

    -
        title: Siskin
        logo: siskin.jpg
        link: https://siskin.im
        text:
        platforms: [fa-apple]

    -
        title: Profanity
        logo: profanity.png
        link: http://profanity.im/
        text: "Console based XMPP client"
        platforms: [fa-linux, fa-windows, fa-apple, fa-android]

    -
        title: converse.js
        logo: conversejs.svg
        link: https://webchat.disroot.org
        text: "XMPP client in your browser"
        platforms: [fa-firefox]

---

<div class=clients markdown=1>

</div>
