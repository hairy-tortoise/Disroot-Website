---
title: XMPP
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<br><br><br><br>
<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Sign up for Disroot</a>
<a class="button button1" href="https://webchat.disroot.org/">Log in via webchat</a>

---
![](xmpp_logo.png?resize=128,128)

# XMPP
### Decentralized and Federated Instant Messaging

Communicate using a standardized, open and federated chat protocol, with the ability to encrypt your communication with OMEMO protocol (based on encryption method also used by services such as Signal and Matrix). With XMPP you are not bound to one service provider (e.g. Disroot server) but can freely communicate with contacts from other Jabber [servers](https://compliance.conversations.im/), just like you would communicate between different email servers.


Project homepage: [https://xmpp.org](https://xmpp.org)
Server implementation: [https://prosody.im](https://prosody.im)

<a href='https://compliance.conversations.im/server/disroot.org'><img src='https://compliance.conversations.im/badge/disroot.org'></a>
