---
title: 'Clients Jabber'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

# Utiliser votre client/app de chat favori
Le choix entre les clients bureau/web/mobile est grand. Prenez celui qui vous convient le mieux. [https://xmpp.org/software/clients.html](https://xmpp.org/software/clients.html)
