---
title: 'Passerelles Xmpp'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](conversations2.jpg)

---

## Passerelles, Bridges et Transports

XMPP permet une grande variété de moyens de connexion aux protocoles de chats.

### Se connecter à une salle IRC
Vous pouvez vous connecter à n'importe quelle salle IRC hébergée sur n'importe quel serveur IRC via notre passerelle IRC Biboumi :
<ul class=disc>#salle%irc.domain.tld@irc.disroot.org</i></b></li>
<li>Pour ajouter un contact IRC: <b><i>nomducontact%irc.domain.tld@irc.disroot.org</i></b></li>
</ul>

Où ***#salle*** est la salle IRC que vous souhaitez rejoindre et ***irc.domain.tld*** est l'adresse du serveur IRC que vous voulez rejoindre. Assurez-vous de laisser ***@irc.disroot.org*** tel quel, car c'est l'adresse de la passerelle IRC.

### Se connecter à une salle Matrix
Vous pouvez vous connecter à n'importe quelle salle Matrix hébergée sur n'importe quel serveur Matrix via la passerelle Matrix.org :
<ul class=disc>
<li>Pour rejoindre une salle Matrix: <b><i>#salle#matrix.domain.ldt@matrix.org</i></b></li>
</ul>

Où ***#salle*** est la salle Matrix que vous souhaitez rejoindre et **matrix.domain.tld*** est l'adresse du serveur Matrix que vous souhaitez rejoindre. Veillez à laisser ***@matrix.org*** tel quel, car il s'agit de l'adresse du pont Matrix.


### A venir...
Dans le futur, nous prévoyons de gérer notre propre pont Matrix. Nous espérons également fournir un pont Telegram et un certain nombre d'autres ponts et transports.
