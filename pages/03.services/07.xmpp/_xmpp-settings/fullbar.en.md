---
title: 'XMPP Settings'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## Server settings
### XMPP ID:<span style="color:#8EB726"> <i>yourusername</i>@disroot.org </span>
##### Advanced settings: server/port<span style="color:#8EB726"> disroot.org/5222 </span>

---

#### Upload file size limit:<span style="color:#8EB726"> 512 MB </span>
#### Archived messages and files expire after <span style="color:#8EB726"> 1 month </span>
<br>
