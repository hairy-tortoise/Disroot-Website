---
title: 'XMPP Settings'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## Server-Einstellungen:
### XMPP ID:<span style="color:#8EB726"> <i>deinnutzername</i>@disroot.org </span>
##### Erweiterte Einstellungen: Server/Port<span style="color:#8EB726"> disroot.org/5222 </span>

---

#### Uploadlimit Dateigröße:<span style="color:#8EB726"> 512 MB </span>
#### Archivierte Dateien und Nachrichten verfallen nach: <span style="color:#8EB726"> 1 Monat </span>
<br>
