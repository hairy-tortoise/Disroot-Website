---
title: 'Easy to use'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Easy to use
Just start a meeting and share its link with other members.
You don't even need any account to use it, and it can be run in your browser, without installing any additional software on your computer.

---

![](easy.png)
