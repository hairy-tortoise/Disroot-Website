---
title: 'Facile à utiliser'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Facile à utiliser
Il suffit de commencer une réunion et de partager son lien avec les autres membres.
Vous n'avez même pas besoin de compte pour l'utiliser, et il peut être exécuté dans votre navigateur, sans installer de logiciel supplémentaire sur votre ordinateur.

---

![](easy.png)
