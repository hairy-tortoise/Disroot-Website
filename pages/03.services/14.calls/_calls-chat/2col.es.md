---
title: 'Chat'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Chat
Utiliza el chat de texto junto con tu conferencia de audio/video.

---

![](chat.png)
