---
title: 'Chat'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Chat
Usa una chat di testo insieme alla tua conferenza audio/video. 

---

![](chat.png)
