---
title: Calls
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://calls.disroot.org/">Start a conference</a>
<a class="button button1" href="http://oubguq76ii5svwyplbheorayf2nmoxy7inyd43a24adq24sy7jahjvyd.onion">Tor</a>

---

![](jitsi_logo.png?resize=150,150)

**Disroot's Calls** service is a videoconferencing software, powered by **Jitsi-Meet**. It provides you high quality video and audio conferences, with as many partners as you want. It also allows to stream your desktop or only some windows to other participants in the call.


Disroot Calls: [https://calls.disroot.org/](https://calls.disroot.org/)

Project homepage: [https://jitsi.org/jitsi-meet/](https://jitsi.org/jitsi-meet/)

Source code: [https://github.com/jitsi/jitsi-meet](https://github.com/jitsi/jitsi-meet)
