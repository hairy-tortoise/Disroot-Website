---
title: Calls
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://calls.disroot.org/">Inizia una videochiamata</a>
<a class="button button1" href="http://oubguq76ii5svwyplbheorayf2nmoxy7inyd43a24adq24sy7jahjvyd.onion">Tor</a>

---

![](jitsi_logo.png?resize=150,150)

Il servizio **Disroot's Calls** è un software di videoconferenza, basato su **Jitsi-Meet**. Ti offre videoconferenze e audio di alta qualità. Consente inoltre di condividere lo schermo o solo alcune finestre con altri partecipanti alla chiamata. 

Disroot Calls: [https://calls.disroot.org/](https://calls.disroot.org/)

Homepage del progetto: [https://jitsi.org/jitsi-meet/](https://jitsi.org/jitsi-meet/)

Codice sorgente: [https://github.com/jitsi/jitsi-meet](https://github.com/jitsi/jitsi-meet)
