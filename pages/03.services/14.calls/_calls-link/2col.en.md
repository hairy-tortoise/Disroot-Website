---
title: 'Custom link'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](link.png)

---

# Custom link
You can choose your own conference room name, that will be your conference's address.
