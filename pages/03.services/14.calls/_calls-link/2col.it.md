---
title: 'Custom link'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](link.png)

---

# Link personalizzati
Puoi scegliere il nome della tua sala conferenze, che sarà l'indirizzo (link) della tua chiamata. 
