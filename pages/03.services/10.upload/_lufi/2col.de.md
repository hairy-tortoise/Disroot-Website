---
title: Upload
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://upload.disroot.org/">Dateien hochladen</a>
<a class="button button1" href="http://m2g6xbbo64mgxjjzxlbdtmypmvdqjxgtuluhoxqvozr27s63fasfz5id.onion">Tor</a>

---

![](lufi.png)
## Lufi - verschlüsselter, temporärer Dateiupload-Service

Disroots Uploadservice ist eine Filehosting-Software auf der Basis von **Lufi**. Mit diesem Service kannst Du temporär Dateien aufbewahren, um sie mit einem Link mit anderen zu teilen.
Um den Datenschutz zu gewährleisten werden alle Dateien im Browser selbst verschlüsselt - das heißt, dass Deine Dateien niemals unverschlüsselt Deinen Rechner verlassen. Die Administratoren sind nicht in der Lage, den Inhalt Deiner Dateien zu sehen, ebenso wie Dein Netzwerkadministrator oder Provider. Die Administratoren können nur den Dateinamen, ihre Größe und den mime-Typ (die Art der Datei: Video, Text, etc.) sehen.

Quellcode: [https://framagit.org/fiat-tux/hat-softwares/lufi](https://framagit.org/fiat-tux/hat-softwares/lufi)
