---
title: Upload
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://upload.disroot.org/">Upload files</a>
<a class="button button1" href="http://m2g6xbbo64mgxjjzxlbdtmypmvdqjxgtuluhoxqvozr27s63fasfz5id.onion">Tor</a>

---

![](lufi.png)
## Lufi - Encrypted temporary file upload service

Disroot's Upload service is a file hosting software, powered by **Lufi**. It temporarily stores files so you can share them with others using a link.
To protect privacy, all the files are encrypted in the browser itself - It means that your files never leave your computer unencrypted. The administrators will not be able to see the content of your file, nor will your network administrator or your ISP. The administrators can only see the file's name, its size and its mimetype (what kind of file it is: video, text, etc).

Source code: [https://framagit.org/fiat-tux/hat-softwares/lufi](https://framagit.org/fiat-tux/hat-softwares/lufi)
