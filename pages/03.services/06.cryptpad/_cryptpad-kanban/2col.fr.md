---
title: 'Kanban'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](todo.png)

## Listes de choses à faire et tableaux kanban simples chiffrés
Créez, partagez et gérez facilement vos listes de choses à faire chiffrées et vos tableaux kanban de projet !


---
![](whiteboard.png)

## Tableaux blancs
Des tableaux blancs chiffrés vous permettent de dessiner en temps réel avec d'autres personnes. Tout est chiffré de bout en bout !
