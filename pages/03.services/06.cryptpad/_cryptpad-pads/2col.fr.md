---
title: 'Pads'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](pads.png)

## Pads riches
L'éditeur **Rich text** vous permet de créer des fichiers texte chiffrés de bout en bout avec une fonction de collaboration en temps réel. L'éditeur permet un formatage standard. Tous les pads peuvent être édités par plusieurs personnes en même temps.


---
![](sheet.png)

## Feuille de calcul en temps réel
**Les feuilles de calcul en temps réel** vous permettent de créer des feuilles de calcul collaboratives. L'application supporte tous les formats standards (libreoffice/MS office). Tout cela grâce à l'incroyable *"Only Office "* et à la possibilité d'éditer en groupe.
