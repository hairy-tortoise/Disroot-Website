---
title: 'Pads'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](pads.png)

## Pad formattati
L'editor **Rich text** ti consente di creare file di testo crittografati end-to-end con funzionalità di collaborazione in tempo reale. Editor consente la formattazione standard. Tutti i pad possono essere modificati con più persone contemporaneamente. 

---
![](sheet.png)

## Fogli di calcolo in tempo reale
**Puoi creare fogli di calcolo collaborativi. L'applicazione supporta tutta la formattazione standard (libreoffice/MS office). Tutto ciò grazie all'incredibile suite di *Only Office*. 
