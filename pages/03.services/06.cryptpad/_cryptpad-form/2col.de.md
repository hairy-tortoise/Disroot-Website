---
title: 'Form'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](form.png)

## Ende-zu-Ende verschlüsselte Formulare
**Form** ermöglicht Dir das Erstellen und Teilen vollständig Ende-zu-Ende verschlüsselter Formulare.


---
![](presentation.png)

## Verschlüsselte Präsentationen im laufenden Betrieb
Erstelle Ende-zu-Ende verschlüsselte Präsentationen mit einem einfachen Editor und zusammen mit Deinen Freunden oder Teammitgliedern. Deine fertige Präsentation kann direkt von Cryptpad gestartet werden.
