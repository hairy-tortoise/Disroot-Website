---
title: 'File storage'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](drive.png)

## Archiviazione di file crittografati
Carica e condividi qualsiasi file. Tutti i file archiviati sono crittografati end-to-end! 


---
![](code.png)

## Editor di codice collaborativo
Modifica il tuo codice insieme ai membri del tuo team su un sistema crittografato end-to-end e con zero knowledge sul lato server. 
