---
title: 'Paramètres'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
Chaque utilisateur dispose de **250Mo d'espace de stockage** pour les fichiers.

---

**Bien que Cryptpad ne nécessite pas de compte, vous pouvez en créer un si vous souhaitez conserver tous vos blocs, fichiers et sondages au même endroit. Vous devrez créer un compte séparé pour le Cryptpad. Votre compte Disroot ne fonctionnera pas !

---
