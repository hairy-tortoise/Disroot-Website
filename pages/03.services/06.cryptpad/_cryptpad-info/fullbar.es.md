---
title: 'CryptPad info'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
Cada usuarix obtiene **250MB de espacio de almacenamiento** para archivos.

---

**Aunque CryptPad no requiere una cuenta, puedes crearla si quieres conservar tus documentos, archivos y encuestas en un solo sitio. Necesitarás crear una cuenta separada para CryptPad. Tu cuenta de Disroot no funcionará.**

---
