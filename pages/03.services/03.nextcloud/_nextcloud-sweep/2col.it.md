---
title: 'Nextcloud Keep or Sweep'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-sweep.png?lightbox=1024)

---

## Mantieni o spazza

Questa app ti aiuta a fare ordine ricordandoti di eliminare i file di cui potresti esserti dimenticato. 
