---
title: 'Nextcloud Checksum'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---

## Checksum app

Permette agli utenti di creare hash checksum di un file secondo i seguenti algoritmi: md5, sha1, sha256, sha384, sha512 e crc32.

---

![](en/checksum.gif)
