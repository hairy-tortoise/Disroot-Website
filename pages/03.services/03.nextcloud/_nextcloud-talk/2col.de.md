---
title: 'Talk'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Wähle Deinen bevorzugten Client'
clients:
    -
        title: Talk
        logo: de/talk_logo.png
        link: https://f-droid.org/de/packages/com.nextcloud.talk2/
        text:
        platforms: [fa-android]

---
## Talk

Audio-/Video-Konferenzschaltungen. Kommuniziere sicher mit Deinen Freunden und Deiner Familie über Deinen Browser mit Rich-Audio und -Video. Alle Gespräche laufen mithilfe von WebRTC-Technologie über eine Peer2Peer-Verbindung. Daher ist die Internetgeschwindigkeit der Teilnehmer entscheidend und nicht die Ressourcen des **Disroot**-Servers.

Du kannst öffentliche Links erstellen, die Du mit Menschen teilen kannst, die keinen **Nextcloud**-Account haben.

---

![](de/call-in-action.png?lightbox=1024)
