---
title: 'Cifrado Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/NC_encryption.png?lightbox=1024)

---

## Cifrado:

Todos los archivos que son almacenados en la nube de **Disroot** están cifrados con claves generadas a partir de tu contraseña. Nadie, ni siquiera los administradores del sistema, puede ver el contenido de tus archivos, solo pueden ver el nombre del archivo, su tamaño y su mime-type (el tipo de archivo que es: video, texto, etc).

###### <span style="color:red">¡¡¡ADVERTENCIA IMPORTANTE!!!</span>
**En caso que olvides tu contraseña no serás capaz de recuperar tus archivos a menos que hayas seleccionado previamente la opción "Recuperación de contraseña" en tu configuración "Personal". Sin embargo, esto implica dar permiso a los administradores para acceder a tus archivos. La manera más segura es nunca perder tu contraseña y siempre tener un respaldo de tus archivos en algún otro lugar.**

*Por favor, consulta [este](https://howto.disroot.org/es/tutorials/user/account/administration) tutorial.*

**Nextcloud** emplea solo cifrado del lado del servidor. Esto significa que tu par de claves de cifrado (privada y pública) son almacenadas en el servidor y son vulnerables a [ataques de fuerza bruta](https://es.wikipedia.org/wiki/Ataque_de_fuerza_bruta) o [ataques de intermediario](https://es.wikipedia.org/wiki/Ataque_de_intermediario) en caso que el servidor se viera comprometido o fuera confiscado.
