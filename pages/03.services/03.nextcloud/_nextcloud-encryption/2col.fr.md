---
title: 'Chiffrement Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/NC_encryption.png?lightbox=1024)

---

## Chiffrement:

Tous vos fichiers stockés sur le cloud de **Disroot** sont chiffrés avec des clés de chiffrement générées par votre mot de passe. Personne, pas même les administrateurs système* ne peut voir le contenu de vos fichiers. L'administrateur ne peut voir que le nom du fichier, sa taille et son type de mime (quel type de fichier c'est: vidéo, texte, etc.).

###### <span style="color:red">!!! AVERTISSEMENT IMPORTANT !!!</span>
**Si vous oubliez votre mot de passe, vous ne pourrez pas récupérer vos fichiers à moins que vous ne choisissiez l'option Récupération de mot de passe dans vos paramètres personnels. Cela signifie cependant que vous donnez théoriquement aux administrateurs la permission d'accéder à vos fichiers. Le moyen le plus sûr est de ne jamais perdre votre mot de passe et de toujours avoir vos fichiers sauvegardés ailleurs.**
*Veuillez vous référer à ce [tutoriel](https://howto.disroot.org/fr/tutorials/user/account/administration).*
