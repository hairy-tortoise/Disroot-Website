---
title: 'Federación Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---

## Federación

Gracias a la federación puedes vincular y compartir tus carpetas con cualquiera que esté utilizando otras instancias de **Nextcloud** (o de **ownCloud**) distintas de **Disroot**.

---

![](en/NC_federation.png?lightbox=1024)
