---
title: 'De bout en bout'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

![](en/endtoend-android-nw.png?lightbox=1024)

---

## Chiffrement de bout en bout

Le chiffrage de bout en bout **Nextcloud** offre la protection ultime de vos données, il permet aux utilisateurs de sélectionner un ou plusieurs dossiers sur leur poste de travail ou leur client mobile pour un cryptage de bout en bout. Les dossiers peuvent être partagés avec d'autres utilisateurs et synchronisés entre les périphériques mais ne sont pas lisibles par le serveur.

<span style="color:red">**Le chiffrement de bout en bout est toujours à l'état alpha, ne l'utilisez pas en production et uniquement avec des données de test !**</span>

Pour en savoir plus https://nextcloud.com/endtoend/
