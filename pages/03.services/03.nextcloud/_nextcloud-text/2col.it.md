---
title: 'Nextcloud Text'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Testo

Crea file in markdown e modificali direttamente online con un'anteprima live. Puoi anche condividerli e renderli modificabili da chiunque. 

---

![](en/nextcloud-text.png?lightbox=1024)
