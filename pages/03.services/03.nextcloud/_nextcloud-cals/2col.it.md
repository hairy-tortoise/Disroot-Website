---
title: 'Calendari e contatti'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Scegli il tuo client preferito'

clients:
    -
        title: GOA
        logo: en/goa.png
        link: https://wiki.gnome.org/Projects/GnomeOnlineAccounts
        text: 'Gnome online accounts'
        platforms: [fa-linux]
    -
        title: Kaddressbook
        logo: en/KDE.png
        link: https://kde.org/applications/office/org.kde.kaddressbook
        text:
        platforms: [fa-linux]
    -
        title: Thunderbird
        logo: en/thunderbird.png
        link: https://www.thunderbird.net/
        text:
        platforms: [fa-linux, fa-windows]
    -
        title: DAVx⁵
        logo: en/davx5.png
        link: https://f-droid.org/en/packages/at.bitfire.davdroid/
        text:
        platforms: [fa-android]

---

## Calendari e contatti

Condividi in modo facile e veloce i tuoi calendari con altri utenti o gruppi di Nextcloud. Archivia i tuoi contatti in Nextcloud e condividili tra i tuoi dispositivi in modo da avere sempre accesso ad essi, indipendentemente dal dispositivo su cui ti trovi..

---

![](en/nextcloud-cal.png?lightbox=1024)
