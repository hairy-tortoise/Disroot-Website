---
title: 'Cerchie di Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-circles.png?lighbox=1024)

---

## Cerchie

Crea  gruppi di utenti/colleghi/amici per condividere in modo più semplice i contenuti. Le proprie cerchie possono essere pubbliche, private (accessibili tramite invito) oppure segrete.
