---
title: 'Deck'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
cloudclients: true
clients_title: 'Elige tu cliente favorito'
clients:
    -
        title: Deck
        logo: en/deck_logo.png
        link: https://f-droid.org/en/packages/it.niedermann.nextcloud.deck/
        text:
        platforms: [fa-android]
---

![](en/nextcloud-deck.png?lightbox=1024)

---

## Tablero

Deck te ayuda a organizar tu trabajo personal o en equipo dentro de **Nextcloud**. Puedes agregar tareas, clasificarlas, compartirlas y comentar en ellas.
