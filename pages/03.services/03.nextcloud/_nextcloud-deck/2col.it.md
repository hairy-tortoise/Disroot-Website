---
title: 'Deck'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
cloudclients: true
clients_title: 'Scegli il tuo client preferito'
clients:
    -
        title: Deck
        logo: en/deck_logo.png
        link: https://f-droid.org/en/packages/it.niedermann.nextcloud.deck/
        text:
        platforms: [fa-android]
---

![](en/nextcloud-deck.png?lightbox=1024)

---

## Deck

Deck ti aiuta a organizzare il tuo lavoro personale o di gruppo all'interno di Nextcloud. È possibile aggiungere attività, classificarle, condividerle e commentarle.
