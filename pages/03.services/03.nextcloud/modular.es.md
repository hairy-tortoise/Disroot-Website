---
title: Nube
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _nextcloud
            - _nextcloud-x-storage
            - _nextcloud-highlights
            - _nextcloud-encryption
            - _nextcloud-e2e
            - _nextcloud-federation
            - _nextcloud-webclient
            - _nextcloud-sync
            - _nextcloud-share
            - _nextcloud-cals
            - _nextcloud-gallery
            - _nextcloud-text
            - _nextcloud-deck
            - _nextcloud-tasks
            - _nextcloud-notes
            - _nextcloud-pads
            #- _nextcloud-news
            - _nextcloud-bookmarks
            - _nextcloud-circles
            - _nextcloud-talk
            - _nextcloud-sweep
            - _nextcloud-checksum
body_classes: modular
header_image: smokecloud.jpg

translation_banner:
    set: true
    last_modified: März 2022
    language: Spanish
---
