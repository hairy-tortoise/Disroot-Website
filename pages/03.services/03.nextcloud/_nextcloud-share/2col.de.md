---
title: 'Nextcloud Teilen'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](de/nextcloud-files-comments.png?lightbox=1024)

---

## Teilen und Zugriffskontrolle

Du kannst Dateien mit anderen Nutzern innerhalb von **Disroot** teilen, passwortgeschützte öffentliche Links erstellen und versenden, andere Nutzer Dateien in Deine Cloud hochladen lassen und Mitteilungen erhalten, wenn ein Nutzer Dateien direkt mit Dir teilt.
