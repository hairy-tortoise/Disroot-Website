---
title: 'Cloud Storage'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Cloud Storage Space

If the FREE 2GB is not enough, you can extend your cloud storage.

Here are the prices **per year, payment fees included**:

||||
|---:|---|---:|
| 5GB |......| 11€ |
| 10GB |......| 20€ |
| 15GB |......| 29€ |
| 30GB |......| 56€ |
| 45GB |......| 83€ |
| 60GB |......| 110€ |


<br>
Transactions within the EU are subjected to extra VAT (Value Added Tax) of 21%.

---

<br><br>

<a class="button button1" href="/forms/extra-storage-space">Request Extra Storage</a>
