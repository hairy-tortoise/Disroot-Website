---
title: 'Bloc de notas Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Blocs de notas colaborativos


Crea blocs o planillas de cálculo directamente desde **Nextcloud** y dales seguimiento.

**¡Aviso! Los blocs y planillas de cálculo son parte de otro servicio y su contenido no es almacenado en tu nube. Adicionalmente, cualquiera que, hipotéticamente, pudiera "adivinar" la dirección aleatoria de los blocs podría acceder a ellos.**

---

![](en/OCownpad.png?lightbox=1024)
