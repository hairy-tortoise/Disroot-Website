---
title: 'Nextcloud Tasks'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Task

Organizza e gestisci i tuoi task. Grazie allo standard caldav puoi sincronizzare i tuoi task su più dispositivi.

---

![](en/OCtasks.png?lightbox=1024)
