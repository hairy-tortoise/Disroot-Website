---
title: Nextcloud
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Sign up for **Disroot**</a>
<a class="button button1" href="https://cloud.disroot.org/">Log in</a>
<a class="button button1" href="http://3rhtbo7bb3o5qvx2iyirppxxtpwnlxkavdyjdckmuzh2uohibignufqd.onion">Tor</a>

---
![](Nextcloud_Logo.png)

**Disroot's Cloud** service is powered by **Nextcloud**. It allows you to host and share your files online and have several online functionalities, such as calendar sharing, contacts management, video calls and much more.

**Nextcloud** offers safe, secure, and compliant sharing solution based on standards compatible with any operating system.
Most importantly, all your data is stored on our encrypted cloud instance! This means that no one is able to see the content of your files if not explicitly allowed by you. Not even system administrators.

Disroot Cloud: [https://cloud.disroot.org](https://cloud.disroot.org)

Project Homepage: [https://nextcloud.com](https://nextcloud.com)

Source code: [https://github.com/nextcloud/server](https://github.com/nextcloud/server)
