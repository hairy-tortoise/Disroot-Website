---
title: Nextcloud
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">**Disroot**-Account registrieren</a>
<a class="button button1" href="https://cloud.disroot.org/">Anmelden</a>
<a class="button button1" href="http://3rhtbo7bb3o5qvx2iyirppxxtpwnlxkavdyjdckmuzh2uohibignufqd.onion">Tor</a>

---
![](Nextcloud_Logo.png)

**Disroots Cloudservice** haben wir mit **Nextcloud** realisiert. Die Cloud ermöglicht es Dir, Deine Dateien online zu speichern und zu teilen. Zusätzlich hast noch diverse nützliche Online-Funktionen, wie das Teilen von Kalendern, Kontakte-Management, Videoanrufe und vieles mehr.

**Nextcloud** bietet eine zuverlässige, sichere und komfortable Cloud-Lösung, die mit jedem Betriebssystem kompatibel ist.
Besonders wichtig ist, dass alle Deine Daten auf unserer verschlüsselten Cloud-Instanz gespeichert sind! Das heißt, niemand ist in der Lage, den Inhalt Deiner Dateien zu sehen, wenn er nicht Deine ausdrückliche Erlaubnis hat. Nicht mal die Systemadministratoren.

Disroot-Cloud: [https://cloud.disroot.org](https://cloud.disroot.org)

Projekt-Website: [https://nextcloud.com](https://nextcloud.com)

Sourcecode: [https://github.com/nextcloud/server](https://github.com/nextcloud/server)
