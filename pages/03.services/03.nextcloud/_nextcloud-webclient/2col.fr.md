---
title: 'Nextcloud Webclient'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

![](en/webclient.png)

---

## Accédez à Nextcloud de n'importe où

Nextcloud est accessible depuis n'importe quel appareil via votre navigateur Web. Vous pouvez donc utiliser les différentes applications présentées ci-dessous directement depuis votre ordinateur, sans rien installer d'autre qu'un navigateur web.
