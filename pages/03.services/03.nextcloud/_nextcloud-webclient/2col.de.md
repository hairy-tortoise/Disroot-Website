---
title: 'Nextcloud Webclient'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

![](en/webclient.png)

---

## Zugriff auf Nextcloud von überall her

**Nextcloud** ist von jedem Gerät aus mit Deinem Webbrowser zugänglich. Du kannst also alle Anwendungen des Servers direkt von Deinem Computer aus nutzen, ohne etwas anderes als einen Webbrowser zu installieren.
