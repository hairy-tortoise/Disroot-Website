---
title: Pads
bgcolor: '#FFF'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _pads
            - _etherpad-highlights
            - _etherpad-features
            - _empty-bar
body_classes: modular
header_image: 'riot-tagging.jpeg'

translation_banner:
    set: true
    last_modified: Mars 2022
    language: French
---
