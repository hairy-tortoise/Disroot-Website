---
title: Akkoma
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://fe.disroot.org/">Log in</a>

---

![akkoma_logo](akkoma.png?resize=100,100)


## Akkoma

**Akkoma** is a microblogging server software that can federate (= exchange messages with) other servers that support ActivityPub, like Friendica, GNU Social, Hubzilla, Mastodon, Misskey, Peertube, and Pixelfed.

Disroot Akkoma: [https://fe.disroot.org](https://fe.disroot.org)

Project homepage: [https://akkoma.social/](https://akkoma.social/)

Source code: [https://akkoma.dev/AkkomaGang/akkoma](https://akkoma.dev/AkkomaGang/akkoma)
