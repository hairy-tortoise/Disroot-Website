---
title: 'Akkoma Features'
wider_column: left
---

## Federate
You can exchange messages with people on any ActivityPub or OStatus servers, like GNU Social, Friendica, Hubzilla and Mastodon.

## Write what you have in mind!
With Akkoma, you're not restricted to 150 characters! You can add links, images, polls, etc.

## Notifications
Receive notifications when someone adds you as a contact, when you're mentioned in a post or when one of your posts in shared by someone.

## Chat
Send messages to other members of Disroot's Akkoma with a real time chat.

---
<b>

![](en/Akkoma_home.png?lightbox=1024)
