---
title: 'Règle'
bgcolor: '#8EB726'
fontcolor: '#FFF'
text_align: center
---

## Disroot n'enregistre pas votre adresse IP!
*Utilisez le bouton en haut de la page pour installer le plugin de recherche disroot sur votre firefox.*
