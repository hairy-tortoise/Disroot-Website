---
title: Ricerca
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: left
---

# Dettagli tecnici - Come funziona?

---

 - Searx non offre una personalizzazione dei risultati come Google, ma allo stesso tempo non genera nemmeno un profilo utente.

- A Searx non interessa il tipo di ricerce che fai e non condivide nemmeno con terze parti.

- Searx è software libero, il codice è quindi aperto e puoi aiutare per renderlo migliore. Per maggiori informazioni guarda la pagina su github.

Se ritieni la privacy un elemento importante e credi nelle libertà digitali fai di Searx il tuo motore di ricerca principale.
