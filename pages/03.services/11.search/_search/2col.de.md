---
title: Suche
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://search.disroot.org">Geh Searx'en</a>
<a class="button button1" href="http://mycroftproject.com/search-engines.html?name=disroot.org">Browser-Plugin installieren</a>
<a class="button button1" href="http://bzg6fq2cbzrp52z5xkmggsiqhfc4zb4ouq3g7y6b2yfdnuud6yajpyqd.onion">Tor</a>

---

![](searx_logo.png)

# Anonyme Metasuchmaschine

Disroots Suche ist eine Suchmaschine wie Google, DuckDuckGo oder Qwant, bereitgestellt von **Searx**. Was sie von den anderen unterscheidet, ist, dass es sich bei ihr um eine Metasuchmaschine handelt, die Suchergebnisse anderer Suchmaschinen sammelt während sie keine Informationen über ihre Nutzer speichert.

Du benötigst keinen Disroot-Account, um diesen Service zu nutzen.

Disroot-Suche: [https://search.disroot.org](https://search.disroot.org)

Quellcode: [https://github.com/asciimoo/searx](https://github.com/asciimoo/searx)
