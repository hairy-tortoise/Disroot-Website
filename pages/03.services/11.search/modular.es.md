---
title: Búsqueda
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _search
            - _search-one-search-to-rule-all
            - _search-tech
            - _empty-bar
body_classes: modular
header_image: search_banner.jpg

translation_banner:
    set: true
    last_modified: März 2022
    language: Spanish
---
