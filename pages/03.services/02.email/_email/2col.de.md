---
title: E-Mail
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Disroot-Account registrieren</a>
<a class="button button1" href="https://webmail.disroot.org/">Anmelden</a>
<a class="button button1" href="http://vvobhgocv3jvjwjjyrocao3vij4q7xuk5panbckqwihuztuuvjiwqdid.onion">Tor</a>


---

## E&#8209;Mail

**Disroot** bietet sichere E&#8209;Mail-Accounts zur Nutzung mit Deinem Desktop-Client oder per Webmail. Die Kommunikation zwischen Dir und dem Mailserver ist SSL-verschlüsselt, was den größtmöglichen Schutz bietet. Darüber hinaus werden alle E&#8209;Mails, die von unserem Server versendet werden, TLS-verschlüsselt, wenn der empfangende Mailserver dies unterstützt. Das heißt, Deine E&#8209;Mails werden nicht wie früher als "Postkarte" verschickt, sondern vorher in einen "Briefumschlag" gesteckt.

**Nichtsdestotrotz raten wir Dir dringend, immer vorsichtig zu sein, wenn Du per E&#8209;Mail kommunizierst, und GPG Ende-zu-Ende-Verschlüsselung zu nutzen. So stellst Du sicher, dass Deine Korrespondenz so privat ist, wie sie nur sein kann.**
