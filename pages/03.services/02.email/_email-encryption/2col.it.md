---
title: 'Messaggi di posta criptati'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: left
wider_column: right
---

## Che cos'e la crittografia

---

La crittografia avviene quando si modificano i dati con uno speciale processo di codifica in modo che i dati diventino irriconoscibili (sono crittografati). È quindi possibile applicare un processo di decodifica speciale e si otterranno i dati originali. Mantenendo il processo di decodifica un segreto, nessun altro può recuperare i dati originali dai dati crittografati.

[Video di YouTube - Come funziona la crittografia asimmetrica (in inglese)](https://invidious.snopyta.org/E5FEqGYLL0o)

<br>
![mailvelope](en/mailvelope.svg?resize=150)
Puoi utilizzare Mailvelope, un componente aggiuntivo del browser per Chrome, Edge e Firefox, che crittografa in modo sicuro le tue e-mail con PGP utilizzando Disroot webmail. Guarda questa [guida](https://mailvelope.com/en/help).
