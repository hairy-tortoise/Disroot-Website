---
title: 'Cifrado de Correo electrónico'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: left
wider_column: right
---

## ¿Qué es el cifrado?

---

Cifrar es modificar información con un proceso especial de codificación para que esta sea irreconocible (es cifrada). Luego puedes aplicar un proceso especial de decodificación y obtienes la información original de vuelta. Manteniendo en secreto el proceso de decodificación, nadie más puede recuperar la información original que ha sido cifrada.

[Video - Criptografía asimétrica explicada](https://invidious.snopyta.org/3qk1fy6rOJM)

<br>
![mailvelope](en/mailvelope.svg?resize=150)
Puedes utilizar Mailvelope, un complemento del navegador en Chrome, Edge y Firefox, que encripta de forma segura tus correos electrónicos con PGP utilizando el webmail de Disroot. Consulta este [guías](https://mailvelope.com/en/help).