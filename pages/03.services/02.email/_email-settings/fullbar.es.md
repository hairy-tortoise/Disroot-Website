---
title: 'Configuración de correo'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
##### IMAP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Puerto <span style="color:#8EB726">993</span> | Autenticación: <span style="color:#8EB726">Contraseña normal</span>
##### SMTP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">STARTTLS</span> Puerto <span style="color:#8EB726">587</span> | Autenticación: <span style="color:#8EB726">Contraseña normal</span>
##### SMTPS: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">TLS</span> Puerto <span style="color:#8EB726">465</span> | Autenticación: <span style="color:#8EB726">Contraseña normal</span>
##### POP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Puerto <span style="color:#8EB726">995</span> | Autenticación: <span style="color:#8EB726">Contraseña normal</span>

---

**Tamaño máximo de la casilla de correo:** 1 GB
**Tamaño máximo de archivos adjuntos:** 64 MB

---

**Delimitadores:** Puedes utilizar delimitadores (signo más, "+") en tu dirección de correo electrónico para crear subdirecciones del tipo **nombredeusuario+loquequieras@disroot.org**. Esto es útil para, por ejemplo, filtrar y rastrear el SPAM. Ejemplo: usuaria@disroot.org puede crear direcciones de correo como 'usuaria+banco@disroot.org' que podría usar como cuenta para su banco. Los delimitadores pueden usarse solamente **para recibir** correo, **no para enviarlos**. Tus correos siempre serán enviados como usuaria@disroot.org.
