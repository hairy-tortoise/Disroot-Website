---
title: 'Parametri di configurazione posta elettronica'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

##### IMAP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Porta <span style="color:#8EB726">993</span> | Autentificazione: <span style="color:#8EB726">Normal Password</span>
##### SMTP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">STARTTLS</span> Porta <span style="color:#8EB726">587</span> | Autentificazione: <span style="color:#8EB726">Normal Password</span>
##### SMTPS: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">TLS</span> Porta <span style="color:#8EB726">465</span> | Autentificazione: <span style="color:#8EB726">Normal Password</span>
##### POP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Porta <span style="color:#8EB726">995</span> | Autentificazione: <span style="color:#8EB726">Normal Password</span>

---

**Limite Mailbox:** 1 GB
**Limite allegati:** 64 MB

---

**Delimitatori:** Puoi (segno più, "+") nel vostro indirizzo e-mail per creare sottoindirizzi, come **nomeutente+qualunquecosa@disroot.org**, ad esempio per filtrare e tracciare lo spam. Esempio: david@disroot.org può impostare indirizzi e-mail come 'david+bank@disroot.org' che può dare alla sua banca. Può essere usato **solo per ricevere e-mail**, non per inviarle. Le vostre e-mail saranno sempre inviate come david@disroot.org.
