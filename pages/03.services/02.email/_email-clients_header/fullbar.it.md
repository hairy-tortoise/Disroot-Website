---
title: 'Client di posta elettronica'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

<br>
# Usa il tuo client di posta elettronica preferito
Esistono svariati client di posta elettronica sia per dispositivi desktop sia per dispositivi mobili. Scegli quello che ti piace di più.<br>

Guarda la pagina delle [guide](https://howto.disroot.org/en/tutorials/email/clients) dedicata ai client di posta elettronica.
<br>
*Controlla le impostazioni nella parte superiore di questa pagina per configurare sul tuo dispositivo il tuo account di posta. Queste sono impostazioni standard che indicano al client come raggiungere il server di posta di* **Disroot**.
