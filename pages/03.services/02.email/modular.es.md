---
title: Correo
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _email
            - _green-bar
            - _email-clients_header
            - _email-clients
            - _email-settings
            - _email-webmail
            - _email-encryption
            - _email-alias
            - _email-x-storage

body_classes: modular
header_image: 'riot-dog.jpg'

translation_banner:
    set: true
    last_modified: März 2022
    language: Spanish
---
