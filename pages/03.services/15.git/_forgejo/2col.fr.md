---
title: Git
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://git.disroot.org/user/sign_up">Créer un compte</a>
<a class="button button1" href="https://git.disroot.org/">S'identifier</a>
<a class="button button1" href="http://kgtz2pmmov5jfvn3z4mqryffjnnw6krzrgxxoyaqhqckjrr4pckyhsqd.onion">Tor</a>

---

![forgejo_logo](forgejo.png?resize=100,100)


## Forgejo

**Disroot's Git** est propulsé par **Forgejo**. **Forgejo** est une solution communautaire, puissante, facile à utiliser et légère pour l'hébergement de code et la collaboration sur des projets. Cette solution est construite autour de la technologie GIT qui est le système de contrôle de version moderne le plus utilisé dans le monde aujourd'hui.

Disroot Git: [https://git.disroot.org](https://git.disroot.org)

Page du projet: [https://forgejo.org/](https://forgejo.org/)

Code source: [https://codeberg.org/forgejo/forgejo](https://codeberg.org/forgejo/forgejo)

<hr>

Vous devez créer un compte séparé sur [git.disroot.org](https://git.disroot.org/) pour utiliser ce service. Les identifiants de votre compte **Disroot** ne fonctionnent pas.
