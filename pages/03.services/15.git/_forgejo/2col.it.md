---
title: Git
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://git.disroot.org/user/sign_up">Registrati</a>
<a class="button button1" href="https://git.disroot.org/">Accedi</a>
<a class="button button1" href="http://kgtz2pmmov5jfvn3z4mqryffjnnw6krzrgxxoyaqhqckjrr4pckyhsqd.onion">Tor</a>

---

![forgejo_logo](forgejo.png?resize=100,100)


## Forgejo

**Disroot Git** è alimentato da **Forgejo**. **Forgejo** è una soluzione guidata dalla community, potente, facile da usare e leggera per l'hosting di codice e la collaborazione di progetti. È basato sulla tecnologia GIT, che è il sistema di controllo versione moderna più utilizzato al mondo oggi.

Disroot Git: [https://git.disroot.org](https://git.disroot.org)

Pagina del progetto: [https://forgejo.org/](https://forgejo.org/)

Codice sorgente: [https://codeberg.org/forgejo/forgejo](https://codeberg.org/forgejo/forgejo)

<hr>

Per utilizzare [git.disroot.org](https://git.disroot.org/) necessiti di creare un account specifico. Le credenziali degli altri servizi di **Disroot** non sono supportate.
