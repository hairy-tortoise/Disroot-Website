---
title: 'Forgejo'
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _forgejo
            - _forgejo-highlights
            - _forgejo-features1
            - _forgejo-features2
body_classes: modular
header_image: Hackers_Junction_2015_by_vmuru.jpg

translation_banner:
    set: true
    last_modified: März 2022
    language: German
---
