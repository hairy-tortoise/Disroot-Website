---
title: 'Caratteristiche di Git'
wider_column: left
---

![](en/forgejo_dashboard.png?lightbox=1024)

![](en/forgejo_activity.png?lightbox=1024)


---

## Tracciamento dei problemi
Gestisci i problemi, richiedi funzionalità e feedback sui tuoi progetti.

## Tag, milestone e commenti
Gestisci il tuo repository in modo efficiente determinando facilmente i milestone, ordina i tuoi problemi, discuti e assegna le tue attività ai membri del team.

## Wiki
Crea un wiki per spiegare il tuo progetto scrivendo in Markdown.

## Applicazione mobile
Gestisci il tuo progetto utilizzando l'applicazione su Android (scaricabile su fdroid).
