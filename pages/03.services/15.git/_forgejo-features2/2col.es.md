---
title: 'Características de Git'
wider_column: left
---

![](en/forgejo_dashboard.png?lightbox=1024)

![](en/forgejo_activity.png?lightbox=1024)


---

## Seguimiento de problemas
Gestiona problemas, solicitud de funcionalidades y comentarios para tus proyectos.

## Etiquetas, asignaciones, metas, comentarios
Administra tus repositorios de manera eficiente determinando fácilmente tus metas, ordenando los problemas, discutiendo y asignando tareas a tus compañerxs de equipo.

## Wiki
Crea una wiki para explicar tu proyecto y utiliza Markdown.

## Aplicación para el móvil
Maneja tu proyecto sobre la marcha con la aplicación para móviles con Android (disponible en F-Droid).
