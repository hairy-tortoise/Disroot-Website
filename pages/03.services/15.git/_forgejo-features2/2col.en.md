---
title: 'Git Features'
wider_column: left
---

![](en/forgejo_dashboard.png?lightbox=1024)

![](en/forgejo_activity.png?lightbox=1024)


---

## Issue tracking
Manage issues, feature request and feedback to your projects.

## Tags, assignee, milestones, comments
Manage your repository efficiently by easily determining your milestones, sort your issues, discuss and assign your tasks to fellow team members.

## Wiki
Create a wiki to explain your project, and use Markdown.

## Mobile app
Manage your project on the go with Android mobile app (available on F-Droid).
