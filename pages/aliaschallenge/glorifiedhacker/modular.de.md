---
title: Cryptocurrencies
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _crypto
            - _bitcoin
            - _monero
            - _faircoin
            - _pagebottom
body_classes: modular
header_image: burning_money.png
---
