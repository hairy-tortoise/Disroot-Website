---
title: 'Introduction'
bgcolor: '#7b384fff'
fontcolor: '#FFF'
text_align: left
---

<br>

<span style="padding-right:10%;" id="fund1">
En los últimos días de este año, cuando el mundo entero se prepara para recibir la luz de una nueva década, tres dominios están peleando por el control del mercado de la remolacha. **Unete a la batalla y elige a tu Jefe Supremo**.
</span>

<span style="padding-right:30%;" id="fund2">
Desde el 23 de Diciembre hasta el 1ro de Enero, participa de nuestro evento de recaudación de fondos de fin de año. **Dona a Disroot** usando uno de los hashtags de dominios de abajo como tu referencia de donación. El dominio victorioso será otorgado como un **alias extra para todxs lxs Disrooters ahora y en el futuro**.
</span>

## ¡Que el dominio más cool gane!
