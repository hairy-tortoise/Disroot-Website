---
title: 'Goal'
bgcolor: '#09846aff'
fontcolor: '#FFFFFF'
text_align: left
---

<br>


<h3 id="goaltitle"> Feliz 2020! </h3>

<div class="cup">
<img src="/user/pages/aliaschallenge/_goal/trophy.png" alt="Trophy" width="80" />

<span class="timer">
Obrigado a todos !!
</span>

</div>

<div class="tree">
<img src="/user/pages/aliaschallenge/_goal/tree.png" alt="Tree of cats" />
</div>
