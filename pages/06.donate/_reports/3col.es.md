---
title: 'Reportes anuales'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---
<br>

#Reportes anuales:

**Reporte Anual 2022**
[Reporte 2022](/annual_reports/AnnualReport2022.pdf?target=_blank)

**Reporte Anual 2020**
[Reporte 2020](/annual_reports/AnnualReport2020.pdf?target=_blank)

**Reporte Anual 2019**
[Reporte 2019](/annual_reports/AnnualReport2019.pdf?target=_blank)

**Reporte Anual 2018**
[Reporte 2018](/annual_reports/AnnualReport2018.pdf?target=_blank)

**Agosto 2016 - Agosto 2017**
[Reporte 2016-2017](/annual_reports/2017.pdf?target=_blank)

**2015 - Agosto 2016**
Costos: €1569.76 / Donaciones: €264.52

---



---
<br>

# Donaciones de equipos
Si tienes cualquier parte o equipo que pudiera ser utilizada por nuestro proyecto (memorias, servidores, CPUs, discos duros, Tarjetas de red/RAID, equipos de red, etc.), ponte en contacto con nosotrxs. Cualquier pieza de hardware es bienvenida. Las cosas que no podemos usar las podemos vender para hacer dinero.
Estamos ansiosos por escuchar cualquier propuesta de espacio para [racks](https://es.wikipedia.org/wiki/Rack).
