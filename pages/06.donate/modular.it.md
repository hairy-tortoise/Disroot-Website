---
title: Donazioni
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _donate
            - _text
            - _goals
            - _blue-bar
            - _overview
            - _reports
body_classes: modular
header_image: donate-banner.jpeg

translation_banner:
    set: true
    last_modified: Marzo 2022
    language: Italian
---
