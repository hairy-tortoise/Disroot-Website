---
title: 'Blue bar'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## We are grateful for your donations and support!

With your help, we are getting closer to reaching our goal for financial sustainability, and we prove that a social model of economy is possible.

To thanks those of you that donate at least 10€, we will send a **Disroot stickers pack**. Don't forget to leave, in the reference of your donation, the postal address we should send this to.

![](stickers.jpg?resize=50%)