---
title: 'Blue bar'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## ¡Estamos agradecidos por sus donaciones y apoyo!

Con tu ayuda, estamos acercándonos a nuestra meta para alcanzar la sustentabilidad financiera, y probamos que un modelo social de economía es posible.

To thanks those of you that donate at least 10€, we will send a **Disroot stickers pack**. Don't forget to leave, in the reference of your donation, the postal address we should send this to.

![](stickers.jpg?resize=50%)