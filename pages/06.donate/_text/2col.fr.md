---
title: Goals
bgcolor: '#FFF'
fontcolor: '#1F5C60'
wider_column: right
---

##  <span style="color:#8EB726;"> Il n'y a pas de service gratuit sur Internet ! </span>

---

Qu'il s'agisse des personnes qui consacrent leur temps à la maintenance du logiciel ou qui paient pour le matériel qui le fait fonctionner, l'électricité utilisée pour alimenter les machines ou l'espace d'hébergement dans le centre de données. Avec les grandes entreprises populaires qui offrent un service "gratuit", ce sont très probablement vos données personnelles d'utilisateur qui en paient le prix. Imaginez l'ampleur de leurs opérations, la quantité de personnel et de matériel nécessaire pour servir des milliards d'utilisateurs dans le monde. Ce n'est pas une organisation caritative, et pourtant elle fournit un service "gratuit"...

Disroot.org est géré par des bénévoles et offre tous les services gratuitement. Nous ne collectons pas d'informations pour les vendre à des sociétés de publicité ou à des agences gouvernementales. Nous devons donc compter sur les dons et le soutien des utilisateurs des services et de la communauté. Si vous souhaitez poursuivre le projet et créer un espace pour de nouveaux utilisateurs potentiels, vous pouvez utiliser l'une des méthodes ci-dessous pour apporter une contribution financière.

**N'oubliez pas que toute somme d'argent compte ! Si tous les Disrooters nous offraient une tasse de café chaque mois, nous pourrions faire fonctionner ce service sans aucune difficulté financière**.
