---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
goals:
    -
        title: 'Condividiamo la nostra fortuna'
        text: "Se riceviamo almeno 400 EUR in donazioni, condividiamo il 15% del nostro surplus con gli sviluppatori del software che stiamo utilizzando. Disroot non esisterebbe senza quegli sviluppatori."
        unlock: yes
    -
        title: 'Paga una quota di volontario'
        text: "Se alla fine del mese ci rimangono più di 150 €, paghiamo a un membro di Disroot Core una quota di volontariato di 150 €."
        unlock: yes
    -
        title: 'Paga due quote ai volontari'
        text: "Se alla fine del mese ci rimangono più di 300 €, paghiamo a due membri di Disroot Core una quota di volontariato di 150 €."
        unlock: yes
    -
        title: 'Paga tre quote ai volontari '
        text: "Se alla fine del mese ci rimangono più di 450 €, paghiamo a tre membri di Disroot Core una quota di volontariato di 150 €."
        unlock: yes
    -
        title: 'Paga quattro quote ai volontari'
        text: "Se alla fine del mese ne rimangono più di 600, paghiamo a quattro membri di Disroot Core una quota di volontariato di 150 €."
        unlock: no
    -
        title: 'Paga cinque quote ai volontari'
        text: "Se alla fine del mese ne rimangono più di 600, paghiamo a quattro membri di Disroot Core una cinque di volontariato di 150 €."
        unlock: no
---

<div class=goals markdown=1>

</div>
