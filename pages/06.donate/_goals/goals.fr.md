---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
goals:
    -
        title: 'Partager notre chance'
        text: "Si nous recevons au moins 400€ de dons, nous partageons 15% de notre excédent avec les développeurs des logiciels que nous utilisons. Disroot n'existerait pas sans ces développeurs."
        unlock: yes
    -
        title: 'Payer un volontaire'
        text: "S'il nous reste plus de 150€ à la fin du mois, nous versons à un membre de l'équipe Disroot Core une rémunération bénévole de 150€."
        unlock: yes
    -
        title: 'Payer deux volontaires'
        text: "S'il nous reste plus de 300€ à la fin du mois, nous versons à un membre de l'équipe Disroot Core une rémunération bénévole de 150€."
        unlock: yes
    -
        title: 'Payer trois volontaires'
        text: "S'il nous reste plus de 450€ à la fin du mois, nous versons à un membre de l'équipe Disroot Core une rémunération bénévole de 150€."
        unlock: yes
    -
        title: 'Payer quatre volontaires'
        text: "S'il nous reste plus de 600€ à la fin du mois, nous versons à un membre de l'équipe Disroot Core une rémunération bénévole de 150€."
        unlock: no
    -
        title: 'Payer cinq volontaires'
        text: "S'il nous reste plus de 750€ à la fin du mois, nous versons à un membre de l'équipe Disroot Core une rémunération bénévole de 150€."
        unlock: no
---

<div class=goals markdown=1>

</div>
