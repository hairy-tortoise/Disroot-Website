---
title: Übersicht
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---

## Übersicht über die Finanzen

![](graph.png?lightbox=1024)


Unterstützt von <button class="button button4">191</button> Disrooters
_(Click the number for an overview)_


![](graph_support.png?lightbox=1024&resize=40,40&class=transparent)

## Spenden an FOSS-Projekte
![](donated.png?lightbox=1024&resize=70%)
