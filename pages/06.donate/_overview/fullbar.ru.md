---
title: Overview
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---

## Месячный обзор

![](graph.png?lightbox=1024)


Вклад внесли <button class="button button4">191</button> дизрутера(ов)
_(Щелкните номер для обзора)_

![](graph_support.png?lightbox=1024&resize=40,40&class=transparent)

## Обзор расходования пожертвований
![](donated.png?lightbox=1024&resize=70%)
