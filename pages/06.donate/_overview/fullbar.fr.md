---
title: Aperçu
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---

## Aperçu financier

![](graph.png?lightbox=1024)


Soutenu par <button class="button button4">191</button> Disrooters
_(Cliquez sur le numéro pour un aperçu)_

![](graph_support.png?lightbox=1024&resize=40,40&class=transparent)

## FOSS Donations
![](donated.png?lightbox=1024&resize=70%)
