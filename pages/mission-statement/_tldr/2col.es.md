---
title: Declaración de Objetivos
bgcolor: '#1F5C60'
fontcolor: '#FFF'
---

---

### TL:DR (Too Long, DID Read)<br>(*Demasiado Largo; Lo He Leído*)

El propósito de la Fundación Disroot.org es recuperar el mundo virtual de la rígida realidad que lo ha dominado. Estamos seguros que este enfoque tiene un gigantesco potencial para arrancar a la sociedad del actual status quo gobernado por la publicidad, el consumismo y el poder al mejor postor. Descentralizando una red de servicios cooperativos en el siglo 21, podemos superar las limitaciones impuestas por las condiciones económicas, el impacto energético y las complejas infraestructuras, dándole a la red el poder de crecer independientemente, de acuerdo a sus propias reglas. Una red de muchas pequeñas tribus cooperando unas con otras derrotará a un gigante de piedra. ¿Vendrás a desraizar con nosotros?
