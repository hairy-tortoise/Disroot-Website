---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "SERVICES"
section_number: "400"
faq:
    -
        question: "What services can I use with my Disroot account?"
        answer: "<p>Your account allows you to use the following services with the same username and password:</p>
        <ul class=disc>
          <li>Email</li>
          <li>Cloud</li>
          <li>Forum</li>
          <li>Chat</li>
          <li>Project Board</li>
        </ul>
        <p>The rest of the services (<b>Pads, PasteBin, Upload, Search, Polls</b>) don’t require any registration.</p>"
    -
        question: "What uses can I give to my email account?"
        answer: "<p>You can use it any way you want, except for commercial purposes or for sending Spam. To get a better idea of what you can and can’t do with it, please, read paragraphs 10 and 11 from our <a href='https://disroot.org/en/tos' target='_blank'>Terms of Service</a></p>"
    -
        question: "What are the inbox and the cloud size?"
        answer: "<p>The <b>mailbox size</b> limit is <b>1GB</b> and <b>attachment size</b> limit is <b>50MB</b>.</p>
        <p><b>Cloud storage</b> is <b>2GB</b>.</p>
        <p>It's possible to extend your cloud and mail storage. Check the options <a href='https://disroot.org/en/forms/extra-storage-space/' target='_blank'>here</a></p>"
    -
        question: "Can I see some status of services like uptime, scheduled maintenance, etc. ?"
        answer: "<p>Yes. And there is multiple ways to be up-to-date with all issues, maintenance and general health information about the platform.</p>
        <ul class=disc>
        <li>Visit <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Subscribe to RSS feed of <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Subscribe to email notifications at <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Follow <b>disroot_state@hub.disroot.org</b> via Hubzilla, Diaspora*, Mastodon, Pleroma, Pixelfed, etc. (the fediverse...)</li>
        <li>Follow <b>disroot@social.weho.st</b> (the fediverse again...)</li>
        <li>Join <b>state@chat.disroot.org</b> (XMPP)</li>
        <li>Install <b>DisApp</b>, <b>Disroot</b>'s Android app where you will get realtime notifications</li>
        </ul>"
---
