---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "SERVICES"
section_number: "400"
faq:
    -
        question: "Welche Services kann ich mit meinem Disroot-Account nutzen?"
        answer: "<p>Dein Account ermöglicht es Dir, die folgenden Angebote mit dem selben Nutzernamen und Passwort zu nutzen:</p>
        <ul class=disc>
          <li>E&#8209;Mail</li>
          <li>Cloud</li>
          <li>Forum</li>
          <li>Chat</li>
          <li>Projektboard</li>
        </ul>
        <p>Die restlichen Services (<b>Pads, PasteBin, Upload, Suche, Umfragen</b>) benötigen keine Anmeldung.</p>"
    -
        question: "Was kann ich mit meinem E&#8209;Mail-Account alles machen?"
        answer: "<p>Du kannst ihn für alles nutzen, das Du willst, außer für gewerbliche Zwecke oder um Spam damit zu versenden. Um eine bessere Vorstellung davon zu erhalten, was und was nicht Du mit Deinem Disroot-E&#8209;Mailaccount machen kannst, lies Dir bitte die Paragraphen 10 und 11 unserer <a href='https://disroot.org/en/tos' target='_blank'>Nutzungsbedingungen</a>durch</p>"
    -
        question: "Wie groß sind mein Postfach und die Cloud?"
        answer: "<p>Die <b>Postfach-Größe</b> beträgt max <b>1GB</b>, <b>Anhänge</b> sind auf max. <b>50MB</b>begrenzt.</p>
        <p>Du hast einen <b>Cloudspeicher</b> von <b>2GB</b>.</p>
        <p>Du kannst Deinen Cloud- und Deinen Mail-Speicher noch erweitern. Nähere Informationen findest Du <a href='https://disroot.org/de/forms/extra-storage-space/' target='_blank'>hier</a></p>"
    -
        question: "Kann ich irgendwo den Status der Services wie zum Beispiel Uptime, geplante Wartungszeiten etc. erfahren ?"
        answer: "<p>Ja. Es gibt sogar mehrere Möglichkeiten, wie Du auf dem neuesten Stand bleiben kannst, was die Fehler, Wartungszeiten und allgemeinen Zustandsinformationen der Plattform angeht.</p>
        <ul class=disc>
        <li>Besuche <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Abonniere den RSS-Feed von <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Abonniere die E&#8209;Mail-Benachrichtigungen von <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Folge <b>disroot_state@hub.disroot.org</b> via Hubzilla, Diaspora*, Mastodon, Pleroma, Pixelfed, etc. (das Fediverse...)</li>
        <li>Folge <b>disroot@social.weho.st</b> (das Fediverse schon wieder...)</li>
        <li>Betritt <b>state@chat.disroot.org</b> (XMPP)</li>
        <li>Installiere die <b>DisApp</b>, <b>Disroot</b>'s Android-App, durch die Du Echtzeit-Benachrichtigungen erhältst</li>
        </ul>"
---
