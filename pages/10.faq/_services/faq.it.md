---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "SERVIZI"
section_number: "400"
faq:
    -
        question: "Di quali servizi posso usufruire con il mio account Disroot?"
        answer: "<p>Il tuo account ti permette di utilizzare i seguente servizi con lo stesso username e la stessa password:</p>
        <ul class=disc>
          <li>Email</li>
          <li>Cloud</li>
          <li>Forum</li>
          <li>Chat</li>
          <li>Project Board</li>
        </ul>
        <p>Gli altri servizi (<b>Pads, PasteBin, Upload, Search, Polls</b>) non richiedono alcuna registrazione.</p>"
    -
        question: "Per quali scopi posso utilizzare il mio account di posta elettronica?"
        answer: "<p>Puoi utilizzarlo come meglio credi tranne che per scopi commerciali e per l'invio di spam. Per avere un'idea migliore di ciò che puoi e non puoi farci, leggi i paragrafi 10 e 11 dei <a href='https://disroot.org/en/tos' target='_blank'>Termini di servizio</a></p>"
    -
        question: "Quanto spazio ho a disposizione per i servizi di posta elettronica e di cloud?"
        answer: "<p>Per la <b>mailbox</b> il limite è di <b>1GB</b>. Si possono inviare <b>allegati</b> fino a <b>50MB</b>.</p>
        <p>Nel<b>Cloud</b> si hanno a disposizione <b>2GB</b>.</p>
        <p>È possibile estendere i limiti sia per il cloud sia per l'email. Leggi <a href='https://disroot.org/en/forms/extra-storage-space/' target='_blank'>qui</a></p>"
    -
        question: "Posso vedere lo stato dei servizi come uptime, manutenzione programmata, ecc ?"
        answer: "<p>Sì. Ci sono diversi modi per essere aggiornati sulla manutenzione e lo stato dell'infrastruttura.</p>
        <ul class=disc>
        <li>Visita <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Iscriviti ai feed <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Sottoscrivi le notifiche email <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Segui <b>disroot_state@hub.disroot.org</b> via Hubzilla, Diaspora*, Mastodon, Pleroma, Pixelfed, etc. (il fediverso...)</li>
        <li>Segui <b>disroot@social.weho.st</b> (il fediverso...)</li>
        <li>Accedi a <b>state@chat.disroot.org</b> (XMPP)</li>
        <li>Installa <b>DisApp</b>, <b>Disroot</b>app dove ottieni le lo stato attraverso delle notifiche in tempo reale</li>
        </ul>"
---
