---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "UTILISATEUR"
section_number: "500"
faq:
    -
        question: "J'ai oublié mon mot de passe. Comment puis-je le réinitialiser?"
        answer: "<p>Afin de réinitialiser votre mot de passe, vous devez configurer des questions de sécurité et/ou un e-mail de récupération. Si c'est le cas, allez <a href='https://user.disroot.org/' target='_blank'>ici </a> et cliquez sur 'Mot de passe oublié'.</p>
        <p>
        Si vous n'avez défini aucune de ces options, vous ne pouvez rien faire d'autre que créer un nouveau compte.</p>"
    -
        question: "Mon compte a été approuvé mais je ne peux pas me connecter. Pourquoi?"
        answer: "<p>Il est très probable que vous entrez incorrectement:</p>
        <ul class=disc>
        <li>votre nom d'utilisateur (rappelez-vous que c'est juste le nom d'utilisateur, sans le domaine), ou...</li>
        <li>votre mot de passe (n'oubliez pas qu'il est sensible à la casse).</li>
        </ul>"
    -
        question: "J'obtiens un message <i>'Clé privée non valide pour l'application de chiffrement'</i> ou <i>'Invalid private key for encryption app'</i>. Pourquoi ?"
        answer: "<p>C'est probablement parce que vous avez changé votre mot de passe récemment. Comme Nextcloud utilise le mot de passe utilisateur pour générer les clés de cryptage, il est nécessaire de les régénérer à partir de votre nouveau mot de passe. Pour ce faire, vous devez:</p>
        <ol>
        <li>vous connecter au cloud </li>
        <li>sélectionner <b>Personnel</b> dans le menu </li>
        <li>faire défiler vers le bas jusqu'au module <b>Cryptage de base</b> et taper votre ancien mot de passe </li>
        <li>puis le nouveau et </li>
        <li>enfin cliquer sur <b>Mettre à jour le mot de passe de la clé privée</b></li></ol>

        <p>Après vous être déconnecté et être retourné dans le cloud, vous devriez voir vos fichiers et le message devrait avoir disparu.</p>

        <p>Si vous ne vous souvenez pas de votre ancien mot de passe, il est toujours possible de réinitialiser le compte mais il ne sera pas possible de récupérer les fichiers sur le cloud car ils sont chiffrés avec l'ancienne clé. Ce que vous devez faire c'est de supprimer tous les fichiers de Nextcloud (cela n'inclut pas les calendriers, les contacts, etc., mais seulement les fichiers), et nous contactez (support@disroot.org). Nous procéderons alors à l'effacement de la clé afin que la nouvelle paire de clés basée sur votre mot de passe actuel puisse être générée automatiquement lors de la nouvelle connexion.</p>"
    -
        question: "Mon compte Disroot expire-t-il ?"
        answer: "<p> Non. Nous n'expirons pas les comptes non utilisés. Cependant, nous apprécions que les gens assument la responsabilité de leur compte et de son impact sur les ressources <b>Disroot</b>, et nous demandons la suppression du compte quand ils décident de ne plus utiliser nos services.</p>"
    -
        question: "Comment puis-je supprimer mon compte Disroot ?"
        answer: "<p>Pour supprimer votre compte <b>Disroot</b> connectez-vous à <a href='https://user.disroot.org' target='_blank'>https://user.disroot.org</a> et sélectionnez <b>'Supprimer mon compte'</b>.</p>
        <p>Les comptes et leurs données sont effacés quotidiennement.</p>"
    -
        question: "Comment puis-je changer mon mot de passe ?"
        answer: "<p>Connectez-vous à <a href='https://user.disroot.org' target='_blank'>https://user.disroot.org</a> et sélectionnez <b>'Changer mot de passe'</b>.</p>
        <p>Vous devrez également changer votre mot de passe sur <b>Nextcloud</b> pour pouvoir accéder à vos fichiers, qui sont chiffrés à l'aide de votre mot de passe. Pour ce faire, suivez les étapes décrites dans la réponse à la question <i>'J'obtiens un message 'Clé privée non valide pour l'application de chiffrement'</i> ou regardez ce <a href='https://howto.disroot.org/fr/user/account/ussc#change-your-password' target='_blank'>tutoriel.</a></p>"

---
