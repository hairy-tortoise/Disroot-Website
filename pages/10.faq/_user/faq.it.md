---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "UTENTE"
section_number: "500"
faq:
    -
        question: "Ho dimenticato la password. Come posso resettarla?"
        answer: "<p>Per poter resettare la password, devi aver precedentemente impostato le domande di sicurezza o un indirizzo di posta elettronica secondario. In questi casi, puoi procedere <a href='https://user.disroot.org/' target='_blank'>qui </a> cliccando su 'Password dimenticata'.</p>
        <p>
        Se non hai impostato nessuna delle opzioni di recupero, non puoi fare nulla, se non registrare un nuovo account.</p>"
    -
        question: "Il mio account è stato approvato ma non riesco ad accedere. Perché?"
        answer: "<p>È molto probabile che tu abbia digitato in modo scorretto:</p>
        <ul class=disc>
        <li>il tuo username (ricorda che devi inserire solo il tuo username, senza il dominio), oppure...</li>
        <li>la tua password (ricorda che il sistema è case sensitive).</li>
        </ul>"
    -
        question: "Ho ricevuto un messaggio <i>'Invalid private key for encryption app'</i>. Perché?"
        answer: "<p>Probabilmente è perché hai cambiato la password di recente. Poiché Nextcloud utilizza la password dell'utente per generare le chiavi di crittografia, è necessario rigenerarle dalla nuova password. Per fare ciò, è necessario:</p>
        <ol>
        <li>fare il login nel cloud; </li>
        <li>selezionare <b>Personal</b> dal menu </li>
        <li>andare su <b>Basic encryption module</b> e digitare la vecchia password </li>
        <li>successivamente quella nuova </li>
        <li>infine cliccare su <b>Update Private Key Password</b></li></ol>

        <p>Dopo aver fatto il logout e successivamente il login il messaggio non dovrebbe più presentarsi..</p>

        <p>Se non ti ricordi la tua vecchia password, è ancora possibile ripristinare l'account ma non sarà possibile ripristinare alcun file sul cloud poiché sono crittografati con la vecchia chiave.</p>"
    -
        question: "Il mio account Disroot scadrà?"
        answer: "<p> No. Non eliminiamo gli account non utilizzati. Tuttavia apprezziamo quando gli utenti  si assumono la responsabilità del proprio account e del suo impatto sulle risorse <b>Disroot</b> e chiedono la cancellazione dell'account quando decidono di non più utilizzare i nostri servizi.</p>"
    -
        question: "Come posso eliminare il mio account disroot?"
        answer: "<p>Per cancellare il proprio account <b>Disroot</b> accedi a <a href='https://user.disroot.org' target='_blank'>https://user.disroot.org</a> e seleziona <b>'Elimina il mio account'</b>.</p>
        <p>Gli account e i loro dati vengono cancellati quotidianamente.</p>"
    -
        question: "Come posso cambiare la mia password?"
        answer: "<p>Fai l'accesso a <a href='https://user.disroot.org' target='_blank'>https://user.disroot.org</a> e seleziona <b>'Cambia password'</b>.</p>
        <p>Dovrai anche cambiare la tua password su <b>Nextcloud</b> per poter accedere ai tuoi file, che sono criptati usando la tua password. Per farlo, segui i passaggi descritti nella risposta a <i>'Invalid private key for encryption app'</i> o controlla questa <a href='https://howto.disroot.org/it/user/account/ussc#change-your-password' target='_blank'>guida.</a></p>"

---
