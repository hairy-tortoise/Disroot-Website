---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "SOBRE EL USUARIO"
section_number: "500"
faq:
    -
        question: "Olvidé mi contraseña. ¿Cómo puedo restablecerla?"
        answer: "<p>Para restablecer tu contraseña necesitas, primero, tener configuradas las preguntas de seguridad y/o una dirección de correo secundaria. Si este es el caso, entonces ve <a href='https://user.disroot.org/' target='_blank'>aquí </a> y haz click en 'Forgotten password'.</p>
        <p>Si no has configurado alguna de estas opciones, entonces no hay nada más que puedas hacer, excepto registrar una nueva cuenta.</p>"
    -
        question: "Mi cuenta fue aprobada pero no puedo iniciar sesión. ¿Por qué?"
        answer: "<p>Es muy probable que estés ingresando incorrectamente:</p>
        <ul class=disc>
        <li>tu nombre de usuarix (recuerda que es solo el nombre de usuarix sin el dominio), o...</li>
        <li>tu contraseña (recuerda que es sensible a las mayúsculas).</li>
        </ul>"
    -
        question: "Recibo un mensaje que dice <i>'La clave privada no es válida para la app de cifrado'</i>. ¿Por qué?"
        answer: "<p>Probablemente porque has cambiado tu contraseña recientemente. Como Nextcloud utiliza la contraseña de usuarix para generar las claves de cifrado, es necesario regenerarlas a partir de tu contraseña nueva. Para hacerlo, debes:</p>
        <ol>
        <li>iniciar sesión en la nube; </li>
        <li>seleccionar <b>Personal</b> desde el menú; </li>
        <li>bajar hasta <b>Módulo de Encriptado Básico</b> y escribir tu antigua contraseña; </li>
        <li>luego la nueva y </li>
        <li>finalmente, hacer click en <b>Actualizar Clave Privada</b></li></ol>

        <p>Después de cerrar sesión en la nube y volver a iniciarla, deberías ver tus archivos y el mensaje haber desaparecido.</p>

        <p>Si no recuerdas tu anterior contraseña, aún es posible restablecer la cuenta pero no será posible recuperar ninguno de los archivos en la nube ya que están cifrados con la clave antigua.</p>"
    -
        question: "¿Caduca mi cuenta de Disroot?"
        answer: "<p> No. No expiramos las cuentas sin utilizar. Sin embargo, apreciamos cuando las personas asumen la responsabilidad de su cuenta, y su impacto en los recursos de <b>Disroot</b> y solicitan la eliminación de la misma cuando deciden no utilizar más nuestros servicios.</p>"
    -
        question: "¿Cómo puedo dar de baja mi cuenta de Disroot?"
        answer: "<p>Para eliminar tu cuenta de <b>Disroot</b>, inicia sesión en <a href='https://user.disroot.org' target='_blank'>https://user.disroot.org</a> y selecciona <b>'Delete My Account'</b>.</p>
        <p>Las cuentas y sus datos son borrados diariamente.</p>"
    -
        question: "¿Cómo puedo cambiar mi contraseña?"
        answer: "<p>Inicia sesión en <a href='https://user.disroot.org' target='_blank'>https://user.disroot.org</a> y selecciona <b>'Change Password'</b>.</p>
        <p>También necesitarás modificar tu contraseña en <b>Nextcloud</b> para poder acceder a tus archivos, que están cifrados utilizando tu contraseña. Para hacerlo, sigue los pasos descritos más arriba en <i>'Recibo un mensaje que dice 'La clave privada no es válida para la app de cifrado''</i> ¿Por qué?' o revisa este <a href='https://howto.disroot.org/es/user/account/ussc#change-your-password' target='_blank'>tutorial.</a></p>"

---
