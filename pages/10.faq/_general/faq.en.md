---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "GENERAL"
section_number: "300"
faq:
    -
        question: "What does Disroot mean?"
        answer: "<p><b>Dis'root'</b>: To tear up the roots of, or by the roots; hence, to tear from a foundation; to uproot.</p>"
    -
        question: "Will Disroot last?"
        answer: "<p>The admins and maintainers of <b>Disroot</b> use it daily and depend on it as their main communication tool. We intend to keep it going for a very long time. <a href='https://forum.disroot.org/t/will-disroot-last/101' target='_blank'>Check the Forum for the long answer</a></p>"
    -
        question: "How many people use Disroot?"
        answer: "<p>We do not keep track of active users so we can't answer this question. Besides, we consider that is not by any means a way to measure the platform's health. User numbers are susceptible to manipulation for many reasons, wow factor and to satisfy investors, for example. Since the platform purposes have nothing to do with any of that, we see no point in giving false or manipulated statistics about it.</p>"

---
