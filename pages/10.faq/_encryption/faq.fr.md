---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "CHIFFREMENT"
section_number: "200"
faq:
    -
        question: "Puis-je utiliser la 2FA?"
        answer: "<p>Oui, vous pouvez l'utiliser pour le <b>Cloud</b>. Mais avant de l'activer, assurez-vous de bien comprendre comment cela fonctionne et comment l'utiliser. Pour plus d'informations, rendez-vous <a href='https://howto.disroot.org/fr/tutorials/cloud/settings#two-factor-authentication' target='_blank'>ici</a></p>"
    -
        question: "Puis-je utiliser le chiffrement de bout en bout dans le cloud?"
        answer: "<p>Actuellement, le chiffrement de bout en bout est désactivé en raison d'un bogue de longue date avec l'application de bureau <b>Nextcloud</b>.</p>"

---
